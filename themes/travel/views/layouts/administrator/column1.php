<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/administrator/main'); ?>
<div class="content-wrapper">
    <?php
            foreach(Yii::app()->user->getFlashes() as $key => $message) {
                    echo '<div class="alert alert-' . $key . '">';
                    echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
                    print $message;
                    print "</div>\n";
                    
                    
            }
    ?>
    <!-- Main content -->
    <?php echo $content; ?>
    
</div><!-- /.content-wrapper -->
<?php $this->endContent(); ?>