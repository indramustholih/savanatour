<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Savana Tour Jogja - Administrator Page</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="author" content="Digital Data Studio">
    <meta name="robots" content="noodp,noydir"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php
      $baseUrl = Yii::app()->theme->baseUrl.'/administrator';
      $resources = Yii::app()->theme->baseUrl.'/administrator/resources';
      $favico = Yii::app()->theme->baseUrl.'/images/favico';
    ?>
   
    
    <!-- FontAwesome 4.3.0 -->
     <link href="<?php echo $resources;?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Ionicons 2.0.0 -->
    <link href="<?php echo $resources;?>/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    
    <!-- DATA TABLES -->
    <link href="<?php echo $resources;?>/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    
    <!-- Theme style -->
    <link href="<?php echo $resources;?>/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo $resources;?>/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Custom CSS -->
    <link href="<?php echo $baseUrl;?>/css/custom.css" rel="stylesheet" type="text/css" />
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- favicon links -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $favico;?>/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $favico;?>/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $favico;?>/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $favico;?>/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $favico;?>/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $favico;?>/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $favico;?>/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $favico;?>/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $favico;?>/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $favico;?>/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $favico;?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $favico;?>/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $favico;?>/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $favico;?>/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $favico;?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    
  </head>
  <body class="skin-blue sidebar-mini wysihtml5-supported">
    <div class="wrapper">
        <?php include_once('header.php');?>
