<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="<?php echo $resources;?>/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p><?php echo User::model()->findByPk(Yii::app()->user->id)->nama;?></p>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <?php /*
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..." />
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          */?>
          <br>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="<?php echo Yii::app()->createUrl('site/administrator');?>">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
            
            <li>
              <a href="<?php echo Yii::app()->createUrl('inbox/admin');?>">
                <i class="fa fa-envelope-o"></i> <span>Inbox</span>
              </a>
            </li>
           
            <li class="treeview">
              <a href="#"><i class="fa fa-image"></i> <span>Image Slider</span></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo Yii::app()->createUrl('slider/create');?>"><i class="fa fa-circle-o text-green"></i> Buat Slider Baru</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('slider/admin');?>"><i class="fa fa-circle-o text-green"></i> Daftar Image Slider</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#"><i class="fa fa-book"></i> <span>Paket Tour</span></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo Yii::app()->createUrl('tour/create');?>"><i class="fa fa-circle-o text-green"></i> Buat Paket Baru</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('tour/admin');?>"><i class="fa fa-circle-o text-green"></i> Daftar Paket Tour</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('tourKategori/admin');?>"><i class="fa fa-circle-o text-green"></i> Tour Kategori</a></li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#"><i class="fa fa-newspaper-o"></i> <span>Blog/News</span></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo Yii::app()->createUrl('blog/create');?>"><i class="fa fa-circle-o text-green"></i> Buat Blog Baru</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('blog/admin');?>"><i class="fa fa-circle-o text-green"></i> Daftar Blog</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('blogKategori/admin');?>"><i class="fa fa-circle-o text-green"></i> Blog Kategori</a></li>
              </ul>
            </li>
            <?php /*
            <li class="treeview">
              <a href="#"><i class="fa fa-book"></i> <span>Event</span></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo Yii::app()->createUrl('event/create');?>"><i class="fa fa-circle-o text-green"></i> Buat Paket Baru</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('event/admin');?>"><i class="fa fa-circle-o text-green"></i> Daftar Paket Tour</a></li>
              </ul>
            </li>
          */?>
            <li class="treeview">
              <a href="#"><i class="fa fa-toggle-right"></i> <span>Data Reference</span></a>
              <ul class="treeview-menu">
                <li><a href="<?php echo Yii::app()->createUrl('eksternalLink/admin');?>"><i class="fa fa-circle-o text-green"></i> Eksternal Link</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('sosmedAccount/admin');?>"><i class="fa fa-circle-o text-green"></i> Account Sosial Media</a></li>
              </ul>
            </li>
            
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-database"></i>
                <span>Data Wilayah</span>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo Yii::app()->createUrl('/pulau/admin');?>">
                    <i class="fa fa-circle-o text-green"></i> <span>Pulau</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo Yii::app()->createUrl('/provinsi/admin');?>">
                    <i class="fa fa-circle-o text-green"></i> <span>Provinsi</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo Yii::app()->createUrl('/kabupaten/admin');?>">
                    <i class="fa fa-circle-o text-green"></i> <span>Kota/Kabupaten</span>
                  </a>
                </li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>User Role</span>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo Yii::app()->createUrl('/user/admin');?>">
                    <i class="fa fa-user text-yellow"></i> <span>User</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo Yii::app()->createUrl('/role/admin');?>">
                    <i class="fa fa-cog text-yellow"></i> <span>Role</span>
                  </a>
                </li>
              </ul>
            </li>
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cogs"></i>
                <span>Setting</span>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="<?php echo Yii::app()->createUrl('/systemConfig/dataWebsite');?>">
                    <i class="fa fa-circle-o text-green"></i> <span>Data Website</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>