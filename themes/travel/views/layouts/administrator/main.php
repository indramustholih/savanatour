 <!-- Require the header -->
<?php include_once('tpl_header.php')?>

<!-- Require the navigation -->
<?php include_once('tpl_navigation.php')?>

<!-- Include content pages -->

<?php echo $content; ?>

<!-- Require the footer -->
<?php include_once('tpl_footer.php')?>