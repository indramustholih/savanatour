<!--footer start-->
  <footer id="footer_wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <aside class="widget widget_text"> 
	      <img width="50%" src="<?php echo $baseUrl;?>/images/logo.png" alt="footer logo" />
	      <?php echo substr(SystemConfig::model()->findByAttributes(array('key'=>'about_us'))->value,0,400);?>....
	      <p><?php echo CHtml::link('<i class="fa fa-angle-right"></i> Selengkapnya', array('/site/about'));?></p>
	  </aside>
          <aside class="widget widget_tag_cloud">
            <h4 class="widget-title"  style="margin-bottom: 20px !important;">Savana Tour Jogja</h4>
            <div class="tagcloud">
	      <ul>
		      <li style="padding-bottom: 10px;"><i class="fa fa-home"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'address'))->value;?>, <?php echo SystemConfig::model()->findByAttributes(array('key'=>'city'))->value;?> -
		      <?php echo SystemConfig::model()->findByAttributes(array('key'=>'provincies'))->value;?> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'zip_code'))->value;?></li>
		      <li style="padding-bottom: 10px;"><i class="fa fa-envelope"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'email'))->value;?></li>
		      <li style="padding-bottom: 10px;"><i class="fa fa-phone"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'phone'))->value;?></li>
		      <li style="padding-bottom: 10px;"><i class="fa fa-whatsapp"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'whatsapp'))->value;?></li>
	      </ul>
	    </div>
          </aside>
        </div>
        <div class="col-md-4">
          <aside class="widget widget_links">
            <h4 class="widget-title">Site Map</h4>
            <ul>
		<li><?php echo CHtml::link('Home', array('/site/index'));?></li>
		<li><?php echo CHtml::link('Tours', array('/site/tours'));?></li>
		<li><?php echo CHtml::link('Blog/News', array('/site/blog'));?></li>
		<li><?php echo CHtml::link('Gallery', array('/site/gallery'));?></li>
		<li><?php echo CHtml::link('About Us', array('/site/about'));?></li>
		<li><?php echo CHtml::link('Contact Us', array('/site/contact'));?></li>
		<li><?php echo CHtml::link('Disclaimer', array('/site/disclaimer'));?></li>
            </ul>
          </aside>
	  
          <aside class="widget widget_links">
            <h4 class="widget-title">Links</h4>
            <ul>
              <?php foreach(EksternalLink::model()->findAll() as $eksternalLink){?>
		<li><a href="<?php echo $eksternalLink->link;?>" target="_blank"><?php echo $eksternalLink->judul;?></a></li>
              <?php }?>
            </ul>
          </aside>
        </div>
        <div class="col-md-4">
          <aside class="widget widget_links">
	    <h4 class="widget-title">Facebook Panel</h4>
	    <?php echo SystemConfig::model()->findByAttributes(array('key'=>'facebook_panel'))->value;?>
	  </aside>
        </div>
      </div>
    </div>
  </footer>
  <!--footer end-->
