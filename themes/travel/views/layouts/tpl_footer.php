<?php include_once('top_footer.php');?>

  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6"> <span>Savana Tour Jogja @2016 | Stay Connected with Us - </span>
	  <a target="_blank" href="<?php echo SosmedAccount::model()->findByAttributes(array('sosial_media'=>'facebook'))->link;?>"><i class="fa fa-facebook"></i></a>
	  <a target="_blank" href="<?php echo SosmedAccount::model()->findByAttributes(array('sosial_media'=>'twitter'))->link;?>"><i class="fa fa-twitter"></i></a>
	  <a target="_blank" href="<?php echo SosmedAccount::model()->findByAttributes(array('sosial_media'=>'google'))->link;?>"><i class="fa fa-google-plus"></i></a>
	  <a target="_blank" href="<?php echo SosmedAccount::model()->findByAttributes(array('sosial_media'=>'google'))->link;?>"><i class="fa fa-instagram"></i></a>
	</div>
        <div class="col-md-6 col-sm-6 text-right"> <span><?php echo CHtml::link('Login', array('/site/administrator'));?> | Develop By <a href="http://digitakstudio.com" target="_blank" style="margin-left: -1px">Digital Data Studio</a>. All Right Reserved.</span> </div>
      </div>
    </div>
  </div>
</div>
<!--Page main section end--> 

    <!--main js file start--> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/jquery-1.11.3.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/bootstrap.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/bootstrap-select.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/datetimepicker/jquery.datetimepicker.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/parallax/jquery.parallax-1.1.3.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/owl/owl.carousel.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/isotope/jquery.isotope.js"></script> 
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/bxslider/jquery-bxslider.js"></script> 
    <!-- pie chart js -->
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/pie-circle/circles.js"></script> 
    <!-- pie chart js -->
    
    <!--counter js-->
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/counter/jquery.countTo.js"></script>
    
    <!--counter js-->
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/counter/jquery.countdown.js"></script>
    
	<!--Google Map-->
	<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCOipTWrmY490bgWR14Nsl1tl-7YbDMOnM"></script>
	    <script>
	    $(document).ready(function() {
		var myCenter=new google.maps.LatLng(<?php echo SystemConfig::model()->findByAttributes(array('key'=>'lat'))->value;?>, <?php echo SystemConfig::model()->findByAttributes(array('key'=>'long'))->value;?> );
	    function initialize()
	    {
	    var mapProp = {
	    center:myCenter,
	    zoom:7,
	    scrollwheel: false,
	    mapTypeId:google.maps.MapTypeId.ROADMAP
	    };
	    var map=new google.maps.Map(document.getElementById("bigth_googleMap"),mapProp);
	    var icon = { 
	    url: '<?php echo $baseUrl?>/images/map_marke_icon.png'
	    };
	    var marker=new google.maps.Marker({
	    position:myCenter,
	    map: map,
	    title: 'Himanshu Softtech',
	    icon: icon
	    });
	    marker.setMap(map);
	    var infowindow = new google.maps.InfoWindow({
	    content:"<?php echo SystemConfig::model()->findByAttributes(array('key'=>'address'))->value;?>, <?php echo SystemConfig::model()->findByAttributes(array('key'=>'city'))->value;?>, <?php echo SystemConfig::model()->findByAttributes(array('key'=>'provincies'))->value;?>, <?php echo SystemConfig::model()->findByAttributes(array('key'=>'contry'))->value;?>"
	    });
	    google.maps.event.addListener(marker, 'click', function() {
	    infowindow.open(map,marker);
	    });
	    }
	    google.maps.event.addDomListener(window, 'load', initialize);
	    });
    </script>
	
	
	
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/revolution/js/jquery.themepunch.tools.min.js">
    </script>
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/revolution/js/jquery.themepunch.revolution.min.js">
    </script>
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/revolution/js/revolution.extension.layeranimation.min.js">
    </script>
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/revolution/js/revolution.extension.navigation.min.js">
    </script>
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/revolution/js/revolution.extension.slideanims.min.js">
    </script>
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/revolution/js/revolution.extension.actions.min.js">
    </script>
    <script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/revolution/js/revolution.extension.parallax.min.js">
    </script>
    <!-- REVOLUTION JS FiLES -->
	<!-- video_popup -->
	<script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/video-popup/jquery.magnific-popup.js">
    </script>
	<!-- video_popup -->
	<!-- slick slider -->
	<script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/slick/jquery-migrate-1.2.1.min.js">
    </script>
	<script type="text/javascript" src="<?php echo $baseUrl?>/js/plugin/slick/slick.min.js"></script>
	<!-- slick slider -->
	<!-- video player js -->
	<script src="<?php echo $baseUrl?>/js/plugin/video_player/mediaelement-and-player.min.js"></script>
	<!-- video player js -->
	<!-- pricefilter -->
	<script src="<?php echo $baseUrl?>/js/plugin/jquery-ui/jquery-ui.js"></script>
	<!-- pricefilter-->
	<script type="text/javascript" src="<?php echo $baseUrl?>/js/custom.js"></script> 
	<!--main js file end-->
	
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.6";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>

</body>
</html>
