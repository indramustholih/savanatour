<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta name="description"  content="<?php echo CHtml::encode($this->pageDescription); ?>"/>
  <meta name="keywords" content="<?php echo CHtml::encode($this->pageKeyword); ?>"/>
  <meta name="author"  content="Bapontar"/>
  <meta name="MobileOptimized" content="320">
    
  <?php $baseUrl = Yii::app()->theme->baseUrl; ?>
  <!--srart theme style -->
  <link href="<?php echo $baseUrl?>/css/main.css" rel="stylesheet" type="text/css"/>
  <link href="<?php echo $baseUrl?>/css/custom.css" rel="stylesheet" type="text/css"/>
  <!-- end theme style -->
  
  <!-- favicon links -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $baseUrl;?>/images/favico/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $baseUrl;?>/images/favico/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $baseUrl;?>/images/favico/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $baseUrl;?>/images/favico/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $baseUrl;?>/images/favico/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $baseUrl;?>/images/favico/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $baseUrl;?>/images/favico/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $baseUrl;?>/images/favico/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $baseUrl;?>/images/favico/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $baseUrl;?>/images/favico/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $baseUrl;?>/images/favico/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $baseUrl;?>/images/favico/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl;?>/images/favico/favicon-16x16.png">
  <link rel="manifest" href="<?php echo $baseUrl;?>/images/favico/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo $baseUrl;?>/images/favico/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
    
    <script>
      //<![CDATA[
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-78082957-1', 'auto');
      ga('send', 'pageview');
      //]]>
    </script>
</head>

<body class="<?php if($this->pageTitle == 'Savana Tour Jogja - Home') echo 'travel_home';?>">
    <?php include_once('header.php');?>