<?php
  $this->pageTitle ="Savana Tour Jogja - Blog/News";
?>
<!--content body start-->
  <div id="content_wrapper"> 
    <!--page title start-->
    <div class="page_title" data-stellar-background-ratio="0" data-stellar-vertical-offset="0" style="background-image:url(<?php echo Yii::app()->theme->baseUrl.'/images/header-top-2.jpg';?>);">
      <ul>
        <li>Blog</li>
        <li>News</li>
      </ul>
    </div>
    <!--page title end-->
    <div class="clearfix"></div>
    <div class="container">
      <div class="row push-down-100">
        <div class="col-md-3">
          <div class="travelite_left_sidebar">
            <?php $this->renderPartial('_sideTourList'); ?>
            
            <aside class="widget hotel_widgets" style="text-align: center">
              <?php echo SystemConfig::model()->findByAttributes(array('key'=>'v_banner_wego_1'))->value;?>
            </aside>
            
            <div class="google-adds">
              <?php echo SystemConfig::model()->findByAttributes(array('key'=>'g_adds_2'))->value;?>
            </div>
          </div>
        </div>
        <div class="col-md-9">
          <div class="post_wrapper">
            <div class="travel_loader">
              <img src="<?php echo Yii::app()->theme->baseUrl;?>/images/icon/travel_loader.gif" alt="" /> </div>
            	<h3><i class="fa fa-tags"></i> <?php echo $key;?></h3>			
		<?php $this->widget('booster.widgets.TbListView',array(
				    'dataProvider'=>$dataProvider,
				    'itemView'=>'_blogList',
				)); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--content body end--> 