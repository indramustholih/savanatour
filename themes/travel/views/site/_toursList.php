<!--sort_list start -->
<div class="sorting_places_wrap  list_sorting_view">
 <div class="col-lg-5 col-md-5 col-sm-5 padding_none">
 <div class="thumb_wrape">
 <img src="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.TourPhoto::model()->findByAttributes(array('tour_id'=>$data->id, 'cover'=>1))->photo;?>" class="img-responsive" alt="list thumb">
 </div>
 </div>
 
  <div class="col-lg-7 col-md-7 col-sm-7">
 <div class="top_head_bar">
 <h4><?php echo CHtml::link($data->judul, array('/site/tourDetail', 'id'=>$data->id, 'title'=>$data->judul));?></h4>
 <span class="time_date"><i class="fa fa-clock-o"></i><?php echo $data->durasi;?></span>
 </div>
 <div class="bottom_desc">
 <h5>Starting from<span><?php echo $data->harga_paket;?></span></h5>
  <!-- desc icons Start-->
 <ul class="sort_place_icons">
 <?php if($data->local_transport == 1){?>
  <li><i class="fa fa-car"></i> Transport</li>
 <?php }?>
 <?php if($data->pesawat == 1){?>
  <li><i class="fa fa-plane"></i> Flight</li>
 <?php }?>
 <?php if($data->tiketing == 1){?>
  <li><i class="fa fa-binoculars"></i> Ticketing</li>
 <?php }?>
 <?php if($data->makan == 1){?>
  <li><i class="fa fa-cutlery"></i> Food</li>
 <?php }?>
 <?php if($data->akomodasi == 1){?>
  <li><i class="fa fa-building-o"></i> Hotel</li>
 <?php }?>
 </ul>
  <!-- desc icons End-->
      <div>
        <?php echo CHtml::link('Selengkapnya', array('/site/tourDetail', 'id'=>$data->id , 'title'=>$data->judul), array('class'=>'list_view_details btns'));?>
      
      </div>
 </div>
 </div>
 </div>
 <!--sort_list start end-->