<aside class="widget hotel_widgets">
<h4 class="widget-title">Top Destination</h4>
<ul>
		     <?php foreach(Tour::model()->findAllByAttributes(array('top'=>'1', 'publish'=>1), array('limit'=>5, 'order'=>'id DESC')) as $tour){;?>  
		     <li> <img src="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.Tour::model()->getCover($tour->id);?>" alt="<?php echo $tour->judul;?>">
		       <div>
			 <h4 class="hover-link"><?php echo CHtml::link($tour->judul, array('site/tourDetail', 'id'=>$tour->id, 'title'=>str_replace(' ', '-', $tour->judul)));?></h4>
			 <p>start from <span><?php echo $tour->harga_paket;?></span></p>
			 
			   
			 <?php if($tour->local_transport == 1){?>
			  <i class="fa fa-car"></i>
			 <?php }?>
			 <?php if($tour->pesawat == 1){?>
			  <i class="fa fa-plane"></i>
			 <?php }?>
			 <?php if($tour->tiketing == 1){?>
			  <i class="fa fa-binoculars"></i>
			 <?php }?>
			 <?php if($tour->makan == 1){?>
			  <i class="fa fa-cutlery"></i>
			 <?php }?>
			 <?php if($tour->akomodasi == 1){?>
			  <i class="fa fa-building"></i>
			 <?php }?>
			
		       
		       </div>
		    </li>
		     <?php }?>
			 	     
</ul>			 
</aside>

<aside class="widget hotel_widgets">
<h4 class="widget-title">Latest Destination</h4>
<ul>
		     <?php foreach(Tour::model()->findAllByAttributes(array('top'=>'0', 'publish'=>1), array('limit'=>5, 'order'=>'id DESC')) as $tour2){;?>  
		     <li> <img src="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.Tour::model()->getCover($tour2->id);?>" alt="<?php echo $tour2->judul;?>">
		       <div>
			 <h4 class="hover-link"><?php echo CHtml::link($tour2->judul, array('site/tourDetail', 'id'=>$tour2->id, 'title'=>str_replace(' ', '-', $tour2->judul)));?></h4>
			 <p>start from <span><?php echo $tour2->harga_paket;?></span></p>
			 
			   
			 <?php if($tour2->local_transport == 1){?>
			  <i class="fa fa-car"></i>
			 <?php }?>
			 <?php if($tour2->pesawat == 1){?>
			  <i class="fa fa-plane"></i>
			 <?php }?>
			 <?php if($tour2->tiketing == 1){?>
			  <i class="fa fa-binoculars"></i>
			 <?php }?>
			 <?php if($tour2->makan == 1){?>
			  <i class="fa fa-cutlery"></i>
			 <?php }?>
			 <?php if($tour2->akomodasi == 1){?>
			  <i class="fa fa-building"></i>
			 <?php }?>
			
		       
		       </div>
		    </li>
		     <?php }?>
			 	     
</ul>			 
</aside>
<div class="travelite_left_sidebar_second" style="margin-bottom: 20px;">
<aside class="widget hotel_booking_widget" style="padding-bottom: 0; padding-top: 0; border-bottom: 1px solid #eeeeee;">
<h4 class="widget-title">Informasi Lebih Lanjut</h4>
<div class="widgett text_widget">
<h3><i class="fa fa-phone"></i><?php echo SystemConfig::model()->findByAttributes(array('key'=>'phone'))->value;?></h3>			
</div>			 
</aside>
</div>
