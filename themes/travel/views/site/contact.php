<?php $this->pageTitle ="Contact Us - Savana Tour Jogja";?>
<!--content body start-->
  <div id="content_wrapper"> 
    <!--page title start-->
    <div class="page_title" data-stellar-background-ratio="0" data-stellar-vertical-offset="0" style="background-image:url(<?php echo Yii::app()->theme->baseUrl.'/images/header-top-3.jpg';?>);">
      <ul>
        <li><a href="javascript:;">contact us</a></li>
      </ul>
    </div>
    <!--page title end-->
    <div class="clearfix"></div>
    
	<!-- contact map section start -->
        <div class="full_width tr_contact_map_section">		
            <div class="map_main">
		<div id="bigth_googleMap"></div>	
	    </div>
		
	</div>
	<!-- contact map section End -->
	
	<!-- contact details section start -->
      <div class="full_width tr_contact_detais_section">
		  <div class="container">
		    <div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
				  <?php if(Yii::app()->user->hasFlash('contact')): ?>
					<div class="flash-success">
						<p><?php echo Yii::app()->user->getFlash('contact'); ?></p>
					</div>
					<?php else: ?>
					  <p>Jika Anda memiliki pertanyaan, silakan isi form berikut untuk menghubungi kami. <br>Terima kasih.</p>					
					
					    <div class="conatact_form_ds">
						    <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
								'id'=>'inbox-form',
								'enableAjaxValidation'=>false,
								'action' => array('/site/contact'),
							)); ?>
							
							    <input type="text" name="Inbox[name]" placeholder="Nama *" class="input_c" required="required">
							    <input type="email" name="Inbox[email]" placeholder="Email *" class="input_c" required="required">
							    <input type="text" name="Inbox[phone]" placeholder="Telepon" class="input_c" >
							    <input type="text" name="Inbox[subjek]" placeholder="Subjek *" class="input_c" required="required">
							    <textarea name="Inbox[message]" placeholder="Pesan *" class="text_area_c"></textarea>							    
							    <input type="text" name="Inbox[captcha]" placeholder="Isi Dengan <?php echo $a.' + '.$b;?> *" onKeyUp="verify(this.value)" class="input_c" required="required">							
							    <input id="verifyNum" type="hidden" value="<?php echo $a+$b;?>">
							    <input type="submit" value="Send" class="btn-yellow" id="form_submit" disabled>
						    <?php $this->endWidget(); ?>
						    
					    </div>
					<?php endif; ?>
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 right">
				<div class="row">
					<div class="col-lg-6 col-md-7 col-sm-8 col-lg-offset-2">
					
					  <div class="address_contact_details">
					  <div class="address_detais_city">
					    <b>Savana Tour Jogja</b><hr>
					  </div>
						<ul>
							<li><i class="fa fa-home"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'address'))->value;?>, <?php echo SystemConfig::model()->findByAttributes(array('key'=>'city'))->value;?> -
							<?php echo SystemConfig::model()->findByAttributes(array('key'=>'provincies'))->value;?> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'zip_code'))->value;?></li>
							<li><i class="fa fa-envelope"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'email'))->value;?></li>
							<li><i class="fa fa-phone"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'phone'))->value;?></li>
							<li><i class="fa fa-whatsapp"></i> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'whatsapp'))->value;?></li>
						</ul>
					  </div>
					 
					</div>
				
					 
					<div class="col-lg-4 col-md-5 col-sm-4 t_align_c">
					 <!-- facebook squre start -->
					<div class="social_box facebook_b_wrap">
						<a target="_blank" href="<?php echo SosmedAccount::model()->findByAttributes(array('sosial_media'=>'facebook'))->link;?>">
						<i class="fa fa-facebook-square"></i></a>
						<div class="social_likes">Follow Our</div>
						<div class="shares_and_likes">Facebook</div>
					</div>
					<!-- facebook squre End -->
					
						<!-- twitter squre start -->
						<div class="social_box twitter_b_wrap">
							<a target="_blank" href="<?php echo SosmedAccount::model()->findByAttributes(array('sosial_media'=>'twitter'))->link;?>">
							<i class="fa fa-twitter-square"></i></a>
							<div class="social_likes">Follow Our</div>
							<div class="shares_and_likes">Twitter</div>
						</div>
						<!-- twitter squre End -->
						
						<!-- RSS squre start -->
						<div class="social_box google_plus_b_wrap" style="background-color: #DD4C40">
							<a target="_blank" href="<?php echo SosmedAccount::model()->findByAttributes(array('sosial_media'=>'google'))->link;?>">
							<i class="fa fa-google-plus-square"></i></a>
							<div class="social_likes">Follow Our</div>
							<div class="shares_and_likes">Google +</div>
						</div>
						<!-- Instagram squre End -->
						
						<!-- Linkedin squre start -->
						<div class="social_box linkedin_b_wrap" style="background-color: #A67B5C">
							<a href="<?php echo SystemConfig::model()->findByAttributes(array('key'=>'sosmed_linkedin'))->value;?>"><i class="fa fa-instagram"></i></a>
							<div class="social_likes">Follow Our</div>
							<div class="shares_and_likes">Instagram</div>
						</div>
						<!-- Instagram squre End -->
						
					</div>
				</div>
				</div>
			</div>
		  </div>
		 
		  
	  </div>
	  <!-- contact details section End -->
	
<!-- contact section End -->

<!-- counter section End -->
	  
  </div>
  <!--content body end-->
  

<script language="javascript" type="text/javascript">
  function verify(nilai) {
    var verify_num = parseInt($("#verifyNum").val());
    if (nilai == verify_num) {
      document.getElementById("form_submit").disabled = false;
    }else{
      document.getElementById("form_submit").disabled = true;
    }
    
  }
</script>