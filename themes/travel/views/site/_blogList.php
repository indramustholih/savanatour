<div class="travel_post">
<h3><?php echo CHtml::link($data->judul, array('/site/blogDetail', 'id'=>$data->id, 'title'=>str_replace(' ', '-', $data->judul)));?></h3>
<div class="travel_meta">
  <ul>
    <li><?php echo Tools::getDateTime($data->create_time);?></li>
    <li><a href=""><i class="fa fa-heart"></i> Likes</a></li>
    <li><a href=""><i class="fa fa-eye"></i> Viewer</a></li>
    <li><i class="fa fa-tags"></i> <?php echo $data->tags;?></li>
  </ul>
</div>
<img src="<?php echo Yii::app()->request->baseUrl.'/images/blog/'.$data->cover_img;?>" alt="<?php echo $data->judul;?>" />
<p><?php echo substr(strip_tags($data->konten),0,250);?>...</p>
<?php echo CHtml::link('Selanjutnya', array('/site/blogDetail', 'id'=>$data->id, 'title'=>str_replace(' ', '-', $data->judul)), array('class'=>'btn-travel btn-yellow'));?>
</div>