<!--content body start-->
  <div id="content_wrapper"> 
    <div class="clearfix"></div>
	
	<div class="slider_tab_main">
	<!-- Home first slider start -->
  <div class="full_width home_slider">
	  <div class="example">
		  <article class="content" style="width:100%; float:left;">
			  <div id="rev_slider_116_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="layer-animations" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
				  <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
				  <div id="rev_slider_116_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
					  <ul style="width:100% !important;" class="home_3_slide">
						  <!-- SLIDE  -->
						    <?php foreach(Slider::model()->findAllByAttributes(array('publish'=>1)) as $slider){?>
						  <li data-index="rs-<?php echo $slider->id?>" data-transition="parallaxhorizontal" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-rotate="0" data-saveperformance="off" data-title="Smooth Mask" data-description="">
							  <!-- MAIN IMAGE -->
							  <img src="<?php echo Yii::app()->request->baseUrl.'/images/slider/'.$slider->image;?>" alt="slide2" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
							  <!-- LAYERS -->
		
							 <div class="slider_heading_wrap">
							  <!-- LAYER NR. 1 -->
							  <div class="tp-caption NotGeneric-Title   tp-resizeme" 
									 id="slide-<?php echo $slider->id?>-layer-1" 
									 data-x="930" data-hoffset="" 
									 data-y="center" data-voffset="-150" 
												data-width="['auto','auto','auto','auto']"
									data-height="['auto','auto','auto','auto']"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:top;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-start="3000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" style="text-align:center;">
									
									<div style="line-height:60px;">
									<img src="themes/travel/images/icon_slider/slider_icon1.png" alt="icon1" >
									</div>
									
								</div>
								<!-- LAYER NR. 1 -->
									 <!--LAYER 2 START-->
									  <div class="tp-caption NotGeneric-Title   tp-resizeme" 
									 id="slide-<?php echo $slider->id?>-layer-2" 
									 data-x="1000" data-hoffset="" 
									 data-y="center" data-voffset="-70" 
												data-width="['auto','auto','auto','auto']"
									data-height="['auto','auto','auto','auto']"
									data-transform_idle="o:1;"
						 
									 data-transform_in="x:right;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									 
									data-start="4000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" style="text-align:center;">
									
									<div style="line-height:60px;">
									<img src="themes/travel/images/icon_slider/slider_icon2.png" alt="icon1" >
									</div>
									
								</div>
					           <!-- LAYER 2 END -->
							   <!-- LAYER NR. 3 -->
							 <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-<?php echo $slider->id?>-layer-3"
							 
							 data-x="1000" data-hoffset="" 
									 data-y="center" data-voffset="30" 
												data-width="['auto','auto','auto','auto']"
									data-height="['auto','auto','auto','auto']"
									data-transform_idle="o:1;"
						 
									 data-transform_in="x:right;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-start="5000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" >
							<div style="line-height:60px;">
									<img src="themes/travel/images/icon_slider/slider_icon3.png" alt="icon1" >
									</div>
									 </div>
									 
									 <!-- LAYER 3 END -->
									 
									  <!-- LAYER NR. 4 -->
							 <div class="tp-caption NotGeneric-Title   tp-resizeme" id="slide-<?php echo $slider->id?>-layer-4"
							 
							 data-x="930" data-hoffset="" 
									 data-y="center" data-voffset="100" 
												data-width="['auto','auto','auto','auto']"
									data-height="['auto','auto','auto','auto']"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
							    data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
									 
									data-start="6000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" >
							<div style="line-height:60px;">
									<img src="themes/travel/images/icon_slider/slider_icon4.png" alt="icon1" >
									</div>
									 </div>
									 
									 <!--LAYER 4 END-->
									 
									 <!-- LAYER NR. 5 -->
							  <div class="tp-caption NotGeneric-Title   tp-resizeme" 
									 id="slide-<?php echo$slider->id ?>-layer-5" 
									 data-x="830" data-hoffset="" 
									 data-y="center" data-voffset="-10" 
												data-width="['auto','auto','auto','auto']"
									data-height="['auto','auto','auto','auto']"
									data-transform_idle="o:1;"
						 
									 data-transform_in="y:top;s:2000;e:Power4.easeInOut;" 
									 data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
									data-start="2000" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" style="text-align:center;">
									
									<div style="line-height:60px;">
									<img src="themes/travel/images/icon_slider/slider_icon5.png" alt="icon1" >
									</div>
									
								</div>
								<!-- LAYER NR. End -->
								
									 <!-- LAYER NR. 6 -->
							  <div class="tp-caption NotGeneric-Title   tp-resizeme" 
									 id="slide-<?php echo $slider->id?>-layer-6" 
									 data-x="580" data-hoffset="" 
									 data-y="center" data-voffset="40" 
												data-width="['auto','auto','auto','auto']"
									data-height="['auto','auto','auto','auto']"
									data-transform_idle="o:1;"
						 
									  data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
									 
									data-start ="1500" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" style="text-align:center; right:300px">
									
									
									    	<?php echo $slider->layer_1;?></center>
									
									
								</div>
								<!-- LAYER NR. End -->
								
								
								 <!-- LAYER NR. 7 -->
							  <div class="tp-caption NotGeneric-Title tp-resizeme" 
									 id="slide-<?php echo $slider->id?>-layer-7" 
									 data-x="600" data-hoffset="" 
									 data-y="center" data-voffset="180" 
												data-width="['auto','auto','auto','auto']"
									data-height="['auto','auto','auto','auto']"
									data-transform_idle="o:1;"
						 
									data-transform_in="y:50px;opacity:0;s:1500;e:Power3.easeOut;" 
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" 
									 
									data-start="7500" 
									data-splitin="none" 
									data-splitout="none" 
									data-responsive_offset="on" style="text-align:center; right:300px ">
									<br>
									<div  style="background-color:#000; opacity:0.8;" >

									  <p style="line-height:60px; font-size:60px; "><?php echo $slider->layer_2?></p>
									  
									</div>
									
								</div>
								<!-- LAYER NR. End -->
								
							
								
							  </div>
							  
							  <!-- LAYER end -->
							  
						  </li>
						  <!--slide-->
						  <?php }?>
						  
						  
						  
						  
					  </ul>
				  </div>
			  </div>
			  <!-- END REVOLUTION SLIDER -->
		  </article>
	  </div>
	  <!-- section end -->
    </div>
  <!-- Home first slider End -->
  <?php /*
  <div class="travelite_slider_menus">
      <div class="container">
      <div class="slider_menus">
       <ul>
        <li><a href="#">Price Guarantee</a></li>
        <li><a href="#">Best Tour</a></li>
        <li><a href="#">Trust & Safety</a></li>
        <li><a href="#">Good Guidance</a></li>
       </ul>
      </div>
    </div>
  </div>
    */?>
  </div><!-- slider main wrapper end -->