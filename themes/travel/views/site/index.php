<?php

$this->pageTitle = 'Home - Tour Organizer - Tiket Murah - Savana Tour Jogja'; 
?>
<?php $this->renderPartial('_slider'); ?>

<!-- feature section start -->
	  <div class="full_width travelite_feature_section">
		  <div class="container">
			  <div class="row">
			
				<?php 
				foreach(Tour::model()->findAllByAttributes(array('top'=>'1', 'publish'=>1), array('limit'=>3, 'order'=>'id DESC')) as $tour){;?>  
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class="feature_box_second">
						 <img id="img-tour-top" src="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.Tour::model()->getCover($tour->id);?>" class="img-responsive" alt="<?php echo $tour->judul;?>">
							<div class="feature_overlay_second" style="padding: 8px 3px;">
								 <div class="bottom_first"><?php echo CHtml::link($tour->judul, array('/site/tourDetail', 'id'=>$tour->id, 'title'=>str_replace(' ', '-', $tour->judul)));?><span><?php echo $tour->durasi;?></span></div>
								 <div class="bottom_second">start from <span><?php echo $tour->harga_paket;?></span></div>
							      </div>
						</div>
					</div>
				<?php }?>  
				  
				  <div class="col-lg-3 col-md-3 col-sm-6">
					  <div class="end_box_fearure" style="padding-top: 0px" style="">
						 <h3 class="travelite_heading_feature" ><font style="">top destination</font></h3>
						  <p>Jangan lewatkan paket perjalanan menarik lainnya bersama kami. </p>
						  <div> <?php echo CHtml::link('Selengkapnya', array('/site/tours'), array('class'=>'black_btn'));?></div>
					  </div>
				  </div>
				  
			  </div>
		  </div>
	  </div>
	  <!-- feature section end -->
	  
	 <!--  special offer section start -->
 	 <div class="full_width travelite_world_section">
		<div class="container">
			<div class="row">
			
				<div class="heading_team">
					<h3>Latest Destination</h3>
					<p>Perjalanan wisata anda akan lebih menarik bersama kami.</p>
				</div>
				<?php foreach(Tour::model()->findAllByAttributes(array('top'=>'0', 'publish'=>1), array('limit'=>4)) as $tour2){;?>  
					<!-- first ractangle start -->
					<div class="col-lg-3 col-md-3 col-sm-6">
						<div class=" full_width offer_box_wrapper">
					    	
							<div class="img_overlay_wrapper">
							<img id="img-tour-off" src="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.Tour::model()->getCover($tour2->id);?>" class="img-responsive" alt="<?php echo $tour2->judul;?>">
							<div class="img_overlay">
							</div>
							</div>
							<div class="full_width offer_inner">
								<h4><?php echo $tour2->judul;?></h4>
								<b><?php echo $tour2->durasi;?></b>
								<div class="overlay_btn">
									<?php echo CHtml::link('Lihat', array('/site/tourDetail', 'id'=>$tour2->id, 'title'=>str_replace(' ', '-', $tour2->judul)), array('class'=>'green_btn'));?>
								</div>
								
							</div>
						</div>
					</div>
					<!-- first ractangle End -->
				<?php }?>	
					
				<div class="full_width destination_button">
				<?php echo CHtml::link('Paket Lainnya', array('/site/tours'), array('class'=>'black_btn feature_more_btn'));?>
				</div>
			
			</div>
		</div>
	</div>
	 <!--  special offer section End -->

   <!-- video section start -->
	  <div class="full_width home_video_section">
	  <div class="video_overlay">
		  <div class="container">
			  <div class="row">
			  <div class="video_popup">
			   <a class="popup_video_swm" href="<?php echo SystemConfig::model()->findByAttributes(array('key'=>'homepage_video'))->value;?>">
				<i class="fa fa-play-circle"></i>
			   </a>
		
				<h3 class="video_text">Watch Our Tourist Experience</h3>
				</div>
			  </div>
		  </div>
		 </div>
	  </div>
	  <!-- video section End -->
	  
	  <!-- latest news section start -->
	<div class="full_width latest_news_section">
		  <div class="container">
			  <div class="row row_top">
			  <div class="heading_team">
					<h3>Latest Blog/News</h3>
					<?php /*<p>Create stunning pages with our powerful admin panel. Functionality and usability combine. Travelllers Deals and Offers on Hotels, Vacation Packages, Flights, Cruises and Car Rentals</p>*/?>
				</div>
				<?php foreach(Blog::model()->findAllByAttributes(array('publish'=>'1'), array('limit'=>4, 'order'=>'id DESC')) as $blog){;?>  
					
					<!--  news box start -->
					<div class="col-lg-3 col-md-3 col-sm-6">
					<div class="latest_news_wrapper">
					<div class="news_thumb_wrapper">
						<img id="img-news" src="<?php echo Yii::app()->request->baseUrl.'/images/blog/'.$blog->cover_img;?>" alt="<?php echo $blog->judul;?>" class="img-responsive"/>
					<div class="latest_news_overlay">
					<span class="date"><?php echo Tools::getDate($blog->create_time);?><br/> <?php echo Tools::getMonthName(Tools::getDate($blog->create_time, 2));?></span>
					<?php /*<span class="icon_img"><i class="fa fa-picture-o"></i></span>*/?>
					</div>
					</div>
					<div class="latest_news_desc">
					<h4><?php echo CHtml::link($blog->judul, array('/site/blogDetail', 'id'=>$blog->id, 'title'=>str_replace(' ', '-', $blog->judul)));?></h4>
					<div class="news_heading_tag"><i class="fa fa-tag"></i> <?php echo $blog->blogKategori->kategori;?></div>
					<p><?php echo substr(strip_tags($blog->konten),0,250);?>...</p>
					<div class="news_read_more">
					<?php echo CHtml::link('Selengkapnya', array('/site/blogDetail', 'id'=>$blog->id, 'title'=>str_replace(' ', '-', $blog->judul)), array('class'=>'btn-travel btn-yellow'));?>
					</div>
					</div>
					</div>
					</div>
					<!--  news box End -->
				<?php }?>
			  </div>
		  </div>
	</div>	  
	<!-- latest news section end -->
	  
	  <!-- subscribe section start -->
	  <div class="full_width home_subscribe_section">
	  <div class="icon_circle_overlay"></div>
		  <div class="container">
			  <div class="row">
			   <div class="col-lg-12">
			   <div class="subscribe_middle_part">
			     <h3><i>"Remember that happiness is a way of travel, not a destination."</i></h3>- Roy M. Goodman
			    </div>
			  </div>
			  </div>
		  </div>
      </div>
<!-- subscribe section End -->
  </div>
  <!--content body end--> 