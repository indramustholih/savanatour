<div class="portfolio-item col-md-4 col-sm-4 col-xs-12 portfolio-custom <?php echo $data->tour->tourKategori->kategori;?>">
<figure>
  <a href="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.$data->photo;?>" class="zoom-item" title="<?php echo $data->tour->judul;?>"> 
  <img id="img-gal" src="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.$data->photo;?>" alt="<?php echo $data->tour->judul;?>"> </a> 
</figure>
<div class="portfolio-content">
  <div class="portfolio-meta"> <a href="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.$data->photo;?>"><i class="fa fa-search-plus"></i></a>
    <?php $tour = Tour::model()->findByPk($data->tour_id);?>
    <h2 class="portfolio-title"> <?php echo CHtml::link($tour->judul, array('site/tourDetail', 'id'=>$tour->id, 'title'=>$tour->judul));?></h2>
  </div>
  <!-- End .portfolio-meta --> 
</div>
<!-- End .portfolio-content --> 
</div>
<!-- End .portfolio-item -->