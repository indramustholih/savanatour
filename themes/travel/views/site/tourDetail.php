<?php
    $this->pageTitle ='Savana Tour Jogja - Tours : '.$model->judul.' '.$model->durasi.' '.$model->harga_paket;
    $this->pageDescription = $model->judul.' '.$model->durasi.' '.$model->harga_paket;
    $this->pageKeyword = $model->judul.', '.$this->pageKeyword;
?>
<!--content body start-->
  <div id="content_wrapper">
  <!--page title Start-->
  <div class="page_title" data-stellar-background-ratio="0" data-stellar-vertical-offset="0" style="background-image:url(<?php echo Yii::app()->theme->baseUrl.'/images/header-top-4.jpg';?>);">
      <ul>
        <li><a href="javascript:;">Tour Destination</a></li>
      </ul>
    </div>
    <!--page title end-->
    <div class="clearfix"></div>
	
	<div class="full_width destinaion_sorting_section">
	  <div class="container">
	   <div class="row space_100">
	   
	     <!-- left sidebar start -->
	     <div class="col-lg-3 col-md-3 col-sm-12">
	       <div class="travelite_left_sidebar_second">
		   <?php $this->renderPartial('_sideTourList'); ?>
		   <?php $this->renderPartial('_sideBlogList'); ?>
		      <aside class="widget hotel_widgets" style="text-align: center; border: 0">
			<?php echo SystemConfig::model()->findByAttributes(array('key'=>'v_banner_wego_1'))->value;?>
		      </aside>
		</div>
	     </div>
	      <!-- left sidebar end -->
	      
	     <!-- right main start -->
		<div class="col-lg-9 col-md-9 col-sm-12">
		<div class="tour_packages_right_section left_space_40">
		<div class="tour_packages_details_top">
		
		<div class="top_head_bar">
		   <h4><?php echo CHtml::link($model->judul, array('site/tourDetail', 'id'=>$model->id, 'title'=>$model->judul));?></h4>
		   </div>
		   <div class="bottom_desc">
		   <h5 class="starting_text">Starting from<span><?php echo $model->harga_paket;?></span></h5>
		    <span class="time_date"><i class="fa fa-clock-o"></i><?php echo $model->durasi;?></span>
		   <h5 class="includes_text">Fasilitas:</h5>
		    <!-- desc icons Start-->
		    <div class="row">
			<div class ="col-lg-6 top_icons_part">			
			    <ul class="sort_place_icons">
			    <?php if($model->local_transport == 1){?>
			     <li><i class="fa fa-car"></i> Transport</li>
			    <?php }?>
			    <?php if($model->pesawat == 1){?>
			     <li><i class="fa fa-plane"></i> Flight</li>
			    <?php }?>
			    <?php if($model->tiketing == 1){?>
			     <li><i class="fa fa-binoculars"></i> Ticketing</li>
			    <?php }?>
			    <?php if($model->makan == 1){?>
			     <li><i class="fa fa-cutlery"></i> Food</li>
			    <?php }?>
			    <?php if($model->akomodasi == 1){?>
			     <li><i class="fa fa-building-o"></i> Hotel</li>
			    <?php }?>
			    </ul>
		    <!-- desc icons End-->
			</div>
			
			<div class="col-lg-6">
			
			
			</div>
		
		    </div>
		   		
		</div>
		</div>
		     <!-- slider start -->
		   <div class="package_details_slider">
			<div id="package_details_slider" class="owl-carousel owl-theme">
                            <?php foreach(TourPhoto::model()->findAllByAttributes(array('tour_id'=>$model->id)) as $photo){?>
                                <div class="item"><img src="<?php echo Yii::app()->request->baseUrl.'/images/tours/'.$photo->photo;?>" alt="<?php echo $model->judul;?>"></div>
                            <?php }?>
                          
			</div>
		   </div>
		     <!-- slider end -->
			 
			 <!-- Booking area Start-->
          <div class="booking_area_section">
            <?php echo $model->deskripsi;?>
			 
			   <?php /* 
                         <div class=" full_width booking_form_bg">
			
                         <div class="main_content_form">
			
                         <!-- tab_search form start -->
			 <form>
				<div class="pullleft check_in_field">
				<label>available on</label>
				<input type="text" id="Check_out_date_tab" placeholder="dd/mm/yyyy">
					<i class="fa fa-calendar"></i>
				</div>
				
				<div class="pullleft room_select_field">
					<label>adults</label>
					<select class="form-control selectpicker" data-live-search="true" id="search_adults">
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
					</select>
					<i class="fa fa-caret-down"></i>
				</div>
				<div class="pullleft room_select_field">
					<label>kids</label>
					<select class="form-control selectpicker" data-live-search="true" id="search_kids">
						<option value="1">01</option>
						<option value="2">02</option>
						<option value="3">03</option>
						<option value="4">04</option>
					</select>
					<i class="fa fa-caret-down"></i> 
				</div>
				<div class="pullleft submit_field">
				<label>total:<span class="total_doller">$1000</span></label>
					<input type="submit" value="BOOK NOW" class="search_tabs">
					<button class="btn tab_search" type="submit"> <i class="glyphicon glyphicon-search"></i></button>
				</div>
				</form>
				<!-- tab_search form End -->
				
            </div>
			 
			 </div>
			 */?>

          </div>
		  <!-- Booking area End -->

		 <!-- package tabs start -->
		  <div class="full_width travelite_middle_tabs" id="travelite_middle_tabs">
		    <div class="pcg_tabs_panel">
				<ul>
					<li> <a href="#tab_search_1">Destinasi</a></li>
					<li> <a href="#tab_search_2" class="">Itenary</a></li>
					<li> <a href="#tab_search_3" class="">Include</a></li>
					<li> <a href="#tab_search_4" class="">Exclude</a></li>
				</ul>
            </div>
		                <!--  tab content start -->
			<div id="tab_search_1" class="tab_details_part">
                            <?php echo $model->destinasi;?>
			</div>
			 <!--  tab content End -->
			
			<!--  tab content start -->
			<div id="tab_search_2" class="tab_details_part">
			     <?php echo $model->itenary;?>
			</div>
			<!--  tab content End -->
                     
			  <!--  tab content start -->
			<div id="tab_search_3" class="tab_details_part">
                            <?php echo $model->include;?>
			</div>
			 <!--  tab content End -->
                         
			  <!--  tab content start -->
			<div id="tab_search_4" class="tab_details_part">
                            <?php echo $model->exclude;?>
			</div>
			 <!--  tab content End -->
		
	  </div>
	    <!-- package tabs End -->
	    <br>
	    <div class="post_share">
		<i class="fa fa-share-alt"></i>
		<span class="post_share_heading">Share :</span>
		<?php $this->widget('application.extensions.SocialShareButton.SocialShareButton', array(
			'style'=>'horizontal',
			'networks' => array('facebook','googleplus','twitter'),
			'data_via'=>'', //twitter username (for twitter only, if exists else leave empty)
		));?>
	    </div>
	    
	    <div class="google-adds">
              <?php echo SystemConfig::model()->findByAttributes(array('key'=>'g_adds_3'))->value;?>
            </div>
		
	   </div><!-- right main start -->	  
	</div> <!-- col-lg-9-end -->
	</div><!--  row main -->
  </div> <!-- container -->
  </div> <!-- main wrapper -->
  <!--content body end--> 