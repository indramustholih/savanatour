<?php $this->pageTitle ="About Us - Savana Tour Jogja";?>
<!--content body start-->
  <div id="content_wrapper"> 
    <!--page title start-->
    <div class="page_title" data-stellar-background-ratio="0" data-stellar-vertical-offset="0" style="background-image:url(<?php echo Yii::app()->theme->baseUrl.'/images/header-top-1.jpg';?>);">
      <ul>
        <li><a href="javascript:;">About us</a></li>
      </ul>
    </div>
    <!--page title end-->
    <div class="clearfix"></div>
    
	<!-- traveller story section start -->
      <section class="full_width ds_traveller_story">
		<div class="container">
			<div class="row">
				<div class="col-lg-5 col-md-5 col-sm-8 col-xs-8">
					<div class="left_side_slider">
						
						<ul class="about_slider">
							
							<li><img src="<?php echo Yii::app()->theme->baseUrl.'/images/img-style-1.jpg';?>" alt=""></li>
							<li><img src="<?php echo Yii::app()->theme->baseUrl.'/images/img-style-2.jpg';?>" alt=""></li>
							<li><img src="<?php echo Yii::app()->theme->baseUrl.'/images/img-style-3.jpg';?>" alt=""></li>
						
						</ul>
						
					</div>
					
					<div class="google-adds">
					  <?php echo SystemConfig::model()->findByAttributes(array('key'=>'g_adds_4'))->value;?>
					</div>
				</div>
				<div class="col-lg-7 col-md-7 col-sm-12 travelite_about_right_side">
				<div class="right_side_details">
					<h3>Short Story about <b class="traveller_text_b">Savana Tour Jogja</b></h3>
					<?php echo SystemConfig::model()->findByAttributes(array('key'=>'about_us'))->value;?>
					</div>
					<div class="full_width check_lists">
						<h4>Our Value :</h4>
						<ul>
							<li><label>integrity</label> <p>- Doing the right thing</p></li>
							<li><label>Responsible</label> <p>- Think globel, act local</p></li>
							<li><label>Growth</label> <p>- Stakeholders in our own success</p></li>
							<li><label>Innovation</label> <p>- Thriving on new ideas and embracing change</p></li>
							<li><label>Fun</label> <p>- It's engrained into who we are and what we do</p></li>
							<li><label>Passion</label> <p>- We are inspired by the work we do and the trips we offer</p></li>
						</ul>
						
					</div>
					<div class="pull_left btn_left">
						<?php echo CHtml::link('View our gallery', array('/site/gallery'), array('class'=>'btn-yellow gallery_btn'));?>
					</div>
				
				</div>
			</div>
			
		</div>
		
	  </section>
      <!-- traveller story section End -->
	   
      <!-- contact section Start -->
	<div class="full_width contact_section"style="background-color:#000">
		    <div class="container">
			    <div class="row" >
			     <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12">
			      <div class="tr_contact_wrap">
				    <h4>where you would like to go</h4>
				    <h3 class="tell_us">tell uswe will help you</h3>
			      </div>
			     </div>
				    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-lg-offset-1">
				    <div class="hello_wrapper">
				    <i class="fa fa-phone"></i>
					    <div class="tr_contact_wrap">
						    <h4>Say Hello</h4>
						    <h3 class="contact"> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'phone'))->value;?></h3>
					    </div>
				    </div>
				    </div>
				    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-lg-offset-1 ">
				    <div class="hello_wrapper">
				    <i class="fa fa-envelope"></i>
					    <div class="tr_contact_wrap">
						    <h4>Say Hi</h4>
						    <h3 class="contact"> <?php echo SystemConfig::model()->findByAttributes(array('key'=>'email'))->value;?></h3>
					    </div>
					    </div>
				    </div>
			    </div>
		</div>
	    </div>  
	  <!-- contact section End -->
	  <!-- counter section start -->
	  <div class="full_width counter_section">
		  <div class="container">
			  <div class="row">
			  
				  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				  <div class="timer" id="counter1" data-to="1580" data-delay="100" data-speed="3000">1580</div>
					  <div class="chart" data-animated="bounceIn" data-delay="100">
						  <div class="percentage-light" data-percent="80">
							<i class="fa fa-globe"></i>
						  </div>
						  <div class="counter_title">places to visit the world</div>
					  </div>
				  </div>
				  
				  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				  <div class="timer" id="counter2" data-to="1240" data-delay="100" data-speed="3000">1240</div>
					  <div class="chart" data-animated="bounceIn" data-delay="100">
						  <div class="percentage_blue" data-percent="90">
							<i class="fa fa-plane"></i>
						  </div>
						  <div class="counter_title">airlines to travel</div>
					  </div>
				  </div>
				  
				  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				  <div class="timer" id="counter3" data-to="3850" data-delay="100" data-speed="3000">3850</div>
					  <div class="chart" data-animated="bounceIn" data-delay="100">
						  <div class="percentage_red" data-percent="85">
							<i class="fa fa-car"></i>
						  </div>
						  <div class="counter_title">vip transport options</div>
					  </div>
				  </div>
				  
				  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				  <div class="timer" id="counter4" data-to="3850" data-delay="100" data-speed="3000">3850</div>
					  <div class="chart" data-animated="bounceIn" data-delay="100">
						  <div class="percentage_skyblue" data-percent="70">
							<i class="fa fa-building-o"></i>
						  </div>
						  <div class="counter_title">amazing hotels to stay</div>
					  </div>
				  </div>
			  </div>
		 </div>
	  </div>
	  
	  	  <!-- counter section End -->
	  
  </div>
  <!--content body end--> 