<aside class="widget hotel_widgets">
<h4 class="widget-title">Latest Blog/News</h4>
<ul>
<?php foreach(Blog::model()->findAllByAttributes(array('publish'=>1), array('limit'=>5, 'order'=>'id DESC')) as $blog){;?>  
<li>
   <div>
      <h4 class="hover-link"><?php echo CHtml::link($blog->judul, array('site/blogDetail', 'id'=>$blog->id, 'title'=>str_replace(' ', '-', $blog->judul)));?></h4>
      <p><i class="fa fa-clock-o"></i> <?php echo Tools::getDateTime($blog->create_time);?></p>
      <?php if(!empty($blog->tags)){?><p><i class="fa fa-tags"></i> <?php echo $blog->tags;?></p><?php }?>
  
  </div>
</li>
<?php }?>
			 	     
</ul>			 
</aside>