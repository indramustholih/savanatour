<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login Form',
);

?>
<div class="login-box">
      <div class="login-logo">
        <a href="#"><b>Backend </b>SavanaTourJogja</a>
	</div><!-- /.login-logo -->
	<div class="login-box-body">
	<p class="login-box-msg">Silahkan login terlebih dahulu</p>
	<?php $form=$this->beginWidget('booster.widgets.TbActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
		)); ?>
	
		<?php echo $form->textFieldGroup($model,'username', array('class'=>'col-sm-12',
									  'prepend'=>'<i class="fa fa-user"></i>',
									  'placeholder'=>'username...',
									  'label'=>false)); ?>
		
		<?php echo $form->passwordFieldGroup($model,'password', array(
								'class'=>'col-sm-12',
								//'id'=>'password',
								'prepend'=>'<i class="fa fa-lock"></i>',
								'placeholder'=>'password...',
								'label'=>false));
		?>

		<hr>
		<div class="row">
			<div class="col-xs-8">
				<?php echo $form->checkboxGroup($model,'rememberMe', array(
									 'class'=>'col-sm-12',
									 'style'=>'text-align:left',
									 )); ?>
			</div>
			<div class="col-xs-4">
				<?php
				$this->widget('booster.widgets.TbButton', array(
					'buttonType'=>'submit',
					'htmlOptions'=>array('class'=>'btn-flat btn-block'),
					'label'=>'LOGIN',
					'context'=>'primary',
					'htmlOptions'=>array(
							     'id'=>'btnLogin',
							     ),
				    ));
				?>
			</div>
		</div>
	
			
		
		
	 
	<?php $this->endWidget(); ?>
	</div><!-- /.login-box-body -->
</div><!-- /.login-box -->
