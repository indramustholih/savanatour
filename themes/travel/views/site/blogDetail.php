<?php
  $this->pageTitle ="Savana Tour Jogja - Blog : ". $model->judul;
  $this->pageDescription = $model->judul;
  $this->pageKeyword = $model->judul.', '.$this->pageKeyword;
?>

<!--content body start-->
  <div id="content_wrapper"> 
    <!--page title start-->
    <div class="page_title" data-stellar-background-ratio="0" data-stellar-vertical-offset="0" style="background-image:url(<?php echo Yii::app()->theme->baseUrl.'/images/header-top-2.jpg';?>);">
      <ul>
        <li>Blog</li>
        <li>News</li>
      </ul>
    </div>
    <!--page title end-->
    <div class="clearfix"></div>
    <div class="container">
      <div class="row push-down-100">
        <div class="col-md-3">
          <div class="travelite_left_sidebar">
              <?php $this->renderPartial('_sideTourList'); ?>
          </div>
        </div>
        <div class="col-md-9">
          <div class="blog_single_page_wrapper">
          	<div class="travel_post_switcher">
            	 <?php echo CHtml::link('<i class="fa fa-caret-left"></i> Daftar Blog/News', array('/site/blog'), array('class'=>'previous_post'));?>
            </div>
            <div class="travel_post">
              <img src="<?php echo Yii::app()->request->baseUrl.'/images/blog/'.$model->cover_img;?>" alt="<?php echo $model->judul;?>" />
              <h3><?php echo CHtml::link($model->judul, array('site/blogDetail', 'id'=>$model->id, 'title'=>$model->judul));?></h3>
              <div class="travel_meta">
                <ul>
                  <li><?php echo Tools::getDateTime($model->create_time);?></li>
                  <li><a href=""><i class="fa fa-heart"></i> Likes</a></li>
                  <li><a href=""><i class="fa fa-eye"></i> Viewer</a></li>
                  <li><i class="fa fa-tags"></i> <?php echo $model->tags;?></li>
                </ul>
              </div>
              <div class="post_detail">
                <?php echo $model->konten;?>
                <br>
                <?php echo SystemConfig::model()->findByAttributes(array('key'=>'h_banner_wego_1'))->value;?>
              </div> 
            
              <div class="row">
              	<div class="col-md-8">
                	<div class="tagcloud">
                    	<i class="fa fa-tags"></i>
                        <span class="tag_heading">Tags :</span>
                        <?php foreach($tags as $tag){ ?>
                            <?php echo CHtml::link(str_replace(' ', '', $tag).', ', array('site/blogTag', 'key'=>$tag), array('class'=>'tag-link'));?>
                        <?php }?>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                	<div class="post_share">
                            <i class="fa fa-share-alt"></i>
                            <span class="post_share_heading">Share :</span>
                            <?php $this->widget('application.extensions.SocialShareButton.SocialShareButton', array(
                                    'style'=>'horizontal',
                                    'networks' => array('facebook','googleplus','twitter'),
                                    'data_via'=>'', //twitter username (for twitter only, if exists else leave empty)
                            ));?>
                        </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--content body end--> 