<?php $this->pageTitle ="Gallery - Savana Tour Jogja : Gallery Photo Objek Wisata Indonesia";?>
  <!--content body start-->
  <div id="content_wrapper"> 
    <!--page title start-->
    <div class="page_title" data-stellar-background-ratio="0" data-stellar-vertical-offset="0" style="background-image:url(<?php echo Yii::app()->theme->baseUrl.'/images/header-top-6.jpg';?>);">
      <ul>
        <li><a href="javascript:;">Tour Gallery</a></li>
      </ul>
    </div>
    <!--page title end-->
    <div class="clearfix"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul id="portfolio-filter" class="text-center">
            <li class="active"><a href="#" data-filter="*"><span class="dot"></span> All</a></li>
            <?php foreach(TourKategori::model()->findAll() as $kategori){;?>
	      <li><a href="#" data-filter=".<?php echo $kategori->kategori;?>"><span class="dot"></span> <?php echo $kategori->kategori;?></a></li>
	    <?php }?>
	  </ul>
          <div class="portfolio-row">
	    <div class="portfolio_column_3_popup">
            <div id="portfolio-item-container" class="max-col-3 popup-gallery" data-layoutmode="fitRows">
              <?php $dataProvider=new CActiveDataProvider('TourPhoto', array(
							  'criteria'=>array('order'=>'id DESC'),
							  'pagination'=>array('pageSize'=>9,), 
						  ));?>
						  
	      <?php $this->widget('booster.widgets.TbListView',array(
				  'dataProvider'=>$dataProvider,
				  'itemView'=>'_galleryList',
			      )); ?>	
	    
            </div><!-- End #portfolio-item-container --> 
	    </div><!--  portfolio_column_3_popup end -->
          </div> <!-- end .row --> 
          
        </div>
        <!-- End .col-md-12 --> 
      </div>
      <!-- End .row --> 
    </div>
    <!-- End .container --> 
    
  </div>
  <!--content body end--> 