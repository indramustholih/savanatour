 <?php $this->pageTitle ='Tours - Savana Tour Jogja : Info Paket Wisata';?>
 <!--content body start-->
  <div id="content_wrapper">
  <!--page title Start-->
  <div class="page_title" data-stellar-background-ratio="0" data-stellar-vertical-offset="0" style="background-image:url(<?php echo Yii::app()->theme->baseUrl.'/images/header-top-5.jpg';?>);">
      <ul>
        <li><a href="javascript:;">Tour Destination</a></li>
      </ul>
    </div>
    <!--page title end-->
    <div class="clearfix"></div>
	
	<div class="full_width destinaion_sorting_section">
	  <div class="container">
	   <div class="row space_100">
	   
	     <!-- left sidebar start -->
	     <div class="col-lg-3 col-md-3 col-sm-12">
	       <div class="travelite_left_sidebar">
		    <?php $this->renderPartial('_sideBlogList'); ?>
		</div>
	       <div class="travelite_left_sidebar_second" style="margin-bottom: 20px;">
		    <aside class="widget hotel_booking_widget" style="padding-bottom: 0; padding-top: 0; border-bottom: 1px solid #eeeeee;">
		    <h4 class="widget-title">Informasi Lebih Lanjut</h4>
		    <div class="widgett text_widget">
			<h3><i class="fa fa-phone"></i><?php echo SystemConfig::model()->findByAttributes(array('key'=>'phone'))->value;?></h3>			
		    </div>			 
		    </aside>
		</div>
	       <div class="travelite_left_sidebar">
		     <aside class="widget hotel_widgets" style="text-align: center">
			<?php echo SystemConfig::model()->findByAttributes(array('key'=>'v_banner_wego_1'))->value;?>
		      </aside>
		</div>
	     </div>
	      <!-- left sidebar end -->
	      
	     <!-- right main start -->
		<div class="col-lg-9 col-md-9 col-sm-12">
		<div class="tour_packages_right_section left_space_40">
		<?php /*
                <div class="tour_packages_description">
		<div class="tour_heading">
		<h4>Australia Tour Packages</h4>
		<span class="packs">(178 Packs Found)</span>
		</div>
		<p class="more_text">There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in 
		some format, by injected humour.  There are many variations of passages of Lorem Ipsu available, 
		but the joy have suffered alteration in some format,by injected humour users 
		Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
		</div>
		*/?>
		<?php /*
		    <div class="full_width sorting_panel">
			<div class="sorting_destination">
					       <select class="form-control selectpicker" id="search_rooms">
						       <option value="1">Sort by : Popularity</option>
						       <option value="2">02</option>
						       <option value="3">03</option>
						       <option value="4">04</option>
					       </select>
					       <i class="fa fa-chevron-down"></i>
				       </div>
				       <div class="sorting_destination">
					       <select class="form-control selectpicker" id="search_places">
						       <option value="1">Show: 9 places/page</option>
						       <option value="2">02</option>
						       <option value="3">03</option>
						       <option value="4">04</option>
					       </select>
					       <i class="fa fa-chevron-down"></i>
				       </div>
				       <div class="sorting_destination">
					       <select class="form-control selectpicker" id="search_prices">
						       <option value="1">Sort by : Price</option>
						       <option value="2">50</option>
						       <option value="3">75</option>
						       <option value="4">100</option>
					       </select>
					       <i class="fa fa-chevron-down"></i>
				       </div>
				       <!-- sorting list -->
				       <div class="pull-right sort_list_grid">
				       <a href="Tour-Packages-Grid-View.html"><i class="fa fa-th-large"></i></a>
				       <a href="Tour-Packages-List-View.html"><i class="fa fa-th-list active_sort"></i></a>
				       </div> 
				       <!-- sorting list end-->
			
			</div><!--  sorting panel End -->
		*/?> 
		 
		 
		<!-- sorting places section -->
		<div class="full_width sorting_places_section" style="padding-top: 0px; margin-top: -15px">
                    <?php $dataProvider=new CActiveDataProvider('Tour', array(
								'criteria'=>array('order'=>'id DESC', 'condition'=>'publish=1'),
								'pagination'=>array('pageSize'=>5,), 
							));?>
							
                    <?php $this->widget('booster.widgets.TbListView',array(
                                        'dataProvider'=>$dataProvider,
                                        'itemView'=>'_toursList',
                                    )); ?>	 
                </div>
                <!-- sorting places section -->
	
	   </div>
	   </div><!-- right main start -->
	  </div>
	</div>

  </div>
  <!--content body end--> 