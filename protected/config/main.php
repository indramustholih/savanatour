<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
        'theme'=>'travel',
		'name'=>'SavanaTour',
        'timeZone' => 'Asia/Jakarta',
	
        // preloading 'log' component
	'preload'=>array('log', 'booster'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
                'application.extensions.EGMap.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1234',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
                                'generatorPaths'=>array(
                                                'booster.gii',
                                                 ),
		),   
	),
        
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
                        'urlFormat'=>'path',
                        'showScriptName'=>false,                        
                        'rules'=>array(
                            'tours/<id:\d+>/<title>' => 'site/tourDetail',
                            'blog/<id:\d+>/<title>' => 'site/blogDetail',
                            'admin/sign-in'=>'site/login',
                            'admin/dashboard'=>'site/administrator',
                            '<name>'=>'site/<name>',
                            
			),
		),
		
                'booster' => array(
                    'class' => 'ext.booster.components.Booster',
                ),
                /*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=savana',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
        /*
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=admin_bapontar',
			'emulatePrepare' => true,
			'username' => 'admin_bapontar',
			'password' => 'digitak1234*',
			'charset' => 'utf8',
		),
		*/
		/*
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		*/
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		//'adminEmail'=>'aeng.anwar@gmail.com',
                'adminEmail'=>'info@savanatourjogja.com',
	),
);