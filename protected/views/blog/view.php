<?php
$this->breadcrumbs=array(
	'Blogs'=>array('index'),
	$model->id,
);

$this->pageTitle='Detail Blog/News';
?>


<div class="box box-primary">
    <div class="box-body">
    <?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
                    'id',
                    'cover_img',
                    'judul',
                    array(
                          'name'=>'blog_kategori_id',
                          'value'=>$model->blogKategori->kategori,
                    ),
                    'publish',
                    array(
                          'name'=>'konten',
                          'type'=>'raw',
                    ),
                    'tags',
                    'create_time',
                    'update_time',
                    'create_user_id',
                    'update_user_id',
    ),
    )); ?>
    </div>
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Blog/News',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Create Blog/News',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('update', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'pencil white',
                    'label'=>'Update Blog/News',
            )); ?>
    </div>
</div>