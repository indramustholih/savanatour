<?php
$this->breadcrumbs=array(
	'Blogs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
$this->pageTitle = 'Update Blog/News';
?>
<div class="box box-primary">
    <div class="box-body">
    <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
    </div>
    <div class="box-footer">
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Blog/News',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Create Blog/News',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('view', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'eye-open white',
                    'label'=>'View Blog/News',
            )); ?>
    </div>
</div>