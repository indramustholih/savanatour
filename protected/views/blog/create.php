<?php
$this->breadcrumbs=array(
	'Blogs'=>array('index'),
	'Create',
);

$this->pageTitle = 'Create Blog/News';
?>

<div class="box box-primary">
    <div class="box-body">
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
    <div class="box-footer">
            <?php $this->widget('booster.widgets.TbButton', array(
                            'buttonType'=>'link',
                            'htmlOptions'=>array('class'=>'btn-flat'),
                            'url'=>array('admin'),
                            'context'=>'primary',
                            'icon'=>'list white',
                            'label'=>'Daftar Blog/News',
                    )); ?>
    </div>
</div>