<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'blog-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' =>array('enctype'=>"multipart/form-data"),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'judul',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->checkboxGroup($model,'publish'); ?>
	
	<?php echo $form->dropDownListGroup($model,'blog_kategori_id', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array('prompt'=>'-Pilih Kategori-', 'Kategori'=>CHtml::listData(BlogKategori::model()->findAll(), 'id', 'kategori')),
						),
						)
						
					); ?>
						
	<?php if(!$model->isNewRecord){?>
		<img width="300px" src="<?php echo Yii::app()->request->baseUrl.'/images/blog/'.$model->cover_img?>"><br><br>
	<?php }?>
	
	<?php echo $form->filefieldGroup($model,'cover_img'); ?>
	
	<?php echo $form->ckEditorGroup($model,'konten',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>
										  
	<?php echo $form->textAreaGroup($model,'tags', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>

	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'primary',
			'icon' => 'fa fa-save',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
