<?php
$this->breadcrumbs=array(
	'Tour Kategoris'=>array('index'),
	$model->id,
);

$this->pageTitle='Detail Tour Kategori';
?>

<div class="box box-primary">
    <div class="box-body">
    <?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
                    'id',
                    'kategori',
                    'keterangan',
                    'create_time',
                    'update_time',
                    'create_user_id',
                    'update_user_id',
    ),
    )); ?>
</div>
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Tour Kategori',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Create Tour Kategori',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('update', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'pencil white',
                    'label'=>'Update Tour Kategori',
            )); ?>
    </div>
</div>
