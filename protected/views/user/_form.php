<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
	
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	
	<?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->passwordFieldGroup($model,'password',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->dropDownListGroup($model,'user_role_id', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array('Role'=>CHtml::listData(UserRole::model()->findAll(), 'id', 'role')),
						),
						)
						
					); ?>
	<?php echo $form->dropDownListGroup($model,'status', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array(0=>'Disable', 1=>'Active'),
						),
						)
						
					); ?>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'context'=>'danger',
			'icon' => 'ok',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
