<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

		<?php echo $form->textFieldGroup($model,'username',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

		<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

		<?php echo $form->textFieldGroup($model,'email',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
		
		<?php echo $form->dropDownListGroup($model,'user_role_id', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array('Role'=>CHtml::listData(UserRole::model()->findAll(), 'id', 'role')),
						),
						)
						
					); ?>
		<?php echo $form->dropDownListGroup($model,'status', array(
							'wrapperHtmlOptions' => array(
								'class' => 'col-sm-5',
								),
							'widgetOptions' => array(
								'data' => array(0=>'Disable', 1=>'Active'),
							),
							)
							
						); ?>

	<div class="form-actions">
		<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType' => 'submit',
			'context'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
