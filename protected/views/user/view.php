<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->pageTitle='Detail User';
?>
<div class="box box-primary">
    <div class="box-body">
        <?php $this->widget('booster.widgets.TbDetailView',array(
        'data'=>$model,
        'attributes'=>array(
                        'id',
                        'username',
                        'password',
                        'nama',
                        'email',
                        array(
                            'name'=>'user_role_id',
                            'value'=>$model->userRole->role,
                        ),
                        array(
                              'name'=>'status',
                              'type'=>'raw',
                              'value'=>$model->status ==0 ? CHtml::tag("span",array("class"=>"label label-danger"),"Disable") :
					 CHtml::tag("span",array("class"=>"label label-success"),"Active"),
                        ),
                        'last_login',
                        //'is_delete',
                        'create_time',
                        'update_time',
                        'create_user_id',
                        'update_user_id',
        ),
        )); ?>
    </div>
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar User',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Create User',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('update', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'pencil white',
                    'label'=>'Update User',
            )); ?>
    </div>
</div>

