<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->pageTitle = 'Daftar User';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('user-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="box box-parimary">
	<div class="box-header with-border">
		<?php echo CHtml::link('Filter Pencarian','#',array('class'=>'box-title search-button btn')); ?>
		<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
	</div>
	<div class="box-body">
		<?php $this->widget('booster.widgets.TbGridView',array(
		'id'=>'user-grid',
		'type'=>'bordered striped hover',
		'dataProvider'=>$model->search(),
		'filter'=>$model,
		'columns'=>array(
				array(
					'header'=>'No',
					'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
					'htmlOptions'=>array(
						'style'=>'text-align:center',
						'width'=>'30px',
				      ),
				),
				
				'username',
				//'password',
				'nama',
				'email',
				array(
				      'name'=>'user_role_id',
				      'filter'=>CHtml::listData(UserRole::model()->findAll(), 'id', 'role'),
				      'value'=>'$data->userRole->role',
				),
				array(
					'name'=>'status',
					'filter'=>array(0=>'Disable', 1=>'Active'),
					'type'=>'raw',
					'value'=>'$data->status ==0 ? CHtml::tag("span",array("class"=>"label label-danger"),"Disable") :
					 CHtml::tag("span",array("class"=>"label label-success"),"Active") ',
				),
				array(
				      'name'=>'last_login',
				      'filter'=>'',
				      'value'=>'Tools::getDateTime($data->last_login)',
				),
				/*
				'is_delete',
				'create_time',
				'update_time',
				'create_user_id',
				'update_user_id',
				*/
		array(
			'header'=>'Pilihan',
			'class'=>'booster.widgets.TbButtonColumn',
			'htmlOptions'=>array(
						'style'=>'text-align:center',
						'width'=>'80px',
				      ),
		),
		),
		)); ?>
	</div>
	<div class="box-footer">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array('class'=>'btn-flat'),
			'url'=>array('create'),
			'context'=>'primary',
			'icon'=>'plus white',
			'label'=>'Create User',
		)); ?>
	</div>
</div>



