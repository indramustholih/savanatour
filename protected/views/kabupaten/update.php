<?php
$this->breadcrumbs=array(
	'Kabupatens'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->pageTitle = 'Edit Kota/Kabupaten';
?>
<div class="box box-primary">
    <div class="box-body">
    <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
    </div>
    <div class="box-footer">
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Kota/Kabupaten',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Tambah Kota/Kabupaten',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('view', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'eye-open white',
                    'label'=>'Detail Kota/Kabupaten',
            )); ?>
    </div>
</div>