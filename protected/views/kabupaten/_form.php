<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'kabupaten-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'nama',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php //echo $form->textFieldGroup($model,'provinsi_id',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	<?php echo $form->dropDownListGroup($model,'provinsi_id', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array('- Pilih Provinsi -', 'Provinsi'=>CHtml::listData(Provinsi::model()->findAllByAttributes(array('is_active'=>1)), 'id', 'nama')),
							
							)
						
						)
						
					); ?>
						
	<?php //echo $form->textFieldGroup($model,'jenis',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>
	<?php echo $form->dropDownListGroup($model,'jenis', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array("0"=>"KABUPATEN", "1"=>"KOTA"),
							
							)
						
						)
						
					); ?>
					
	<?php echo $form->textAreaGroup($model,'keterangan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>6, 'cols'=>50, 'class'=>'span8')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'icon' => 'fa fa-save',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
