<?php
$this->breadcrumbs=array(
	'Kabupatens'=>array('index'),
	$model->id,
);

$this->pageTitle='Detail Kota/Kabupaten';
?>

<div class="box box-primary">
    <div class="box-body">
    <?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
                    'id',
                    array('name'=>'provinsi_id', 'value'=>$model->provinsi->nama),
                    'nama',
                    //'jenis',
                    array('name'=>'jenis', 'value'=>$model->jenis = 0 ? "KABUPATEN" : "KOTA"),
                    'keterangan',
                    'create_time',
                    'update_time',
                    'create_user_id',
                    'update_user_id',
    ),
    )); ?>
    </div>
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Kota/Kabupaten',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Tambah Kota/Kabupaten',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('update', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'pencil white',
                    'label'=>'Edit Kota/Kabupaten',
            )); ?>
    </div>
</div>
