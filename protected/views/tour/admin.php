<?php
$this->breadcrumbs=array(
	'Tours'=>array('index'),
	'Manage',
);

$this->pageTitle ='Daftar Paket Tour';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('tour-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="box box-parimary">
	<?php /*
	<div class="box-header with-border">
		<?php echo CHtml::link('Filter Pencarian','#',array('class'=>'box-title search-button btn')); ?>
		<div class="search-form" style="display:none">
			<?php $this->renderPartial('_search',array(
			'model'=>$model,
		)); ?>
		</div><!-- search-form -->
	</div>
	*/?>
	<div class="box-body">
	<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'tour-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
			      ),
			),
			'judul',
			array(
			      'name'=>'tour_kategori_id',
			      'filter' =>CHtml::listData(TourKategori::model()->findAll(), 'id', 'kategori'),
			      'value'=>'$data->tourKategori->kategori',
			),
			'harga_paket',
			'durasi',
			'minimal',
			/*
			'deskripsi',
			'itenary',
			'destinasi',
			'include',
			'exclude',
			'kabupaten_id',
			'publish',
			'create_time',
			'update_time',
			'create_user_id',
			'update_user_id',
			*/
			array(
				'name'=>'top',
				'filter'=>array('0'=>'-', '1'=>'Top'),
				'value'=>'$data->top == 1 ? "Top" : "-"',
			),
			array(
				'name' => 'publish',
				'filter'=>array(0=>"Unpublish", 1=>"Publish"),
				'type'=>'raw',
				'value'=>'$data->publish ==0 ? CHtml::tag("span",array("class"=>"label label-danger"),"Unpublish") :
				 CHtml::tag("span",array("class"=>"label label-success"),"Publish") ',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'80px',
			      ),
			),
			array(
			'header'=>'Pilihan',
			'class'=>'booster.widgets.TbButtonColumn',
			'htmlOptions'=>array(
						'style'=>'text-align:center',
						'width'=>'80px',
				      ),
			),
			),
	)); ?>
	</div>
	<div class="box-footer">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array('class'=>'btn-flat'),
			'url'=>array('create'),
			'context'=>'primary',
			'icon'=>'plus white',
			'label'=>'Create Paket Tour',
		)); ?>
	</div>
</div>