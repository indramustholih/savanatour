<div class="view">

		<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('judul')); ?>:</b>
	<?php echo CHtml::encode($data->judul); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tour_kategori_id')); ?>:</b>
	<?php echo CHtml::encode($data->tour_kategori_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harga_paket')); ?>:</b>
	<?php echo CHtml::encode($data->harga_paket); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('durasi')); ?>:</b>
	<?php echo CHtml::encode($data->durasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('minimal')); ?>:</b>
	<?php echo CHtml::encode($data->minimal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deskripsi')); ?>:</b>
	<?php echo CHtml::encode($data->deskripsi); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('itenary')); ?>:</b>
	<?php echo CHtml::encode($data->itenary); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('destinasi')); ?>:</b>
	<?php echo CHtml::encode($data->destinasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('include')); ?>:</b>
	<?php echo CHtml::encode($data->include); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exclude')); ?>:</b>
	<?php echo CHtml::encode($data->exclude); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kabupaten_id')); ?>:</b>
	<?php echo CHtml::encode($data->kabupaten_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publish')); ?>:</b>
	<?php echo CHtml::encode($data->publish); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_user_id')); ?>:</b>
	<?php echo CHtml::encode($data->update_user_id); ?>
	<br />

	*/ ?>

</div>