<?php
$this->breadcrumbs=array(
	'Tours'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
$this->pageTitle = 'Update Paket Tour';
?>
<div class="box box-primary">
    <div class="box-body">
    <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
    </div>
    <div class="box-footer">
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Paket Tour',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Create Paket Tour',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('view', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'eye-open white',
                    'label'=>'View Paket Tour',
            )); ?>
    </div>
</div>