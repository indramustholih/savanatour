<?php
$this->breadcrumbs=array(
	'Tours'=>array('index'),
	$model->id,
);

$this->pageTitle='Detail Tour Kategori';
?>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $model->judul;?></h3>
        <div class="box-tools pull-right">
          <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
    </div>
    <div class="box-body">
    <?php $this->widget('booster.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
                    'id',
                    'judul',
                    array(
                          'name'=>'top',
                          'value'=>$model->top == 1 ? "Top" : "-",
                    ),
                    array(
                          'name'=>'publish',
                          'value'=>$model->publish == 1 ? "Publish" : "Unpublish",
                    ),
                    array(
                          'name'=>'tour_kategori_id',
                          'value'=>$model->tourKategori->kategori,
                    ),
                    'harga_paket',
                    'durasi',
                    'minimal',
                    array(
                          'name'=>'deskripsi',
                          'type'=>'raw',
                          'value'=>$model->deskripsi,
                    ),
                    array(
                          'name'=>'itenary',
                          'type'=>'raw',
                          'value'=>$model->itenary,
                    ),
                    array(
                          'name'=>'destinasi',
                          'type'=>'raw',
                          'value'=>$model->destinasi,
                    ),
                    array(
                          'name'=>'include',
                          'type'=>'raw',
                          'value'=>$model->include,
                    ),
                    array(
                          'name'=>'exclude',
                          'type'=>'raw',
                          'value'=>$model->exclude,
                    ),
                    array(
                          'name'=>'kabupaten_id',
                          'value'=>$model->kabupaten->nama,
                    ),
                    'create_time',
                    'update_time',
                    'create_user_id',
                    'update_user_id',
    ),
    )); ?>
      </div>
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Paket Tour',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Create Paket Tour',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('update', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'pencil white',
                    'label'=>'Update Paket Tour',
            )); ?>
    </div>
</div>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Photo Paket Tour <small>Silahkan Tambahkan Photo Menu.</small></h3>
        <div class="box-tools pull-right">
          <button data-widget="collapse" class="btn btn-box-tool"><i class="fa fa-minus"></i></button>
        </div><!-- /.box-tools -->
    </div>
    <div class="box-body">
        <!-- Thumbnail Foto -->
        <?php foreach(TourPhoto::model()->findAllByAttributes(array('tour_id'=>$model->id)) as $photo){?>
            <div class="col-sm-6 col-md-3 gallery clearfix">
                <div class="thumbnail" style="height: auto">
                    <a title="<?php echo $model->judul;?>" rel="prettyPhoto[gallery1]" href="<?php echo Yii::app()->baseUrl.'/images/tours/'.$photo->photo;?>">
                        <img id="thumb-photo" src="<?php echo Yii::app()->baseUrl.'/images/tours/'.$photo->photo;?>" alt="<?php echo $model->judul;?>">
                    </a>
                    <div class="caption">
                       <p><span class="label label-warning">Cover : <?php echo $photo->cover == 1 ? '<i class="fa fa-check"></i>' : '<i class="fa fa-times"></i>';?></span></p>
                        <p><?php echo $photo->keterangan;?></p>
                        <p>
                            <?php $this->widget('booster.widgets.TbButton', array(
                                            'buttonType'=>'link',
                                            'icon'=>'trash white',
                                            'label'=>'',
                                            'context'=>'danger',
                                            'url'=>array('tour/hapusPhoto','id'=>$photo->id),
                                            'htmlOptions'=>array('class'=>'btn-sm', 'confirm' => 'Anda yakin akan menghapus photo ini?'),
                            )); ?>
                        </p>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                                        'buttonType'=>'link',
                                        'icon'=>'upload white',
                                        'label'=>'Tambah Photo',
                                        'context'=>'warning',
                                        'htmlOptions'=>array('class'=>'pull-left btn-flat',
                                                            'data-toggle'=>'modal',
                                                            'style'=>'margin-right:10px',
                                                            'data-target'=>'#addImage',
                                                            ),
        )); ?>
    </div>
</div>


<!-- Show Modal Add Photo-->
<?php $this->beginWidget('booster.widgets.TbModal', array('id'=>'addImage')); ?>
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4 class="modal-title">Tambahkan Photo</h4>
</div>

<div class="modal-body">
    <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	    'id'=>'tour-photo-form',
	    'enableAjaxValidation'=>false,
            'htmlOptions' =>array('enctype'=>"multipart/form-data"),
            'action'=>array('/tour/uploadPhoto'),
    )); ?>
    
    <?php echo $form->hiddenField($TourPhoto,'tour_id', array('value'=>$model->id)); ?>
    
    <?php echo $form->filefieldGroup($TourPhoto,'photo'); ?>
    
    <?php echo $form->checkboxGroup($TourPhoto,'cover'); ?>
    
    <?php echo $form->textAreaGroup($TourPhoto,'keterangan',array('class'=>'col-sm-12','maxlength'=>255)); ?>
</div>

<div class="modal-footer">
    
    <?php $this->widget('booster.widgets.TbButton', array(
							'label'=>'Cancel',
							'icon'=>'repeat',
							'url'=>'#',
							'htmlOptions'=>array('data-dismiss'=>'modal',
                                                                             'class'=>'pull-left',
                                                                             ),
						    )); ?>
    
    <?php $this->widget('booster.widgets.TbButton',array('buttonType'=>'submit',
							  'label'=>'Tambahkan',
                                                          'context'=>'danger',
							  'icon'=>'ok white',
							  )); ?>
</div>
<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
