<?php
$this->breadcrumbs=array(
	'Tours',
);

$this->menu=array(
array('label'=>'Create Tour','url'=>array('create')),
array('label'=>'Manage Tour','url'=>array('admin')),
);
?>

<h1>Tours</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
