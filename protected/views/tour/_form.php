<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'tour-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'judul',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	
	<?php echo $form->checkboxGroup($model,'top'); ?>
		
	<?php echo $form->checkboxGroup($model,'publish'); ?>
	
	<?php echo $form->dropDownListGroup($model,'tour_kategori_id', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array('prompt'=>'-Pilih Kategori-', 'Kategori'=>CHtml::listData(TourKategori::model()->findAll(), 'id', 'kategori')),
						),
						)
						
					); ?>
					
	<?php echo $form->dropDownListGroup($model,'provinsi_id', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array('- Pilih Provinsi -', 'Provinsi'=>CHtml::listData(Provinsi::model()->findAllByAttributes(array('is_active'=>1)), 'id', 'nama')),
							'htmlOptions' => array(
								'ajax' => array(
								  'type' => 'POST',
								  'url' => Yii::app()->createUrl('/Kabupaten/getKabupaten'),
								  'update'=>'#'.CHtml::activeId($model,'kabupaten_id'),
								),
							),
						),
						)
						
					); ?>
					
	<?php echo $form->dropDownListGroup($model,'kabupaten_id', array(
						'wrapperHtmlOptions' => array(
							'class' => 'col-sm-5',
							),
						'widgetOptions' => array(
							'data' => array('prompt'=>'-Pilih Kota/Kabupaten-', 'Kategori'=>CHtml::listData(Kabupaten::model()->findAll(), 'id', 'nama')),
						),
						)
						
					); ?>
	<?php echo $form->textFieldGroup($model,'harga_paket',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'durasi',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textFieldGroup($model,'minimal',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>
	<div class="fasilitas" style="margin: 5px; padding: 5px 10px; border: 1px solid #DDD;">
		<h4>Fasilitas :</h4>
		<?php echo $form->checkboxGroup($model,'local_transport'); ?>
		<?php echo $form->checkboxGroup($model,'pesawat'); ?>
		<?php echo $form->checkboxGroup($model,'tiketing'); ?>
		<?php echo $form->checkboxGroup($model,'makan'); ?>
		<?php echo $form->checkboxGroup($model,'akomodasi'); ?>
	</div>
	<?php echo $form->ckEditorGroup($model,'deskripsi',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>
										  
	<?php echo $form->ckEditorGroup($model,'itenary',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>
										  
	<?php echo $form->ckEditorGroup($model,'destinasi',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>
										  
	<?php echo $form->ckEditorGroup($model,'include',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>
										  
	<?php echo $form->ckEditorGroup($model,'exclude',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>									  
	
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'icon' =>'fa fa-save',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
