<?php
$this->breadcrumbs=array(
	'Eksternal Links'=>array('index'),
	'Create',
);
$this->pageTitle = 'Buat Eksternal Link';
?>
<div class="box box-primary">
    <div class="box-body">
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
    <div class="box-footer">
            <?php $this->widget('booster.widgets.TbButton', array(
                            'buttonType'=>'link',
                            'htmlOptions'=>array('class'=>'btn-flat'),
                            'url'=>array('admin'),
                            'context'=>'primary',
                            'icon'=>'list white',
                            'label'=>'Daftar Eksternal Link',
                    )); ?>
    </div>
</div>