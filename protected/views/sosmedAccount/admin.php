<?php
$this->breadcrumbs=array(
	'Sosmed Accounts'=>array('index'),
	'Manage',
);

$this->pageTitle = 'Account Sosial Media';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('sosmed-account-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="box box-primary">
	<div class="box-body">
	<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'sosmed-account-grid',
	'type' => 'striped hover bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
			      ),
			),
			'sosial_media',
			'account',
			'link',
			'keterangan',
			array(
				'name' => 'publish',
				'filter'=>array(0=>"Unpublish", 1=>"Publish"),
				'type'=>'raw',
				'value'=>'$data->publish ==0 ? CHtml::tag("span",array("class"=>"label label-danger"),"Unpublish") :
				 CHtml::tag("span",array("class"=>"label label-success"),"Publish") ',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'80px',
			      ),
			),
			array(
				'header'=>'Pilihan',
				'class'=>'booster.widgets.TbButtonColumn',
				'template'=>'{update}',
			),
			),
	)); ?>
	</div>
</div>
