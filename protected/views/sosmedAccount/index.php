<?php
$this->breadcrumbs=array(
	'Sosmed Accounts',
);

$this->menu=array(
array('label'=>'Create SosmedAccount','url'=>array('create')),
array('label'=>'Manage SosmedAccount','url'=>array('admin')),
);
?>

<h1>Sosmed Accounts</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
