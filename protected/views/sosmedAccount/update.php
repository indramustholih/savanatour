<?php
$this->breadcrumbs=array(
	'Sosmed Accounts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);
$this->pageTitle = 'Edit Account Sosial Media';
?>

<div class="box box-primary">
    <div class="box-body">
    <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
    </div>
</div>