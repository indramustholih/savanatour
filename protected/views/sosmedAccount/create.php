<?php
$this->breadcrumbs=array(
	'Sosmed Accounts'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List SosmedAccount','url'=>array('index')),
array('label'=>'Manage SosmedAccount','url'=>array('admin')),
);
?>

<h1>Create SosmedAccount</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>