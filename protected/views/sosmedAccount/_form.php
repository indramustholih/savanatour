<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'sosmed-account-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldGroup($model,'sosial_media',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255, 'readOnly'=>'true')))); ?>

	<?php echo $form->checkboxGroup($model,'publish'); ?>
	
	<?php echo $form->textFieldGroup($model,'account',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

	<?php echo $form->textAreaGroup($model,'link', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>2, 'cols'=>50, 'class'=>'span8')))); ?>

	<?php echo $form->textAreaGroup($model,'keterangan', array('widgetOptions'=>array('htmlOptions'=>array('rows'=>3, 'cols'=>50, 'class'=>'span8')))); ?>

<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
		'buttonType'=>'submit',
		'icon' => 'fa fa-save',
		'context'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>
	
	<?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'fa fa-repeat',
                    'label'=>'Batal',
            )); ?>
</div>

<?php $this->endWidget(); ?>
