<?php
$this->breadcrumbs=array(
	'Sosmed Accounts'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List SosmedAccount','url'=>array('index')),
array('label'=>'Create SosmedAccount','url'=>array('create')),
array('label'=>'Update SosmedAccount','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SosmedAccount','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SosmedAccount','url'=>array('admin')),
);
?>

<h1>View SosmedAccount #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'sosial_media',
		'publish',
		'account',
		'link',
		'keterangan',
),
)); ?>
