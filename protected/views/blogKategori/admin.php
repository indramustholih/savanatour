<?php
$this->breadcrumbs=array(
	'Blog Kategoris'=>array('index'),
	'Manage',
);

$this->pageTitle ='Daftar Kategori Blog/News';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('blog-kategori-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="box box-parimary">
	<div class="box-body">
	<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'blog-kategori-grid',
	'type' => 'bordered striped hover',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			'kategori',
			//'create_user_id',
			//'update_user_id',
			//'create_time',
			//'update_time',
			array(
				'header'=>'Pilihan',
				'class'=>'booster.widgets.TbButtonColumn',
				'htmlOptions'=>array(
							'style'=>'text-align:center',
							'width'=>'80px',
						),
			),
			),
	)); ?>
	</div>
	<div class="box-footer">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array('class'=>'btn-flat'),
			'url'=>array('create'),
			'context'=>'primary',
			'icon'=>'plus white',
			'label'=>'Tambah Kategori Blog/News',
		)); ?>
	</div>
</div>
