<?php
$this->breadcrumbs=array(
	'Sliders'=>array('index'),
	$model->id,
);

$this->pageTitle='Detail Image Slider';
?>
<div class="box box-primary">
    <div class="box-body">
            <div class="row">
                <div class="col-lg-4 col-xs-4">
                    <br><br>
                    <img width="80%" src="<?php echo Yii::app()->request->baseUrl.'/images/slider/'.$model->image?>">
                </div>
                <div class="col-lg-8 col-xs-8">
                    <?php $this->widget('booster.widgets.TbDetailView',array(
                    'data'=>$model,
                    'attributes'=>array(
                                    'id',
                                    //'image',
                                    array(
                                        'name'=>'publish',
                                        'value'=>$model->publish == 0 ? "Unpublish" : "Publish",
                                    ),
                                    array(
                                          'name'=>'layer_1',
                                          'type'=>'raw',
                                    ),
                                    array(
                                          'name'=>'layer_2',
                                          'type'=>'raw',
                                    ),
                                    'create_time',
                                    'update_time',
                                    'create_user_id',
                                    'update_user_id',
                    ),
                    )); ?>
                </div>
            </div>
    </div>
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Daftar Slider',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('create'),
                    'context'=>'primary',
                    'icon'=>'plus white',
                    'label'=>'Buat Slider',
            )); ?>
            
            <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('update', 'id'=>$model->id),
                    'context'=>'primary',
                    'icon'=>'pencil white',
                    'label'=>'Edit Slider',
            )); ?>
    </div>
</div>
