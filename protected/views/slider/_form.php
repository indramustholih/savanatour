<?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
	'id'=>'slider-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' =>array('enctype'=>"multipart/form-data"),
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php if(!$model->isNewRecord){?>
		<img width="300px" src="<?php echo Yii::app()->request->baseUrl.'/images/slider/'.$model->image?>"><br><br>
	<?php }?>
	
	<?php echo $form->filefieldGroup($model,'image'); ?>
	
	<?php echo $form->checkboxGroup($model,'publish'); ?>
	
	<?php echo $form->ckEditorGroup($model,'layer_1',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>
	<?php echo $form->ckEditorGroup($model,'layer_2',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'500',
										  'resize_maxWidth' => '740',
										  'resize_minWidth' => '320'
										  ))); ?>
<div class="form-actions">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'submit',
			'icon' => 'fa fa-save',
			'context'=>'primary',
			'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
		)); ?>
</div>

<?php $this->endWidget(); ?>