<?php
$this->breadcrumbs=array(
	'System Config'=>array('admin'),
	'Data Website',
);

$this->pageTitle = 'Data Wesbite';

?>

<p class="help-block">Silahkan melakukan pembeharuan data website pada form dibawah.</p>

<div class="box box-primary">
	<div class="box-body">
            <?php $form=$this->beginWidget('booster.widgets.TbActiveForm',array(
                    'id'=>'slider-form',
                    'enableAjaxValidation'=>false,
            )); ?>
            
            <div class="form-group">
                <label for="SystemConfig_about_us" class="control-label">About Us</label>
                <textarea id="SystemConfig_about_us" name="SystemConfig[about_us]" rows="10" cols="80"><?php echo $aboutUs->value;?></textarea>
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_address" class="control-label">Address</label>
                <textarea id="SystemConfig_address" name="SystemConfig[address]" placeholder="Address" class="form-control"><?php echo $address->value;?></textarea>
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_city" class="control-label">City</label>
                <input type="text" value="<?php echo $city->value;?>" id="SystemConfig_city" name="SystemConfig[city]" placeholder="City" class="form-control">
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_provincies" class="control-label">Provincies</label>
                <input type="text" value="<?php echo $provincies->value;?>" id="SystemConfig_provincies" name="SystemConfig[provincies]" placeholder="Provincies" class="form-control">
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_contry" class="control-label">Contry</label>
                <input type="text" value="<?php echo $contry->value;?>" id="SystemConfig_contry" name="SystemConfig[contry]" placeholder="Contry" class="form-control">
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_zip_code" class="control-label">Zip Codde</label>
                <input type="text" value="<?php echo $zipCode->value;?>" id="SystemConfig_zip_code" name="SystemConfig[zip_code]" placeholder="Zip Code" class="form-control">
            </div>
          
            <div class="form-group">
                <label for="SystemConfig_lat" class="control-label">Latitude Coordinate</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                    <input type="text" value="<?php echo $lat->value;?>" id="SystemConfig_lat" name="SystemConfig[lat]" placeholder="Latitude Coordinate" class="form-control">
                </div>
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_long" class="control-label">Longitude Coordinate</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                    <input type="text" value="<?php echo $long->value;?>" id="SystemConfig_long" name="SystemConfig[long]" placeholder="Latitude Coordinate" class="form-control">
                </div>
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_email" class="control-label">Email</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                    <input type="text" value="<?php echo $email->value;?>" id="SystemConfig_email" name="SystemConfig[email]" placeholder="Value" class="form-control">
                </div>
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_phone" class="control-label">Phone</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-phone"></i></span>
                    <input type="text" value="<?php echo $phone->value;?>" id="SystemConfig_phone" name="SystemConfig[phone]" placeholder="Value" class="form-control">
                </div>
            </div>
            
            <div class="form-group">
                <label for="SystemConfig_whatsapp" class="control-label">Whatsapp</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
                    <input type="text" value="<?php echo $whatsapp->value;?>" id="SystemConfig_whatsapp" name="SystemConfig[whatsapp]" placeholder="Value" class="form-control">
                </div>
            </div>
	    
	    <div class="form-group">
                <label for="SystemConfig_whatsapp" class="control-label">Video Homepage</label>
                <div class="input-group"><span class="input-group-addon"><i class="fa fa-youtube"></i></span>
                    <input type="text" value="<?php echo $home_vid->value;?>" id="SystemConfig_home_vid" name="SystemConfig[home_vid]" placeholder="Value" class="form-control">
                </div>
            </div>
	    
	    <div class="form-group">
                <label for="SystemConfig_address" class="control-label">Facebook Panel</label>
                <textarea id="SystemConfig_facebook_panel" name="SystemConfig[facebook_panel]" placeholder="facebook_panel" class="form-control"><?php echo $facebook_panel->value;?></textarea>
            </div>
               
            <div class="form-actions">
                    <?php $this->widget('booster.widgets.TbButton', array(
                                    'buttonType'=>'submit',
                                    'icon' => 'fa fa-save',
                                    'context'=>'primary',
                                    'label'=>$model->isNewRecord ? 'Tambah' : 'Simpan',
                            )); ?>
            </div>
            
            <?php $this->endWidget(); ?>
        </div>
        <div class="box-footer">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array('class'=>'btn-flat'),
			'url'=>array('/site/administrator'),
			'context'=>'warning',
			'icon'=>'fa fa-hand-o-left',
			'label'=>'Kembali',
		)); ?>
	</div>
</div>

<script type="text/javascript">
      $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('SystemConfig_about_us');
      });
    </script>
