<?php
$this->breadcrumbs=array(
	'System Configs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List SystemConfig','url'=>array('index')),
	array('label'=>'Create SystemConfig','url'=>array('create')),
	array('label'=>'View SystemConfig','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage SystemConfig','url'=>array('admin')),
	);
	?>

	<h1>Update SystemConfig <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>