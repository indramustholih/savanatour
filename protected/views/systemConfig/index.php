<?php
$this->breadcrumbs=array(
	'System Configs',
);

$this->menu=array(
array('label'=>'Create SystemConfig','url'=>array('create')),
array('label'=>'Manage SystemConfig','url'=>array('admin')),
);
?>

<h1>System Configs</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
