<?php
$this->breadcrumbs=array(
	'System Configs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List SystemConfig','url'=>array('index')),
array('label'=>'Manage SystemConfig','url'=>array('admin')),
);
?>

<h1>Create SystemConfig</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>