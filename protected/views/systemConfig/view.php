<?php
$this->breadcrumbs=array(
	'System Configs'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List SystemConfig','url'=>array('index')),
array('label'=>'Create SystemConfig','url'=>array('create')),
array('label'=>'Update SystemConfig','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete SystemConfig','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage SystemConfig','url'=>array('admin')),
);
?>

<h1>View SystemConfig #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'key',
		'value',
		'keterangan',
),
)); ?>
