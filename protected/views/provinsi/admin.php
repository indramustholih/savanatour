<?php
$this->breadcrumbs=array(
	'Provinsis'=>array('index'),
	'Manage',
);

$this->pageTitle = 'Daftar Provinsi';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('provinsi-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<div class="box box-parimary">
	<div class="box-body">
	<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'provinsi-grid',
	'type'=>'striped hover bordered',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			array(
			      'name'=>'pulau_id',
			      'filter'=>CHtml::listData(Pulau::model()->findAll(), 'id', 'nama'),
			      'value'=>'$data->pulau->nama',
			),
			'nama',
			'keterangan',
			//'create_time',
			//'update_time',
			//'create_user_id',
			/*
			'update_user_id',
			*/
	array(
		'header'=>'Pilihan',
		'class'=>'booster.widgets.TbButtonColumn',
		'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'80px',
				),
	),
	),
	)); ?>
	</div>
	<div class="box-footer">
	<?php $this->widget('booster.widgets.TbButton', array(
			'buttonType'=>'link',
			'htmlOptions'=>array('class'=>'btn-flat'),
			'url'=>array('create'),
			'context'=>'primary',
			'icon'=>'plus white',
			'label'=>'Tambah Provinsi',
		)); ?>
	</div>
</div>
