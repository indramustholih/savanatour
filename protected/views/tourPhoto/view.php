<?php
$this->breadcrumbs=array(
	'Tour Photos'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TourPhoto','url'=>array('index')),
array('label'=>'Create TourPhoto','url'=>array('create')),
array('label'=>'Update TourPhoto','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TourPhoto','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TourPhoto','url'=>array('admin')),
);
?>

<h1>View TourPhoto #<?php echo $model->id; ?></h1>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'tour_id',
		'keterangan',
		'photo',
		'cover',
		'create_time',
		'update_time',
		'create_user_id',
		'update_user_id',
),
)); ?>
