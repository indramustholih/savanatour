<?php
$this->breadcrumbs=array(
	'Tour Photos',
);

$this->menu=array(
array('label'=>'Create TourPhoto','url'=>array('create')),
array('label'=>'Manage TourPhoto','url'=>array('admin')),
);
?>

<h1>Tour Photos</h1>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
