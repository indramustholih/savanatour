<?php
$this->breadcrumbs=array(
	'Tour Photos'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TourPhoto','url'=>array('index')),
array('label'=>'Manage TourPhoto','url'=>array('admin')),
);
?>

<h1>Create TourPhoto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>