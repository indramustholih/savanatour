<?php
$this->breadcrumbs=array(
	'Tour Photos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TourPhoto','url'=>array('index')),
	array('label'=>'Create TourPhoto','url'=>array('create')),
	array('label'=>'View TourPhoto','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TourPhoto','url'=>array('admin')),
	);
	?>

	<h1>Update TourPhoto <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>