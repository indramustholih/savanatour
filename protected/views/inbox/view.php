<?php
$this->breadcrumbs=array(
	'Inboxes'=>array('index'),
	$model->name,
);

$this->pageTitle='Read Inbox ';
?>

<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title"><i class="fa fa-envelope-o"></i> Visitor Message</h3>
      <div class="box-tools pull-right">
        <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Previous"><i class="fa fa-chevron-left"></i></a>
        <a href="#" class="btn btn-box-tool" data-toggle="tooltip" title="Next"><i class="fa fa-chevron-right"></i></a>
      </div>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
      <div class="mailbox-read-info">
        <h3><?php echo $model->subjek;?></h3>
        <h5><b>From: </b><?php echo $model->name;?> | <b>Email:</b> <?php echo $model->email;?> | <b>Phone: </b><?php echo $model->phone;?> <span class="mailbox-read-time pull-right"><?php echo Tools::getDateTime($model->create_time);?></span></h5>
      </div><!-- /.mailbox-read-info -->
      <div class="mailbox-controls with-border">
        <div class="btn-group">
          <?php $this->widget('booster.widgets.TbButton', array(
                            'buttonType'=>'link',
                            'icon'=>'trash white',
                            'label'=>'',
                            'context'=>'default',
                            'url'=>array('inbox/delete','id'=>$model->id),
                            'htmlOptions'=>array('data-toggle'=>'tooltip', 'title'=>'Hapus', 'class'=>'btn btn-default btn-sm', 'confirm' => 'Anda yakin akan menghapus data ini?'),
            )); ?>
            <button class="btn btn-default btn-sm" data-toggle="tooltip" title="Tandai"><i class="fa fa-tag"></i></button>
            <button class="btn btn-default btn-sm" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></button>
        </div><!-- /.btn-group -->
        
      </div><!-- /.mailbox-controls -->
      <div class="mailbox-read-message">
            <?php echo $model->message;?>
      </div><!-- /.mailbox-read-message -->
    </div><!-- /.box-body -->
    <div class="box-footer">
        <?php $this->widget('booster.widgets.TbButton', array(
                    'buttonType'=>'link',
                    'htmlOptions'=>array('class'=>'btn-flat'),
                    'url'=>array('admin'),
                    'context'=>'primary',
                    'icon'=>'list white',
                    'label'=>'Inbox List',
            )); ?>
    </div><!-- /.box-footer -->
  </div><!-- /. box -->
