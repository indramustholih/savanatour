<?php
$this->breadcrumbs=array(
	'Inboxes'=>array('index'),
	'Manage',
);

$this->pageTitle = 'Inbox';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('inbox-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<div class="box box-parimary">
	<div class="box-body">
	<?php $this->widget('booster.widgets.TbGridView',array(
	'id'=>'inbox-grid',
	'type' =>'hover striped',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
			//'id',
			array(
				'header'=>'No',
				'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
				'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'30px',
				),
			),
			array(
			      'name'=>'is_read',
			      'filter'=>array('0'=>'Unread', '1'=>'Read'),
			      'header'=>'Read/Unread',
			      'type'=>'raw',
			      'value'=>'$data->is_read ==0 ? CHtml::tag("i",array("class"=>"fa fa-envelope"),"") :
					 CHtml::tag("i",array("class"=>"fa fa-envelope-o"),"") ',
					'htmlOptions'=>array(
						'style'=>'text-align:center',
						'width'=>'100px',
					),
			),
			'subjek',
			'name',
			'email',
			'phone',
			//'message',
			
			/*
			'is_publish',
			'create_time',
			'update_time',
			'create_user_id',
			'update_user_id',
			*/
	array(
		'header'=>'Pilihan',
		'class'=>'booster.widgets.TbButtonColumn',
		'template'=>'{view} {delete}',
		'htmlOptions'=>array(
					'style'=>'text-align:center',
					'width'=>'80px',
				),
	),
	),
	)); ?>
	</div>
</div>
