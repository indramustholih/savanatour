-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16 Okt 2016 pada 10.38
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `savana`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `blog_kategori_id` int(11) NOT NULL,
  `cover_img` text,
  `judul` varchar(255) NOT NULL,
  `publish` int(11) NOT NULL DEFAULT '1',
  `konten` text,
  `tags` text,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `blog_kategori_id`, `cover_img`, `judul`, `publish`, `konten`, `tags`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 1, '1462893509_1.jpg', 'Menikmati Kesunyian Ranu Kumbolo', 1, '<p>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour. There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour users..</p>\r\n\r\n<blockquote>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour.</blockquote>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour. There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour users.</p>\r\n', 'Ranu Kumbolo, Semeru, Malang, Jawa Timur', '2016-05-10 15:23:51', '2016-05-11 16:07:47', NULL, 1),
(2, 3, '1462894238_3.jpg', 'Menikmati Keindahan Alam Kota Malan', 1, '<p>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour. There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour users..</p>\r\n\r\n<blockquote>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour.</blockquote>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour. There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour users.</p>\r\n', 'Malang, Jawa Timur, Wisata Indonesia', '2016-05-10 15:23:51', '2016-05-20 19:31:56', 1, 1),
(3, 2, '1462894448_2.jpg', 'Pendakian Yang Terkenang Seumur Hidup', 1, '<p>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour. There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour users..</p>\r\n\r\n<blockquote>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour.</blockquote>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour. There are many variations of passages of Lorem Ipsum available, but the joy have suffered alteration in some format, by injected humour users.</p>\r\n', 'Semeru, Pendaki Gunung, Indonesia', '2016-05-10 15:34:08', '2016-05-11 16:11:25', 1, 1),
(4, 3, '1463771232_8.jpg', 'Menikmati Keindahan Kota Malang Malam Hari', 1, '<p style="text-align:justify">Ketika berbicara pusat seni dan budaya di Pulau Bali sekaligus suasana yang nyaman maka semua sepakat Anda akan diarahkan ke objek wisata favorit ini. Ubud terletak di Kabupaten Gianyar Bali, tempat ini menawarkan berbagai keindahan alam, pentas seni dan budaya, serta hasil kerajinan masyarakat Bali yang eksotis.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:justify">Dengan lingkungan yang masih alami, daerah ini merupakan daerah sumber inspirasi bagi para seniman, termasuk seniman luar negeri, terutama seniman Eropa. Berada di antara sawah, hutan, dan jurang-jurang gunung yang membuat alamnya sangat indah. Selain itu Ubud dikenal sebagai tempat dimana seni dan budaya Bali terjaga dengan baik. Denyut nadi kehidupan masyarakat Ubud tidak bisa dilepaskan dari kesenian. Di sini terdapat galeri-galeri seni hingga arena pertunjukan musik dan tari digelar setiap malamnya secara bergantian di berbagai penjuru desa.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:justify">Ubud adalah tempat yang sempurna jika Anda tertarik untuk belajar berbagai seni Bali seperti melukis, mengukir, atau menari. Ya, karena di sini banyak terdapat sanggar seni. Bahkan bila tidak pun maka di Ubud sudah cukup memuaskan Anda untuk sekedar menikmati suasana Bali tradisional lalu membawa pulang oleh-oleh seni kreasi tangan yang berkualitas tinggi.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:justify">Ubud adalah tempat yang tepat untuk Anda mencari kedamaian dan ketenangan. Ubud menawarkan tempat peristirahatan dari rutinitas kota yang menjemukan. Di Ubud Anda dapat memanjakan tubuh dan pikiran, karena di sini terdapat restoran dan spa berkualitas yang akan membuat Anda rileks dan puas. Berjalan-jalanlah melewati sawah yang menghijau, melihat karya seni dan budaya yang eksotis, bercengkrama dengan penduduknya yang ramah, dan melihat prosesi adat dimana wanita Bali berjalan menuju pura dengan anggun sambil menyeimbangkan tumpukan sesajen buah-buahan di kepalanya. Itu hanyalah sepenggal kesan mendalam yang dapat ditangkap saat mengunjungi tempat yang indah ini.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p style="text-align:justify">Cara terbaik untuk menyelami tradisi dan budaya Bali adalah melebur dengan penduduk setempat dan melihat keseharian mereka. Di Ubud, Anda dapat mengunjungi pura dan desa yang relatif tidak banyak berubah selama bertahun-tahun. Sebuah kenangan akan suasana dari masa Hindu di Jawa dahulu yang sulit akan Anda temukan lagi sekarang ini.</p>\r\n', '', '2016-05-20 19:07:12', NULL, 1, NULL),
(5, 3, '1463895859_Pesona-Danau-Batur-Kintamani-Yang-Tak-Pernah-Pudar.jpg', 'Pesona Danau Batur Kintamani yang Tak Pernah Pudar', 1, '<p>Anda dan keluarga sudah pernah berlibur untuk mengunjungi danau Batur Kintamani? Salah satu objek wisata di Pulau Dewata yang selalu ramai pengunjung dari berbagai penjuru ini. Jika belum pernah, mari mengenal lebih jauh mengenai pesona Danau Batur Kintamani di Pulau Dewata atau Pulau Bali yang tak akan pernah pudar ini dengan menyimak uraian di bawah.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; Danau Batur Kintamani adalah salah satu danau terluas dari keempat danau yang ada di Pulau Bali. Danau ini terletak tepat di lereng Gunung Batur sehingga dinamakan Danau Batur. Sedangkan nama dari &lsquo;Kintamani&rsquo; merupakan nama salah satu objek wisata di Pulau Bali yang memiliki udara pegunungan berhawa sejuk di dekat Danau Batur, sehingga kawasan ini dinamakan sebagai Danau Batur Kintamani.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Danau ini terletak pada ketinggian 1.050 meter diatas permukaan air laut dengan luas sekitar &nbsp;dan kedalaman danau yang bervariasi dengan rata-rata kedalamannya mencapai 50,8 meter. Pesona Danau Batur Kintamani ini tak akan pernah pudar karena di sekeliling danau terdapat pegunungan dengan pepohonan rindang membuat suasana menjadi sejuk nan asri serta air danau yang sangat alami karena berasal dari air hujan dan dari rembesan air hutan yang berada di pegunungan.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Hal ini tentu membuat para pengunjung betah berlama-lama untuk menikmati udara yang masih alami di sekitar danau. Nah, jika anda ingin berkeliling danau, anda juga bisa menggunakan sampan atau perahu yang telah disediakan oleh masyarakat setempat tentunya dengan harga sewa yang tergolong murah. Jika anda berkeliling danau dengan sampan atau perahu tentu anda akan bisa menikmati pesona danau dengan lebih santai.</p>\r\n\r\n<p>Di danau Batur Kintamani, selain anda bisa menikmati suasana sejuk dan bisa berkeliling danau dengan sampan atau perahu, anda juga bisa bersantai sambil memancing. Karena selain dimanfaatkan sebagai objek wisata, danau ini juga dimanfaatkan sebagai sumber keanekaragaman hayati berbagai biota darat maupun biota air.</p>\r\n\r\n<p>Terdapat berbagai macam ikan yang tumbuh dan berkembang di danau ini sehingga anda bisa memancing ikan sepuasnya. Tak cukup itu saja, sebelum anda menuju ke danau Batur Kintamani anda bisa menikmati terapi air panas alam Desa Toya Bungkah kemudian baru start point trekking ke Gunung Batur.</p>\r\n\r\n<p>Nah, di sebelah barat dari danau ini juga terdapat sebuah desa tertua di Pulau Bali yaitu Desa Trunyan yang terkenal dengan tradisi unik dalam pemakaman mayat. Sehingga setelah puas menikmati pesona Danau Batur Kintamani, anda bisa langsung berkunjung ke Desa Trunyan untuk menyaksikan tradisi pemakaman mayat yang tergolong unik.</p>\r\n\r\n<p>Di kawasan ini juga telah disediakan beberapa restaurant dan hotel sebagai tempat makan dan beristirahat selepas jalan-jalan berkeliling danau. Dari restaurant dan juga hotel ini anda bisa menikmati keindahan danau dengan lebih jelas karena restaurant dan hotel terletak lebih tinggi dari danau.</p>\r\n\r\n<p>Dengan begitu, anda bisa melihat gemerlap biru danau dari atas sambil bersantap bersama keluarga atau rekan kerja. Selain itu, anda juga bisa menikmati pesona kemegahan Gunung Batur yang menjulang tinggi. Dengan berbagai keindahan inilah tentu pesona dari danau Batur Kintamani tak akan pernah pudar.</p>\r\n\r\n<p>Nah, uraian mengenai pesona Danau Batur Kintamani diatas dapat anda jadikan referensi liburan anda bersama keluarga atau rekan kerja. Karena kawasan ini sangat cocok untuk dijadikan tempat berlibur dan tempat refreshing melepaskan penat di akhir pekan.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 'Bali', '2016-05-22 06:44:19', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog_kategori`
--

CREATE TABLE `blog_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `blog_kategori`
--

INSERT INTO `blog_kategori` (`id`, `kategori`, `create_user_id`, `update_user_id`, `create_time`, `update_time`) VALUES
(1, 'Hiking', NULL, 1, '0000-00-00 00:00:00', '2016-05-11 16:02:26'),
(2, 'Snorkling', 1, NULL, '2016-05-11 16:02:41', '2016-05-11 16:02:41'),
(3, 'Backpacker', 1, 1, '2016-05-12 10:35:53', '2016-05-12 10:37:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `eksternal_link`
--

CREATE TABLE `eksternal_link` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `eksternal_link`
--

INSERT INTO `eksternal_link` (`id`, `judul`, `link`, `keterangan`) VALUES
(1, 'Indonesia.Travel', 'http://www.indonesia.travel', NULL),
(2, 'Dieng Culture Festival', '-', NULL),
(3, 'Mandara.id', '-', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `kabupaten_id` int(11) NOT NULL,
  `long` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `publish` int(11) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `event_photo`
--

CREATE TABLE `event_photo` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `photo` text NOT NULL,
  `cover` int(11) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `inbox`
--

CREATE TABLE `inbox` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `subjek` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `is_read` int(1) NOT NULL DEFAULT '0',
  `is_publish` int(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `inbox`
--

INSERT INTO `inbox` (`id`, `name`, `email`, `phone`, `subjek`, `message`, `is_read`, `is_publish`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'Aeng A. Sanusi', 'aeng.anwar@gmail.com', '-', 'Mohon Informasi Travel', ' \r\n\r\nHello John,\r\n\r\nKeffiyeh blog actually fashion axe vegan, irony biodiesel. Cold-pressed hoodie chillwave put a bird on it aesthetic, bitters brunch meggings vegan iPhone. Dreamcatcher vegan scenester mlkshk. Ethical master cleanse Bushwick, occupy Thundercats banjo cliche ennui farm-to-table mlkshk fanny pack gluten-free. Marfa butcher vegan quinoa, bicycle rights disrupt tofu scenester chillwave 3 wolf moon asymmetrical taxidermy pour-over. Quinoa tote bag fashion axe, Godard disrupt migas church-key tofu blog locavore. Thundercats cronut polaroid Neutra tousled, meh food truck selfies narwhal American Apparel.\r\n\r\nRaw denim McSweeney''s bicycle rights, iPhone trust fund quinoa Neutra VHS kale chips vegan PBR&B literally Thundercats +1. Forage tilde four dollar toast, banjo health goth paleo butcher. Four dollar toast Brooklyn pour-over American Apparel sustainable, lumbersexual listicle gluten-free health goth umami hoodie. Synth Echo Park bicycle rights DIY farm-to-table, retro kogi sriracha dreamcatcher PBR&B flannel hashtag irony Wes Anderson. Lumbersexual Williamsburg Helvetica next level. Cold-pressed slow-carb pop-up normcore Thundercats Portland, cardigan literally meditation lumbersexual crucifix. Wayfarers raw denim paleo Bushwick, keytar Helvetica scenester keffiyeh 8-bit irony mumblecore whatever viral Truffaut.\r\n\r\nPost-ironic shabby chic VHS, Marfa keytar flannel lomo try-hard keffiyeh cray. Actually fap fanny pack yr artisan trust fund. High Life dreamcatcher church-key gentrify. Tumblr stumptown four dollar toast vinyl, cold-pressed try-hard blog authentic keffiyeh Helvetica lo-fi tilde Intelligentsia. Lomo locavore salvia bespoke, twee fixie paleo cliche brunch Schlitz blog McSweeney''s messenger bag swag slow-carb. Odd Future photo booth pork belly, you probably haven''t heard of them actually tofu ennui keffiyeh lo-fi Truffaut health goth. Narwhal sustainable retro disrupt.\r\n\r\nSkateboard artisan letterpress before they sold out High Life messenger bag. Bitters chambray leggings listicle, drinking vinegar chillwave synth. Fanny pack hoodie American Apparel twee. American Apparel PBR listicle, salvia aesthetic occupy sustainable Neutra kogi. Organic synth Tumblr viral plaid, shabby chic single-origin coffee Etsy 3 wolf moon slow-carb Schlitz roof party tousled squid vinyl. Readymade next level literally trust fund. Distillery master cleanse migas, Vice sriracha flannel chambray chia cronut.\r\n\r\nThanks,\r\nJane\r\n', 1, 1, '2016-05-21 15:44:33', '2016-05-21 17:36:05', 1, 1),
(2, 'Susilawati', 'aeng.anwar.info@gmail.com', '', 'Mohon info paket  ', '', 1, 1, '2016-05-21 16:08:54', '2016-10-15 06:51:44', 1, 1),
(3, 'Ahda', 'ahda@gmail.com', '0821 2708 2909', 'Info Banyuwangi', 'Mohon info paket wisata untuk daerah banyu wangi', 1, 1, '2016-05-21 17:34:27', '2016-05-21 17:35:31', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kabupaten`
--

CREATE TABLE `kabupaten` (
  `id` int(11) NOT NULL,
  `provinsi_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `jenis` int(11) NOT NULL DEFAULT '0' COMMENT '0=kabupaten, 1=kota',
  `keterangan` text,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kabupaten`
--

INSERT INTO `kabupaten` (`id`, `provinsi_id`, `nama`, `jenis`, `keterangan`, `is_active`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 8, 'BANDUNG', 0, '', 1, '2013-12-17 10:00:44', '2013-12-17 12:50:02', 1, 1),
(2, 8, 'CIAMIS', 0, '', 1, '2013-12-17 10:25:25', '2013-12-17 12:50:02', 1, 1),
(3, 8, 'KOTA BANDUNG', 1, '', 1, '2013-12-17 10:25:39', '2013-12-17 12:50:02', 1, 1),
(4, 8, 'KOTA CIMAHI', 1, '', 1, '2013-12-17 10:25:59', '2013-12-17 13:07:06', 1, 1),
(5, 1, 'ACEH BARAT', 0, '', 1, '2013-12-17 11:43:31', '2014-02-22 03:06:24', 1, 1),
(6, 1, 'ACEH BARAT DAYA', 0, '', 1, '2013-12-17 11:43:56', '2013-12-17 12:50:02', 1, 1),
(7, 1, 'ACEH BESAR', 0, '', 1, '2013-12-17 11:44:40', '2013-12-17 12:50:02', 1, 1),
(8, 1, 'ACEH JAYA', 0, '', 1, '2013-12-17 11:45:54', '2013-12-17 12:50:02', 1, 1),
(9, 1, 'ACEH SELATAN', 0, '', 1, '2013-12-17 11:46:23', '2013-12-17 12:50:02', 1, 1),
(10, 1, 'ACEH SINGKIL', 0, '', 1, '2013-12-17 11:46:46', '2013-12-17 12:50:02', 1, 1),
(11, 1, 'ACEH TAMIANG', 0, '', 1, '2013-12-17 11:47:11', '2013-12-17 12:50:02', 1, 1),
(12, 1, 'ACEH TENGAH', 0, '', 1, '2013-12-17 11:47:40', '2013-12-17 12:50:02', 1, 1),
(13, 1, 'ACEH TENGGARA', 0, '', 1, '2013-12-17 11:48:23', '2013-12-17 12:50:02', 1, 1),
(14, 1, 'ACEH TIMUR', 0, '', 1, '2013-12-17 11:48:43', '2013-12-17 12:50:02', 1, 1),
(15, 1, 'ACEH UTARA', 0, '', 1, '2013-12-17 11:49:05', '2013-12-17 12:50:02', 1, 1),
(16, 1, 'BENER MERIAH', 0, '', 1, '2013-12-17 11:49:55', '2013-12-17 12:50:02', 1, 1),
(17, 1, 'BIREUEN', 0, '', 1, '2013-12-17 11:50:32', '2013-12-17 12:50:02', 1, 1),
(18, 1, 'GAYO LUWES', 0, '', 1, '2013-12-17 11:51:01', '2013-12-17 12:50:02', 1, 1),
(19, 1, 'KOTA BANDA ACEH', 1, '', 1, '2013-12-17 11:51:19', '2013-12-17 12:50:02', 1, 1),
(20, 1, 'KOTA LANGSA', 1, '', 1, '2013-12-17 11:51:36', '2013-12-17 12:50:02', 1, 1),
(21, 1, 'KOTA LHOKSEUMAWE', 1, '', 1, '2013-12-17 11:52:01', '2013-12-17 12:50:02', 1, 1),
(22, 1, 'KOTA SABANG', 1, '', 1, '2013-12-17 11:52:19', '2013-12-17 12:50:02', 1, 1),
(23, 1, 'KOTA SUBULUSSALAM', 1, '', 1, '2013-12-17 11:52:43', '2013-12-17 12:50:03', 1, 1),
(24, 1, 'NAGAN RAYA', 0, '', 1, '2013-12-17 11:53:00', '2013-12-17 12:50:03', 1, 1),
(25, 1, 'PIDIE', 0, '', 1, '2013-12-17 11:53:13', '2013-12-17 12:50:03', 1, 1),
(26, 1, 'PIDIE JAYA', 0, '', 1, '2013-12-17 11:53:27', '2013-12-17 12:50:03', 1, 1),
(27, 1, 'SIMEULUE', 0, '', 1, '2013-12-17 11:53:48', '2013-12-17 12:50:03', 1, 1),
(28, 2, 'BADUNG', 0, '', 1, '2013-12-17 11:54:58', '2013-12-17 12:50:03', 1, 1),
(29, 2, 'BANGLI', 0, '', 1, '2013-12-17 11:55:13', '2013-12-17 12:50:03', 1, 1),
(30, 2, 'BULELENG', 0, '', 1, '2013-12-17 11:55:28', '2013-12-17 12:50:03', 1, 1),
(31, 2, 'GIANYAR', 0, '', 1, '2013-12-17 11:55:44', '2013-12-17 12:50:03', 1, 1),
(32, 2, 'JEMBRANA', 0, '', 1, '2013-12-17 11:55:58', '2013-12-17 12:50:03', 1, 1),
(33, 2, 'KARANGASEM', 0, '', 1, '2013-12-17 11:56:12', '2013-12-17 12:50:03', 1, 1),
(34, 2, 'KLUNGKUNG', 0, '', 1, '2013-12-17 11:56:37', '2013-12-17 12:50:03', 1, 1),
(35, 2, 'KOTA DENPASAR', 1, '', 1, '2013-12-17 11:56:56', '2013-12-17 12:50:03', 1, 1),
(36, 2, 'TABANAN', 0, '', 1, '2013-12-17 11:57:11', '2013-12-17 12:50:03', 1, 1),
(37, 3, 'KOTA CILEGON', 1, '', 1, '2013-12-17 11:57:54', '2013-12-17 12:50:03', 1, 1),
(38, 3, 'KOTA SERANG', 1, '', 1, '2013-12-17 11:58:19', '2013-12-17 12:50:03', 1, 1),
(39, 3, 'KOTA TANGGERANG', 1, '', 1, '2013-12-17 11:58:42', '2013-12-17 12:50:03', 1, 1),
(40, 3, 'KOTA TENGGERANG SELATAN', 1, '', 1, '2013-12-17 11:59:03', '2013-12-17 12:50:03', 1, 1),
(41, 3, 'LEBAK', 0, '', 1, '2013-12-17 11:59:21', '2013-12-17 12:50:03', 1, 1),
(42, 3, 'PANDEGLANG', 0, '', 1, '2013-12-17 11:59:54', '2013-12-17 12:50:03', 1, 1),
(43, 3, 'SERANG', 0, '', 1, '2013-12-17 12:00:10', '2013-12-17 12:50:03', 1, 1),
(44, 3, 'TANGGERANG', 0, '', 1, '2013-12-17 12:00:25', '2013-12-17 12:50:03', 1, 1),
(45, 4, 'BENGKULU SELATAN', 0, '', 1, '2013-12-17 12:26:37', '2013-12-17 12:50:03', 1, 1),
(46, 4, 'BENGKULU TENGAH', 0, '', 1, '2013-12-17 12:29:56', '2013-12-17 12:50:03', 1, 1),
(47, 4, 'BENGKULU UTARA', 0, '', 1, '2013-12-17 12:30:17', '2013-12-17 12:50:03', 1, 1),
(48, 4, 'KAUR', 0, '', 1, '2013-12-17 12:30:32', '2013-12-17 12:50:03', 1, 1),
(49, 4, 'KEPAHIANG', 0, '', 1, '2013-12-17 12:31:10', '2013-12-17 12:50:03', 1, 1),
(50, 4, 'KOTA BENGKULU', 1, '', 1, '2013-12-17 12:31:38', '2013-12-17 12:50:03', 1, 1),
(51, 4, 'LEBONG', 0, '', 1, '2013-12-17 12:32:10', '2013-12-17 12:50:04', 1, 1),
(52, 4, 'MUKOMUKO', 0, '', 1, '2013-12-17 12:32:30', '2013-12-17 12:50:04', 1, 1),
(53, 4, 'REJANG LEBONG', 0, '', 1, '2013-12-17 12:32:51', '2013-12-17 12:50:04', 1, 1),
(54, 4, 'SELUMA', 0, '', 1, '2013-12-17 12:33:18', '2013-12-17 12:50:04', 1, 1),
(55, 34, 'BANTUL', 0, '', 1, '2013-12-17 12:34:13', '2013-12-17 12:50:04', 1, 1),
(56, 34, 'GUNUNG KIDUL', 0, '', 1, '2013-12-17 12:35:21', '2013-12-17 12:50:04', 1, 1),
(57, 34, 'KOTA YOGYAKARTA', 1, '', 1, '2013-12-17 12:36:49', '2013-12-17 12:50:04', 1, 1),
(58, 34, 'KULON PROGO', 0, '', 1, '2013-12-17 12:37:07', '2013-12-17 12:50:04', 1, 1),
(59, 34, 'SLEMAN', 0, '', 1, '2013-12-17 12:37:24', '2013-12-17 12:50:04', 1, 1),
(60, 6, 'JAKARTA BARAT', 0, '', 1, '2013-12-17 12:39:24', '2013-12-17 12:50:04', 1, 1),
(61, 6, 'JAKARTA PUSAT', 0, '', 1, '2013-12-17 12:39:41', '2013-12-17 12:50:04', 1, 1),
(62, 6, 'JAKARTA SELATAN', 0, '', 1, '2013-12-17 12:39:57', '2013-12-17 12:50:04', 1, 1),
(63, 6, 'JAKARTA TIMUR', 0, '', 1, '2013-12-17 12:40:11', '2013-12-17 12:50:04', 1, 1),
(64, 6, 'JAKARTA UTARA', 0, '', 1, '2013-12-17 12:40:28', '2013-12-17 12:50:04', 1, 1),
(65, 6, 'KEPULAUAN SERIBU', 0, '', 1, '2013-12-17 12:40:51', '2013-12-17 12:50:04', 1, 1),
(66, 5, 'BOALEMO', 0, '', 1, '2013-12-17 12:44:01', '2013-12-17 12:50:04', 1, 1),
(67, 5, 'BONE BOLANGO', 0, '', 1, '2013-12-17 12:44:14', '2013-12-17 12:50:04', 1, 1),
(68, 5, 'GORONTALO', 0, '', 1, '2013-12-17 12:44:29', '2013-12-17 12:50:04', 1, 1),
(69, 5, 'GORONTALO UTARA', 0, '', 1, '2013-12-17 12:44:43', '2013-12-17 12:50:04', 1, 1),
(70, 5, 'KOTA GORONTALO 	', 1, '', 1, '2013-12-17 12:44:58', '2013-12-17 12:50:04', 1, 1),
(71, 5, 'PAHUWATO', 0, '', 1, '2013-12-17 12:45:16', '2013-12-17 12:50:04', 1, 1),
(72, 7, 'BATANGHARI', 0, '', 1, '2013-12-17 12:51:03', NULL, 1, NULL),
(73, 7, 'BUNGO', 0, '', 1, '2013-12-17 12:51:16', NULL, 1, NULL),
(74, 7, 'KERINCI', 0, '', 1, '2013-12-17 12:51:28', NULL, 1, NULL),
(75, 7, 'KOTA JAMBI', 1, '', 1, '2013-12-17 12:51:42', NULL, 1, NULL),
(76, 7, 'KOTA SUNGAI PENUH', 1, '', 1, '2013-12-17 12:53:07', '2013-12-17 12:53:42', 1, 1),
(77, 7, 'MERANGIN', 0, '', 1, '2013-12-17 12:53:26', NULL, 1, NULL),
(78, 7, 'MUARO JAMBI', 0, '', 1, '2013-12-17 12:54:22', NULL, 1, NULL),
(79, 7, 'SAROLANGUN', 0, '', 1, '2013-12-17 12:54:35', NULL, 1, NULL),
(80, 7, 'TANJUNG JABUNG BARAT', 0, '', 1, '2013-12-17 12:54:48', NULL, 1, NULL),
(81, 7, 'TANJUNG JABUNG TIMUR', 0, '', 1, '2013-12-17 12:55:04', NULL, 1, NULL),
(82, 7, 'TEBO', 0, '', 1, '2013-12-17 12:55:19', NULL, 1, NULL),
(83, 8, 'BANDUNG BARAT', 0, '', 1, '2013-12-17 12:58:08', NULL, 1, NULL),
(84, 8, 'BEKASI', 0, '', 1, '2013-12-17 12:58:29', NULL, 1, NULL),
(85, 8, 'BOGOR', 0, '', 1, '2013-12-17 12:58:45', NULL, 1, NULL),
(86, 8, 'CIANJUR', 0, '', 1, '2013-12-17 12:58:58', NULL, 1, NULL),
(87, 8, 'CIREBON', 0, '', 1, '2013-12-17 12:59:13', NULL, 1, NULL),
(88, 8, 'GARUT', 0, '', 1, '2013-12-17 12:59:27', NULL, 1, NULL),
(89, 8, 'INDRAMAYU', 0, '', 1, '2013-12-17 12:59:39', NULL, 1, NULL),
(90, 8, 'KARAWANG', 0, '', 1, '2013-12-17 12:59:53', NULL, 1, NULL),
(91, 8, 'KOTA BANJAR', 1, '', 1, '2013-12-17 13:00:29', NULL, 1, NULL),
(92, 8, 'KOTA BEKASI', 1, '', 1, '2013-12-17 13:00:45', NULL, 1, NULL),
(93, 8, 'KOTA BOGOR', 1, '', 1, '2013-12-17 13:01:01', NULL, 1, NULL),
(94, 8, 'KOTA CIREBON', 1, '', 1, '2013-12-17 13:01:16', NULL, 1, NULL),
(95, 8, 'KOTA DEPOK', 1, '', 1, '2013-12-17 13:01:30', NULL, 1, NULL),
(96, 8, 'KOTA SUKABUMI', 1, '', 1, '2013-12-17 13:01:45', NULL, 1, NULL),
(97, 8, 'KOTA TASIKMALAYA', 1, '', 1, '2013-12-17 13:02:01', NULL, 1, NULL),
(98, 8, 'KUNINGAN', 0, '', 1, '2013-12-17 13:02:16', '2013-12-17 13:04:48', 1, 1),
(99, 8, 'MAJALENGKA', 0, '', 1, '2013-12-17 13:05:01', NULL, 1, NULL),
(100, 8, 'PURWAKARTA', 0, '', 1, '2013-12-17 13:05:13', NULL, 1, NULL),
(101, 8, 'SUBANG', 0, '', 1, '2013-12-17 13:05:27', NULL, 1, NULL),
(102, 8, 'SUKABUMI', 0, '', 1, '2013-12-17 13:05:39', NULL, 1, NULL),
(103, 8, 'SUMEDANG', 0, '', 1, '2013-12-17 13:05:51', NULL, 1, NULL),
(104, 8, 'TASIKMALAYA', 0, '', 1, '2013-12-17 13:06:03', NULL, 1, NULL),
(105, 9, 'BANJARNEGARA', 0, '', 1, '2013-12-17 13:08:56', NULL, 1, NULL),
(106, 9, 'BANYUMAS', 0, '', 1, '2013-12-17 13:09:14', NULL, 1, NULL),
(107, 9, 'BATANG', 0, '', 1, '2013-12-17 13:09:31', NULL, 1, NULL),
(108, 9, 'BLORA', 0, '', 1, '2013-12-17 13:09:44', NULL, 1, NULL),
(109, 9, 'BOYOLALI', 0, '', 1, '2013-12-17 13:09:56', NULL, 1, NULL),
(110, 9, 'BREBES', 0, '', 1, '2013-12-17 13:10:08', NULL, 1, NULL),
(111, 9, 'CILACAP', 0, '', 1, '2013-12-17 13:10:21', NULL, 1, NULL),
(112, 9, 'DEMAK', 0, '', 1, '2013-12-17 13:10:35', NULL, 1, NULL),
(113, 9, 'GROBOGAN', 0, '', 1, '2013-12-17 13:10:49', NULL, 1, NULL),
(114, 9, 'JEPARA', 0, '', 1, '2013-12-17 13:11:05', NULL, 1, NULL),
(115, 9, 'KARANGANYAR', 0, '', 1, '2013-12-17 13:11:17', '2013-12-17 13:21:58', 1, 1),
(116, 9, 'KEBUMEN', 0, '', 1, '2013-12-17 13:11:31', NULL, 1, NULL),
(117, 9, 'KENDAL', 0, '', 1, '2013-12-17 13:11:43', NULL, 1, NULL),
(118, 9, 'KLATEN', 0, '', 1, '2013-12-17 13:11:56', NULL, 1, NULL),
(119, 9, 'KOTA MAGELANG', 1, '', 1, '2013-12-17 13:12:12', '2013-12-17 13:12:49', 1, 1),
(120, 9, 'KOTA PEKALONGAN', 1, '', 1, '2013-12-17 13:12:29', NULL, 1, NULL),
(121, 9, 'KOTA SALATIGA', 1, '', 1, '2013-12-17 13:13:11', NULL, 1, NULL),
(122, 9, 'KOTA SEMARANG', 1, '', 1, '2013-12-17 13:13:28', NULL, 1, NULL),
(123, 9, 'KOTA SURAKARTA', 1, '', 1, '2013-12-17 13:13:44', '2013-12-17 13:20:23', 1, 1),
(124, 9, 'KOTA TEGAL', 1, '', 1, '2013-12-17 13:13:58', NULL, 1, NULL),
(125, 9, 'KUDUS', 0, '', 1, '2013-12-17 13:14:08', NULL, 1, NULL),
(126, 9, 'MAGELANG', 0, '', 1, '2013-12-17 13:14:20', NULL, 1, NULL),
(127, 9, 'PATI', 0, '', 1, '2013-12-17 13:14:32', NULL, 1, NULL),
(128, 9, 'PEKALONGAN', 0, '', 1, '2013-12-17 13:14:46', NULL, 1, NULL),
(129, 9, 'PEMALANG', 0, '', 1, '2013-12-17 13:15:02', NULL, 1, NULL),
(130, 9, 'PURBALINGGA', 0, '', 1, '2013-12-17 13:15:16', NULL, 1, NULL),
(131, 9, 'PURWOREJO', 0, '', 1, '2013-12-17 13:15:33', NULL, 1, NULL),
(132, 9, 'REMBANG', 0, '', 1, '2013-12-17 13:15:48', NULL, 1, NULL),
(133, 9, 'SEMARANG', 0, '', 1, '2013-12-17 13:16:00', NULL, 1, NULL),
(134, 9, 'SRAGEN', 0, '', 1, '2013-12-17 13:16:17', NULL, 1, NULL),
(135, 9, 'SUKOHARJO', 0, '', 1, '2013-12-17 13:16:28', NULL, 1, NULL),
(136, 9, 'TEGAL', 0, '', 1, '2013-12-17 13:16:41', NULL, 1, NULL),
(137, 9, 'TEMANGGUNG', 0, '', 1, '2013-12-17 13:17:00', NULL, 1, NULL),
(138, 9, 'WONOGIRI', 0, '', 1, '2013-12-17 13:17:14', NULL, 1, NULL),
(139, 9, 'WONOSOBO', 0, '', 1, '2013-12-17 13:17:26', NULL, 1, NULL),
(140, 10, 'BANGKALAN', 0, '', 1, '2013-12-17 15:32:21', NULL, 1, NULL),
(141, 10, 'BANYUWANGI', 0, '', 1, '2013-12-17 15:32:37', NULL, 1, NULL),
(142, 10, 'BLITAR', 0, '', 1, '2013-12-17 15:33:46', NULL, 1, NULL),
(143, 10, 'BOJONEGORO', 0, '', 1, '2013-12-17 15:34:01', NULL, 1, NULL),
(144, 10, 'BONDOWOSO', 0, '', 1, '2013-12-17 15:35:00', NULL, 1, NULL),
(145, 10, 'GRESIK', 0, '', 1, '2013-12-17 15:36:03', NULL, 1, NULL),
(146, 10, 'JEMBER', 0, '', 1, '2013-12-17 15:36:20', NULL, 1, NULL),
(147, 10, 'JOMBANG', 0, '', 1, '2013-12-17 15:36:36', NULL, 1, NULL),
(148, 10, 'KEDIRI', 0, '', 1, '2013-12-17 15:36:49', NULL, 1, NULL),
(149, 10, 'KOTA BATU', 1, '', 1, '2013-12-17 15:37:03', '2013-12-17 15:40:00', 1, 1),
(150, 10, 'KOTA BLITAR', 1, '', 1, '2013-12-17 15:37:20', '2013-12-17 15:40:24', 1, 1),
(151, 10, 'KOTA KEDIRI', 1, '', 1, '2013-12-17 15:37:32', '2013-12-17 15:40:37', 1, 1),
(152, 10, 'KOTA MADIUN', 1, '', 1, '2013-12-17 15:37:50', '2013-12-17 15:40:51', 1, 1),
(153, 10, 'KOTA MALANG', 1, '', 1, '2013-12-17 15:38:04', '2013-12-17 15:41:19', 1, 1),
(154, 10, 'KOTA MOJOKERTO', 1, '', 1, '2013-12-17 15:38:31', NULL, 1, NULL),
(155, 10, 'KOTA PASURUAN', 1, '', 1, '2013-12-17 15:38:45', '2013-12-17 15:41:39', 1, 1),
(156, 10, 'KOTA PROBOLINGGO', 1, '', 1, '2013-12-17 15:39:00', NULL, 1, NULL),
(157, 10, 'KOTA SURABAYA', 1, '', 1, '2013-12-17 15:39:16', NULL, 1, NULL),
(158, 10, 'LAMONGAN', 0, '', 1, '2013-12-17 15:42:10', '2013-12-17 15:48:13', 1, 1),
(159, 10, 'LUMAJANG', 0, '', 1, '2013-12-17 15:42:23', NULL, 1, NULL),
(160, 10, 'MADIUN', 0, '', 1, '2013-12-17 15:42:37', NULL, 1, NULL),
(161, 10, 'MAGETAN', 0, '', 1, '2013-12-17 15:42:53', NULL, 1, NULL),
(162, 10, 'MALANG', 0, '', 1, '2013-12-17 15:43:08', NULL, 1, NULL),
(163, 10, 'MOJOKERTO', 0, '', 1, '2013-12-17 15:43:21', NULL, 1, NULL),
(164, 10, 'NGANJUK', 0, '', 1, '2013-12-17 15:43:36', NULL, 1, NULL),
(165, 10, 'NGAWI', 0, '', 1, '2013-12-17 15:43:47', NULL, 1, NULL),
(166, 10, 'PACITAN', 0, '', 1, '2013-12-17 15:44:09', NULL, 1, NULL),
(167, 10, 'PAMEKASAN', 0, '', 1, '2013-12-17 15:44:49', NULL, 1, NULL),
(168, 10, 'PASURUAN', 0, '', 1, '2013-12-17 15:45:04', NULL, 1, NULL),
(169, 10, 'PONOROGO', 0, '', 1, '2013-12-17 15:45:18', NULL, 1, NULL),
(170, 10, 'PROBOLINGGO', 0, '', 1, '2013-12-17 15:45:37', NULL, 1, NULL),
(171, 10, 'SAMPANG', 0, '', 1, '2013-12-17 15:45:51', NULL, 1, NULL),
(172, 10, 'SIDOARJO', 0, '', 1, '2013-12-17 15:46:02', NULL, 1, NULL),
(173, 10, 'SITUBONDO', 0, '', 1, '2013-12-17 15:46:16', NULL, 1, NULL),
(174, 10, 'SUMENEP', 0, '', 1, '2013-12-17 15:46:30', NULL, 1, NULL),
(175, 10, 'TRENGGALEK', 0, '', 1, '2013-12-17 15:46:46', NULL, 1, NULL),
(176, 10, 'TUBAN', 0, '', 1, '2013-12-17 15:46:59', NULL, 1, NULL),
(177, 10, 'TULUNGAGUNG', 0, '', 1, '2013-12-17 15:47:20', NULL, 1, NULL),
(178, 11, 'BENGKAYANG', 0, '', 1, '2013-12-17 15:51:59', NULL, 1, NULL),
(179, 11, 'KAPUAS HULU', 0, '', 1, '2013-12-17 15:52:18', NULL, 1, NULL),
(180, 11, 'KAYONG UTARA', 0, '', 1, '2013-12-17 15:52:44', NULL, 1, NULL),
(181, 11, 'KETAPANG', 0, '', 1, '2013-12-17 15:53:00', NULL, 1, NULL),
(182, 11, 'KOTA PONTIANAK', 1, '', 1, '2013-12-17 15:53:16', '2013-12-17 16:00:07', 1, 1),
(183, 11, 'KOTA SINGKAWANG', 1, '', 1, '2013-12-17 15:53:33', NULL, 1, NULL),
(184, 11, 'KUBU RAYA', 0, '', 1, '2013-12-17 15:53:51', NULL, 1, NULL),
(185, 11, 'LANDAK', 0, '', 1, '2013-12-17 15:54:03', NULL, 1, NULL),
(186, 11, 'MELAWI', 0, '', 1, '2013-12-17 15:54:15', NULL, 1, NULL),
(187, 11, 'PONTIANAK', 0, '', 1, '2013-12-17 15:54:29', NULL, 1, NULL),
(188, 11, 'SAMBAS', 0, '', 1, '2013-12-17 15:54:42', NULL, 1, NULL),
(189, 11, 'SANGGAU', 0, '', 1, '2013-12-17 15:54:54', NULL, 1, NULL),
(190, 11, 'SEKADAU', 0, '', 1, '2013-12-17 15:55:07', NULL, 1, NULL),
(191, 11, 'SINTANG', 0, '', 1, '2013-12-17 15:55:20', NULL, 1, NULL),
(192, 12, 'BALANGAN', 0, '', 1, '2013-12-17 16:08:25', NULL, 1, NULL),
(193, 12, 'BANJAR', 0, '', 1, '2013-12-17 16:08:38', NULL, 1, NULL),
(194, 12, 'BARITO KUALA', 0, '', 1, '2013-12-17 16:08:52', NULL, 1, NULL),
(195, 12, 'HULU SUNGAI SELATAN', 0, '', 1, '2013-12-17 16:09:07', NULL, 1, NULL),
(196, 12, 'HULU SUNGAI TENGAH', 0, '', 1, '2013-12-17 16:09:22', NULL, 1, NULL),
(197, 12, 'HULU SUNGAI UTARA', 0, '', 1, '2013-12-17 16:09:36', NULL, 1, NULL),
(198, 12, 'KOTA BANJARBARU', 1, '', 1, '2013-12-17 16:09:53', NULL, 1, NULL),
(199, 12, 'KOTA BANJARMASIN', 1, '', 1, '2013-12-17 16:10:09', NULL, 1, NULL),
(200, 12, 'KOTABARU', 1, '', 1, '2013-12-17 16:10:32', NULL, 1, NULL),
(201, 12, 'TABALONG', 0, '', 1, '2013-12-17 16:10:45', NULL, 1, NULL),
(202, 12, 'TANAH BUMBU', 0, '', 1, '2013-12-17 16:10:58', NULL, 1, NULL),
(203, 12, 'TANAH LAUT', 0, '', 1, '2013-12-17 16:11:15', NULL, 1, NULL),
(204, 12, 'TAPIN', 0, '', 1, '2013-12-17 16:11:31', NULL, 1, NULL),
(205, 13, 'BARITO SELATAN', 0, '', 1, '2013-12-17 16:17:16', NULL, 1, NULL),
(206, 13, 'BARITO TIMUR', 0, '', 1, '2013-12-17 16:17:31', NULL, 1, NULL),
(207, 13, 'BARITO UTARA', 0, '', 1, '2013-12-17 16:17:49', NULL, 1, NULL),
(208, 13, 'GUNUNG MAS', 0, '', 1, '2013-12-17 16:18:04', NULL, 1, NULL),
(209, 13, 'KAPUAS', 0, '', 1, '2013-12-17 16:18:17', NULL, 1, NULL),
(210, 13, 'KATINGAN', 0, '', 1, '2013-12-17 16:18:30', NULL, 1, NULL),
(211, 13, 'KOTA PALANGKARAYA', 1, '', 1, '2013-12-17 16:18:48', NULL, 1, NULL),
(212, 13, 'KOTAWARINGIN BARAT', 1, '', 1, '2013-12-17 16:19:07', NULL, 1, NULL),
(213, 13, 'KOTAWARINGIN TIMUR', 1, '', 1, '2013-12-17 16:19:25', NULL, 1, NULL),
(214, 13, 'LAMANDAU', 0, '', 1, '2013-12-17 16:19:40', NULL, 1, NULL),
(215, 13, 'MURUNG RAYA', 0, '', 1, '2013-12-17 16:19:54', NULL, 1, NULL),
(216, 13, 'PULANG PISAU', 0, '', 1, '2013-12-17 16:20:10', NULL, 1, NULL),
(217, 13, 'SERUYAN', 0, '', 1, '2013-12-17 16:20:24', NULL, 1, NULL),
(218, 13, 'SUKAMARA', 0, '', 1, '2013-12-17 16:20:37', NULL, 1, NULL),
(219, 14, 'BERAU', 0, '', 1, '2013-12-17 16:21:23', NULL, 1, NULL),
(220, 14, 'BULUNGAN', 0, '', 1, '2013-12-17 16:21:37', NULL, 1, NULL),
(221, 14, 'KOTA BALIKPAPAN', 1, '', 1, '2013-12-17 16:21:52', NULL, 1, NULL),
(222, 14, 'KOTA BONTANG', 1, '', 1, '2013-12-17 16:22:07', NULL, 1, NULL),
(223, 14, 'KOTA SAMARINDA', 1, '', 1, '2013-12-17 16:22:37', NULL, 1, NULL),
(224, 14, 'KOTA TARAKAN', 1, '', 1, '2013-12-17 16:22:52', NULL, 1, NULL),
(225, 14, 'KUTAI BARAT', 0, '', 1, '2013-12-17 16:23:10', NULL, 1, NULL),
(226, 14, 'KUTAI KARTANEGARA', 0, '', 1, '2013-12-17 16:23:24', NULL, 1, NULL),
(227, 14, 'KUTAI TIMUR', 0, '', 1, '2013-12-17 16:23:37', NULL, 1, NULL),
(228, 14, 'MALINAU', 0, '', 1, '2013-12-17 16:23:52', NULL, 1, NULL),
(229, 14, 'NUNUKAN', 0, '', 1, '2013-12-17 16:24:08', NULL, 1, NULL),
(230, 14, 'PASER', 0, '', 1, '2013-12-17 16:24:21', NULL, 1, NULL),
(231, 14, 'PENAJAM PASER UTARA', 0, '', 1, '2013-12-17 16:24:34', NULL, 1, NULL),
(232, 14, 'TANA TIDUNG', 0, '', 1, '2013-12-17 16:24:51', NULL, 1, NULL),
(233, 16, 'BANGKA', 0, '', 1, '2013-12-17 16:25:27', NULL, 1, NULL),
(234, 16, 'BANGKA BARAT', 0, '', 1, '2013-12-17 16:25:41', NULL, 1, NULL),
(235, 16, 'BANGKA SELATAN', 0, '', 1, '2013-12-17 16:25:54', NULL, 1, NULL),
(236, 16, 'BANGKA TENGAH', 0, '', 1, '2013-12-17 16:26:08', NULL, 1, NULL),
(237, 16, 'BELITUNG', 0, '', 1, '2013-12-17 16:26:23', NULL, 1, NULL),
(238, 16, 'BELITUNG TIMUR', 0, '', 1, '2013-12-17 16:26:36', NULL, 1, NULL),
(239, 16, 'KOTA PANGKALPINANG', 1, '', 1, '2013-12-17 16:26:53', NULL, 1, NULL),
(240, 17, 'BINTAN', 0, '', 1, '2013-12-17 16:27:39', NULL, 1, NULL),
(241, 17, 'KARIMUN', 0, '', 1, '2013-12-17 16:27:53', NULL, 1, NULL),
(242, 17, 'KEPULAUAN ANAMBAS', 0, '', 1, '2013-12-17 16:28:10', NULL, 1, NULL),
(243, 17, 'KOTA BATAM', 1, '', 1, '2013-12-17 16:28:34', NULL, 1, NULL),
(244, 17, 'KOTA TANJUNGPINANG', 1, '', 1, '2013-12-17 16:28:52', NULL, 1, NULL),
(245, 17, 'LINGGA', 0, '', 1, '2013-12-17 16:29:07', NULL, 1, NULL),
(246, 17, 'NATUNA', 0, '', 1, '2013-12-17 16:29:21', NULL, 1, NULL),
(247, 18, 'KOTA BANDAR LAMPUNG', 1, '', 1, '2013-12-17 16:30:12', NULL, 1, NULL),
(248, 18, 'KOTA METRO', 1, '', 1, '2013-12-17 16:30:27', NULL, 1, NULL),
(249, 18, 'LAMPUNG BARAT', 0, '', 1, '2013-12-17 16:30:44', NULL, 1, NULL),
(250, 18, 'LAMPUNG SELATAN', 0, '', 1, '2013-12-17 16:30:59', NULL, 1, NULL),
(251, 18, 'LAMPUNG TENGAH', 0, '', 1, '2013-12-17 16:31:13', NULL, 1, NULL),
(252, 18, 'LAMPUNG TIMUR', 0, '', 1, '2013-12-17 16:31:27', NULL, 1, NULL),
(253, 18, 'LAMPUNG UTARA', 0, '', 1, '2013-12-17 16:31:41', NULL, 1, NULL),
(254, 18, 'MESUJI', 0, '', 1, '2013-12-17 16:31:53', NULL, 1, NULL),
(255, 18, 'PESAWARAN', 0, '', 1, '2013-12-17 16:32:04', NULL, 1, NULL),
(256, 18, 'PRINGSEWU', 0, '', 1, '2013-12-17 16:32:19', NULL, 1, NULL),
(257, 18, 'TANGGAMUS', 0, '', 1, '2013-12-17 16:32:33', NULL, 1, NULL),
(258, 18, 'TULANG BAWANG', 0, '', 1, '2013-12-17 16:32:47', NULL, 1, NULL),
(259, 18, 'TULANG BAWANG BARAT', 0, '', 1, '2013-12-17 16:33:00', NULL, 1, NULL),
(260, 18, 'WAY KANAN', 0, '', 1, '2013-12-17 16:33:14', NULL, 1, NULL),
(261, 19, 'BURU', 0, '', 1, '2013-12-17 16:34:17', NULL, 1, NULL),
(262, 19, 'BURU SELATAN', 0, '', 1, '2013-12-17 16:34:32', NULL, 1, NULL),
(263, 19, 'KEPULAUAN ARU', 0, '', 1, '2013-12-17 16:34:45', NULL, 1, NULL),
(264, 19, 'KOTA AMBON', 1, '', 1, '2013-12-17 16:34:56', '2013-12-17 16:35:06', 1, 1),
(265, 19, 'KOTA TUAL', 1, '', 1, '2013-12-17 16:35:21', NULL, 1, NULL),
(266, 19, 'MALUKU BARAT DAYA', 0, '', 1, '2013-12-17 16:35:36', NULL, 1, NULL),
(267, 19, 'MALUKU TENGAH', 0, '', 1, '2013-12-17 16:35:49', NULL, 1, NULL),
(268, 19, 'MALUKU TENGGARA', 0, '', 1, '2013-12-17 16:36:01', NULL, 1, NULL),
(269, 19, 'MALUKU TENGGARA BARAT', 0, '', 1, '2013-12-17 16:36:15', NULL, 1, NULL),
(270, 19, 'SERAM BAGIAN BARAT', 0, '', 1, '2013-12-17 16:36:29', NULL, 1, NULL),
(271, 19, 'SERAM BAGIAN TIMUR', 0, '', 1, '2013-12-17 16:36:44', NULL, 1, NULL),
(272, 20, 'HALMAHERA BARAT', 0, '', 1, '2013-12-17 16:39:04', NULL, 1, NULL),
(273, 20, 'HALMAHERA SELATAN', 0, '', 1, '2013-12-17 16:39:17', NULL, 1, NULL),
(274, 20, 'HALMAHERA TENGAH', 0, '', 1, '2013-12-17 16:39:31', NULL, 1, NULL),
(275, 20, 'HALMAHERA TIMUR', 0, '', 1, '2013-12-17 16:40:02', NULL, 1, NULL),
(276, 20, 'HALMAHERA UTARA', 0, '', 1, '2013-12-17 16:40:22', NULL, 1, NULL),
(277, 20, 'KEPULAUAN SULA', 0, '', 1, '2013-12-17 16:40:36', NULL, 1, NULL),
(278, 20, 'KOTA TERNATE', 1, '', 1, '2013-12-17 16:40:50', NULL, 1, NULL),
(279, 20, 'KOTA TIDORE KEPULAUAN', 1, '', 1, '2013-12-17 16:41:17', NULL, 1, NULL),
(280, 20, 'PULAU MOROTAI', 0, '', 1, '2013-12-17 16:41:28', NULL, 1, NULL),
(281, 21, 'BIMA', 0, '', 1, '2013-12-17 17:04:14', NULL, 1, NULL),
(282, 21, 'DOMPU', 0, '', 1, '2013-12-17 17:04:29', NULL, 1, NULL),
(283, 21, 'KOTA BIMA', 1, '', 1, '2013-12-17 17:04:49', NULL, 1, NULL),
(284, 21, 'KOTA MATARAM', 1, '', 1, '2013-12-17 17:05:07', NULL, 1, NULL),
(285, 21, 'LOMBOK BARAT', 0, '', 1, '2013-12-17 17:05:36', NULL, 1, NULL),
(286, 21, 'LOMBOK TENGAH', 0, '', 1, '2013-12-17 17:05:52', NULL, 1, NULL),
(287, 21, 'LOMBOK TIMUR', 0, '', 1, '2013-12-17 17:06:08', NULL, 1, NULL),
(288, 21, 'LOMBOK UTARA', 0, '', 1, '2013-12-17 17:06:20', NULL, 1, NULL),
(289, 21, 'SUMBAWA', 0, '', 1, '2013-12-17 17:06:33', NULL, 1, NULL),
(290, 21, 'SUMBAWA BARAT', 0, '', 1, '2013-12-17 17:06:46', NULL, 1, NULL),
(291, 22, 'ALOR', 0, '', 1, '2013-12-17 17:08:19', NULL, 1, NULL),
(292, 22, 'BELU', 0, '', 1, '2013-12-17 17:08:31', NULL, 1, NULL),
(293, 22, 'ENDE', 0, '', 1, '2013-12-17 17:08:42', NULL, 1, NULL),
(294, 22, 'FLORES TIMUR', 0, '', 1, '2013-12-17 17:08:57', NULL, 1, NULL),
(295, 22, 'KOTA KUPANG', 1, '', 1, '2013-12-17 17:09:12', '2013-12-17 17:09:20', 1, 1),
(296, 22, 'KUPANG', 0, '', 1, '2013-12-17 17:09:34', NULL, 1, NULL),
(297, 22, 'LEMBATA', 0, '', 1, '2013-12-17 17:10:31', NULL, 1, NULL),
(298, 22, 'MANGGARAI', 0, '', 1, '2013-12-17 17:10:46', NULL, 1, NULL),
(299, 22, 'MANGGARAI BARAT', 0, '', 1, '2013-12-17 17:11:03', NULL, 1, NULL),
(300, 22, 'MANGGARAI TIMUR', 0, '', 1, '2013-12-17 17:12:19', NULL, 1, NULL),
(301, 22, 'NAGEKEO', 0, '', 1, '2013-12-17 17:12:42', NULL, 1, NULL),
(302, 22, 'NGADA', 0, '', 1, '2013-12-17 17:12:53', NULL, 1, NULL),
(303, 22, 'ROTE NDAO', 0, '', 1, '2013-12-17 17:13:10', NULL, 1, NULL),
(304, 22, 'SABU RAIJUA', 0, '', 1, '2013-12-17 17:13:25', NULL, 1, NULL),
(305, 22, 'SIKKA', 0, '', 1, '2013-12-17 17:13:46', NULL, 1, NULL),
(306, 22, 'SUMBA BARAT', 0, '', 1, '2013-12-17 17:14:01', NULL, 1, NULL),
(307, 22, 'SUMBA BARAT DAYA', 0, '', 1, '2013-12-17 17:14:15', NULL, 1, NULL),
(308, 22, 'SUMBA TENGAH', 0, '', 1, '2013-12-17 17:14:30', NULL, 1, NULL),
(309, 22, 'SUMBA TIMUR', 0, '', 1, '2013-12-17 17:14:43', NULL, 1, NULL),
(310, 22, 'TIMOR TENGAH SELATAN', 0, '', 1, '2013-12-17 17:14:56', NULL, 1, NULL),
(311, 22, 'TIMOR TENGAH UTARA', 0, '', 1, '2013-12-17 17:15:08', NULL, 1, NULL),
(312, 23, 'ASMAT', 0, '', 1, '2013-12-17 17:16:43', NULL, 1, NULL),
(313, 23, 'BIAK NUMFOR', 0, '', 1, '2013-12-17 17:16:55', NULL, 1, NULL),
(314, 23, 'BOVEN DIGOEL', 0, '', 1, '2013-12-17 17:17:10', NULL, 1, NULL),
(315, 23, 'DEIYAI', 0, '', 1, '2013-12-17 17:17:20', NULL, 1, NULL),
(316, 23, 'DOGIYAI', 0, '', 1, '2013-12-17 17:17:32', NULL, 1, NULL),
(317, 23, 'INTAN JAYA', 0, '', 1, '2013-12-17 17:17:46', NULL, 1, NULL),
(318, 23, 'JAYAPURA', 0, '', 1, '2013-12-17 17:18:04', NULL, 1, NULL),
(319, 23, 'JAYAWIJAYA', 0, '', 1, '2013-12-17 17:18:15', NULL, 1, NULL),
(320, 23, 'KEEROM', 0, '', 1, '2013-12-17 17:18:25', NULL, 1, NULL),
(321, 23, 'KEPULAUAN YAPEN', 0, '', 1, '2013-12-17 17:18:36', NULL, 1, NULL),
(322, 23, 'KOTA JAYAPURA', 1, '', 1, '2013-12-17 17:19:02', '2013-12-17 17:19:18', 1, 1),
(323, 23, 'LANNY JAYA', 0, '', 1, '2013-12-17 17:19:33', NULL, 1, NULL),
(324, 23, 'MAMBERAMO RAYA', 0, '', 1, '2013-12-17 17:19:45', NULL, 1, NULL),
(325, 23, 'MAMBERAMO TENGAH', 0, '', 1, '2013-12-17 17:19:57', NULL, 1, NULL),
(326, 23, 'MAPPI', 0, '', 1, '2013-12-17 17:20:07', NULL, 1, NULL),
(327, 23, 'MERAUKE', 0, '', 1, '2013-12-17 17:20:19', NULL, 1, NULL),
(328, 23, 'MIMIKA', 0, '', 1, '2013-12-17 17:20:32', NULL, 1, NULL),
(329, 23, 'NABIRE', 0, '', 1, '2013-12-17 17:20:49', NULL, 1, NULL),
(330, 23, 'NDUGA', 0, '', 1, '2013-12-17 17:20:59', NULL, 1, NULL),
(331, 23, 'PANIAI', 0, '', 1, '2013-12-17 17:21:12', NULL, 1, NULL),
(332, 23, 'PEGUNUNGAN BINTANG', 0, '', 1, '2013-12-17 17:21:23', NULL, 1, NULL),
(333, 23, 'PUNCAK', 0, '', 1, '2013-12-17 17:21:34', NULL, 1, NULL),
(334, 23, 'PUNCAK JAYA', 0, '', 1, '2013-12-17 17:21:45', NULL, 1, NULL),
(335, 23, 'SARMI', 0, '', 1, '2013-12-17 17:21:55', NULL, 1, NULL),
(336, 23, 'SUPIORI', 0, '', 1, '2013-12-17 17:22:06', NULL, 1, NULL),
(337, 23, 'TOLIKARA', 0, '', 1, '2013-12-17 17:22:17', NULL, 1, NULL),
(338, 23, 'WAROPEN', 0, '', 1, '2013-12-17 17:22:27', NULL, 1, NULL),
(339, 23, 'YAHUKIMO', 0, '', 1, '2013-12-17 17:22:42', NULL, 1, NULL),
(340, 23, 'YALIMO', 0, '', 1, '2013-12-17 17:22:51', NULL, 1, NULL),
(341, 24, 'FAKFAK', 0, '', 1, '2013-12-17 17:23:56', NULL, 1, NULL),
(342, 24, 'KAIMANA', 0, '', 1, '2013-12-17 17:24:12', NULL, 1, NULL),
(343, 24, 'KOTA SORONG', 1, '', 1, '2013-12-17 17:24:26', NULL, 1, NULL),
(344, 24, 'MANOKWARI', 0, '', 1, '2013-12-17 17:24:37', NULL, 1, NULL),
(345, 24, 'MAYBRAT', 0, '', 1, '2013-12-17 17:24:47', NULL, 1, NULL),
(346, 24, 'RAJA AMPAT', 0, '', 1, '2013-12-17 17:24:59', NULL, 1, NULL),
(347, 24, 'SORONG', 0, '', 1, '2013-12-17 17:25:10', NULL, 1, NULL),
(348, 24, 'SORONG SELATAN', 0, '', 1, '2013-12-17 17:25:24', NULL, 1, NULL),
(349, 24, 'TAMBRAUW', 0, '', 1, '2013-12-17 17:25:35', NULL, 1, NULL),
(350, 24, 'TELUK BINTUNI', 0, '', 1, '2013-12-17 17:25:46', NULL, 1, NULL),
(351, 24, 'TELUK WONDAMA', 0, '', 1, '2013-12-17 17:25:58', NULL, 1, NULL),
(352, 25, 'BENGKALIS', 0, '', 1, '2013-12-17 17:26:42', NULL, 1, NULL),
(353, 25, 'INDRAGIRI HILIR', 0, '', 1, '2013-12-17 17:26:54', NULL, 1, NULL),
(354, 25, 'INDRAGIRI HULU', 0, '', 1, '2013-12-17 17:27:06', NULL, 1, NULL),
(355, 25, 'KAMPAR', 0, '', 1, '2013-12-17 17:27:16', NULL, 1, NULL),
(356, 25, 'KEPULAUAN MERANTI', 0, '', 1, '2013-12-17 17:27:28', NULL, 1, NULL),
(357, 25, 'KOTA DUMAI', 1, '', 1, '2013-12-17 17:27:39', '2013-12-17 17:28:37', 1, 1),
(358, 25, 'KOTA PEKANBARU', 1, '', 1, '2013-12-17 17:27:51', '2013-12-17 17:28:24', 1, 1),
(359, 25, 'KUANTAN SINGINGI', 0, '', 1, '2013-12-17 17:28:09', NULL, 1, NULL),
(360, 25, 'PELALAWAN', 0, '', 1, '2013-12-17 17:29:09', NULL, 1, NULL),
(361, 25, 'ROKAN HILIR', 0, '', 1, '2013-12-17 17:29:22', NULL, 1, NULL),
(362, 25, 'ROKAN HULU', 0, '', 1, '2013-12-17 17:29:34', NULL, 1, NULL),
(363, 25, 'SIAK', 0, '', 1, '2013-12-17 17:29:45', NULL, 1, NULL),
(364, 26, 'MAJENE', 0, '', 1, '2013-12-17 17:30:38', NULL, 1, NULL),
(365, 26, 'MAMASA', 0, '', 1, '2013-12-17 17:30:51', NULL, 1, NULL),
(366, 26, 'MAMUJU', 0, '', 1, '2013-12-17 17:31:04', NULL, 1, NULL),
(367, 26, 'MAMUJU UTARA', 0, '', 1, '2013-12-17 17:31:17', NULL, 1, NULL),
(368, 26, 'POLEWALI MANDAR', 0, '', 1, '2013-12-17 17:31:31', NULL, 1, NULL),
(369, 27, 'BANTAENG', 0, '', 1, '2013-12-17 17:31:52', NULL, 1, NULL),
(370, 27, 'BARRU', 0, '', 1, '2013-12-17 17:32:03', NULL, 1, NULL),
(371, 27, 'BONE', 0, '', 1, '2013-12-17 17:32:15', NULL, 1, NULL),
(372, 27, 'BULUKUMBA', 0, '', 1, '2013-12-17 17:32:24', NULL, 1, NULL),
(373, 27, 'ENREKANG', 0, '', 1, '2013-12-17 17:32:35', NULL, 1, NULL),
(374, 27, 'GOWA', 0, '', 1, '2013-12-17 17:32:45', NULL, 1, NULL),
(375, 27, 'JENEPONTO', 0, '', 1, '2013-12-17 17:32:59', NULL, 1, NULL),
(376, 27, 'KEPULAUAN SELAYAR', 0, '', 1, '2013-12-17 17:33:12', NULL, 1, NULL),
(377, 27, 'KOTA MAKASSAR', 1, '', 1, '2013-12-17 17:33:28', NULL, 1, NULL),
(378, 27, 'KOTA PALOPO', 1, '', 1, '2013-12-17 17:33:49', NULL, 1, NULL),
(379, 27, 'KOTA PARE PARE', 1, '', 1, '2013-12-17 17:34:03', NULL, 1, NULL),
(380, 27, 'LUWU', 0, '', 1, '2013-12-17 17:34:16', NULL, 1, NULL),
(381, 27, 'LUWU TIMUR', 0, '', 1, '2013-12-17 17:34:28', NULL, 1, NULL),
(382, 27, 'LUWU UTARA', 0, '', 1, '2013-12-17 17:34:40', NULL, 1, NULL),
(383, 27, 'MAROS', 0, '', 1, '2013-12-17 17:34:49', NULL, 1, NULL),
(384, 27, 'PANGKAJENE DAN KEPULAUAN', 0, '', 1, '2013-12-17 17:35:02', NULL, 1, NULL),
(385, 27, 'PINRANG', 0, '', 1, '2013-12-17 17:35:12', NULL, 1, NULL),
(386, 27, 'SIDENRENG RAPPANG', 0, '', 1, '2013-12-17 17:35:24', NULL, 1, NULL),
(387, 27, 'SINJAI', 0, '', 1, '2013-12-17 17:35:38', NULL, 1, NULL),
(388, 27, 'SOPPENG', 0, '', 1, '2013-12-17 17:35:50', NULL, 1, NULL),
(389, 27, 'TAKALAR', 0, '', 1, '2013-12-17 17:36:00', NULL, 1, NULL),
(390, 27, 'TANA TORAJA', 0, '', 1, '2013-12-17 17:36:12', NULL, 1, NULL),
(391, 27, 'TORAJA UTARA', 0, '', 1, '2013-12-17 17:36:24', NULL, 1, NULL),
(392, 27, 'WAJO', 0, '', 1, '2013-12-17 17:36:36', NULL, 1, NULL),
(393, 28, 'BANGGAI', 0, '', 1, '2013-12-17 17:37:33', NULL, 1, NULL),
(394, 28, 'BANGGAI KEPULAUAN', 0, '', 1, '2013-12-17 17:37:45', NULL, 1, NULL),
(395, 28, 'BUOL', 0, '', 1, '2013-12-17 17:38:02', NULL, 1, NULL),
(396, 28, 'DONGGALA', 0, '', 1, '2013-12-17 17:38:15', NULL, 1, NULL),
(397, 28, 'KOTA PALU', 0, '', 1, '2013-12-17 17:38:29', NULL, 1, NULL),
(398, 28, 'MOROWALI', 0, '', 1, '2013-12-17 17:38:41', NULL, 1, NULL),
(399, 28, 'PARIGI MOUTONG', 0, '', 1, '2013-12-17 17:38:54', NULL, 1, NULL),
(400, 28, 'POSO', 0, '', 1, '2013-12-17 17:39:10', NULL, 1, NULL),
(401, 28, 'SIGI', 0, '', 1, '2013-12-17 17:39:22', NULL, 1, NULL),
(402, 28, 'TOJO UNA-UNA', 0, '', 1, '2013-12-17 17:39:34', NULL, 1, NULL),
(403, 28, 'TOLITOLI', 0, '', 1, '2013-12-17 17:39:45', NULL, 1, NULL),
(404, 29, 'BOMBANA', 0, '', 1, '2013-12-17 17:40:47', NULL, 1, NULL),
(405, 29, 'BUTON', 0, '', 1, '2013-12-17 17:41:00', NULL, 1, NULL),
(406, 29, 'BUTON UTARA', 0, '', 1, '2013-12-17 17:41:12', NULL, 1, NULL),
(407, 29, 'KOLAKA', 0, '', 1, '2013-12-17 17:42:00', NULL, 1, NULL),
(408, 29, 'KOLAKA UTARA', 0, '', 1, '2013-12-17 17:42:32', NULL, 1, NULL),
(409, 29, 'KONAWE', 0, '', 1, '2013-12-17 17:42:46', NULL, 1, NULL),
(410, 29, 'KONAWE SELATAN', 0, '', 1, '2013-12-17 17:43:00', NULL, 1, NULL),
(411, 29, 'KONAWE UTARA', 0, '', 1, '2013-12-17 17:43:13', NULL, 1, NULL),
(412, 29, 'KOTA BAU BAU', 1, '', 1, '2013-12-17 17:43:28', '2013-12-17 17:44:26', 1, 1),
(413, 29, 'KOTA KENDARI', 1, '', 1, '2013-12-17 17:43:40', '2013-12-17 17:44:57', 1, 1),
(414, 29, 'MUNA', 0, '', 1, '2013-12-17 17:43:51', NULL, 1, NULL),
(415, 29, 'WAKATOBI', 0, '', 1, '2013-12-17 17:44:06', NULL, 1, NULL),
(416, 30, 'BOLAANG MONGONDOW', 0, '', 1, '2013-12-17 17:47:55', NULL, 1, NULL),
(417, 30, 'BOLAANG MONGONDOW SELATAN', 0, '', 1, '2013-12-17 17:48:18', NULL, 1, NULL),
(418, 30, 'BOLAANG MONGONDOW TIMUR', 0, '', 1, '2013-12-17 17:49:01', NULL, 1, NULL),
(419, 30, 'BOLAANG MONGONDOW UTARA', 0, '', 1, '2013-12-17 17:49:31', NULL, 1, NULL),
(420, 30, 'KEP. SIAU TAGULANDANG BIARO', 0, '', 1, '2013-12-17 17:49:50', NULL, 1, NULL),
(421, 30, 'KEPULAUAN SANGIHE', 0, '', 1, '2013-12-17 17:50:03', NULL, 1, NULL),
(422, 30, 'KEPULAUAN TALAUD', 0, '', 1, '2013-12-17 17:50:19', NULL, 1, NULL),
(423, 30, 'KOTA BITUNG', 1, '', 1, '2013-12-17 17:50:31', '2013-12-17 17:52:31', 1, 1),
(424, 30, 'KOTA KOTAMOBAGU', 1, '', 1, '2013-12-17 17:50:54', '2013-12-17 17:51:03', 1, 1),
(425, 30, 'KOTA MANADO', 1, '', 1, '2013-12-17 17:52:53', NULL, 1, NULL),
(426, 30, 'KOTA TOMOHON', 1, '', 1, '2013-12-17 17:53:07', NULL, 1, NULL),
(427, 30, 'MINAHASA', 0, '', 1, '2013-12-17 17:53:18', NULL, 1, NULL),
(428, 30, 'MINAHASA SELATAN', 0, '', 1, '2013-12-17 17:53:35', NULL, 1, NULL),
(429, 30, 'MINAHASA TENGGARA', 0, '', 1, '2013-12-17 17:53:50', NULL, 1, NULL),
(430, 30, 'MINAHASA UTARA', 0, '', 1, '2013-12-17 17:54:10', NULL, 1, NULL),
(431, 31, 'AGAM', 0, '', 1, '2013-12-17 17:55:07', NULL, 1, NULL),
(432, 31, 'DHARMASRAYA', 0, '', 1, '2013-12-17 17:55:19', NULL, 1, NULL),
(433, 31, 'KEPULAUAN MENTAWAI', 0, '', 1, '2013-12-17 17:55:32', NULL, 1, NULL),
(434, 31, 'KOTA BUKITTINGGI', 1, '', 1, '2013-12-17 17:55:48', NULL, 1, NULL),
(435, 31, 'KOTA PADANG', 0, '', 1, '2013-12-17 17:56:01', NULL, 1, NULL),
(436, 31, 'KOTA PADANG PANJANG', 1, '', 1, '2013-12-17 17:56:16', NULL, 1, NULL),
(437, 31, 'KOTA PARIAMAN', 1, '', 1, '2013-12-17 17:56:30', NULL, 1, NULL),
(438, 31, 'KOTA PAYAKUMBUH', 1, '', 1, '2013-12-17 17:56:47', NULL, 1, NULL),
(439, 31, 'KOTA SAWAHLUNTO', 1, '', 1, '2013-12-17 17:57:08', NULL, 1, NULL),
(440, 31, 'KOTA SOLOK', 1, '', 1, '2013-12-17 17:57:24', NULL, 1, NULL),
(441, 31, 'LIMA PULUH KOTA', 0, '', 1, '2013-12-17 17:57:48', NULL, 1, NULL),
(442, 31, 'PADANG PARIAMAN', 0, '', 1, '2013-12-17 17:58:18', NULL, 1, NULL),
(443, 31, 'PASAMAN', 0, '', 1, '2013-12-17 18:00:15', NULL, 1, NULL),
(444, 31, 'PASAMAN BARAT', 0, '', 1, '2013-12-17 18:01:57', NULL, 1, NULL),
(445, 31, 'PESISIR SELATAN', 0, '', 1, '2013-12-17 18:02:22', NULL, 1, NULL),
(446, 31, 'SIJUNJUNG', 0, '', 1, '2013-12-17 18:02:47', NULL, 1, NULL),
(447, 31, 'SOLOK', 0, '', 1, '2013-12-17 18:03:02', NULL, 1, NULL),
(448, 31, 'SOLOK SELATAN', 0, '', 1, '2013-12-17 18:03:17', NULL, 1, NULL),
(449, 31, 'TANAH DATAR', 0, '', 1, '2013-12-17 18:03:35', NULL, 1, NULL),
(451, 32, 'BANYUASIN', 0, '', 1, '2013-12-17 18:16:45', NULL, 1, NULL),
(452, 32, 'EMPAT LAWANG', 0, '', 1, '2013-12-17 18:18:54', NULL, 1, NULL),
(453, 32, 'KOTA LUBUKLINGGAU', 1, '', 1, '2013-12-17 18:21:49', NULL, 1, NULL),
(454, 32, 'KOTA PAGAR ALAM', 1, '', 1, '2013-12-17 18:22:03', NULL, 1, NULL),
(455, 32, 'KOTA PALEMBANG', 1, '', 1, '2013-12-17 18:22:16', '2013-12-17 18:22:25', 1, 1),
(456, 32, 'KOTA PRABUMULIH', 1, '', 1, '2013-12-17 18:22:40', NULL, 1, NULL),
(457, 32, 'LAHAT', 0, '', 1, '2013-12-17 18:22:54', NULL, 1, NULL),
(458, 32, 'MUARA ENIM', 0, '', 1, '2013-12-17 18:23:06', NULL, 1, NULL),
(459, 32, 'MUSI BANYUASIN', 0, '', 1, '2013-12-17 18:23:19', NULL, 1, NULL),
(460, 32, 'MUSI RAWAS', 0, '', 1, '2013-12-17 18:23:31', NULL, 1, NULL),
(461, 32, 'OGAN ILIR', 0, '', 1, '2013-12-17 18:23:42', NULL, 1, NULL),
(462, 32, 'OGAN KOMERING ILIR', 0, '', 1, '2013-12-17 18:23:54', NULL, 1, NULL),
(463, 32, 'OGAN KOMERING ULU', 0, '', 1, '2013-12-17 18:24:06', NULL, 1, NULL),
(464, 32, 'OGAN KOMERING ULU SELATAN', 0, '', 1, '2013-12-17 18:24:18', NULL, 1, NULL),
(465, 32, 'OGAN KOMERING ULU TIMUR', 0, '', 1, '2013-12-17 18:24:29', NULL, 1, NULL),
(466, 33, 'ASAHAN', 0, '', 1, '2013-12-17 18:25:34', NULL, 1, NULL),
(467, 33, 'BATU BARA', 0, '', 1, '2013-12-17 18:25:48', NULL, 1, NULL),
(468, 33, 'DAIRI', 0, '', 1, '2013-12-17 18:25:58', NULL, 1, NULL),
(469, 33, 'DELI SERDANG', 0, '', 1, '2013-12-17 18:26:11', '2013-12-17 18:29:40', 1, 1),
(470, 33, 'HUMBANG HASUNDUTAN', 0, '', 1, '2013-12-17 18:26:24', NULL, 1, NULL),
(471, 33, 'KARO', 0, '', 1, '2013-12-17 18:26:34', NULL, 1, NULL),
(472, 33, 'KOTA BINJAI', 1, '', 1, '2013-12-17 18:26:49', NULL, 1, NULL),
(473, 33, 'KOTA GUNUNGSITOLI', 1, '', 1, '2013-12-17 18:27:03', NULL, 1, NULL),
(474, 33, 'KOTA MEDAN', 1, '', 1, '2013-12-17 18:27:14', '2013-12-17 18:27:22', 1, 1),
(475, 33, 'KOTA PADANG SIDIMPUAN', 1, '', 1, '2013-12-17 18:31:45', NULL, 1, NULL),
(476, 33, 'KOTA PEMATANGSIANTAR', 1, '', 1, '2013-12-17 18:31:59', NULL, 1, NULL),
(477, 33, 'KOTA SIBOLGA', 1, '', 1, '2013-12-17 18:32:11', NULL, 1, NULL),
(478, 33, 'KOTA TANJUNG BALAI', 1, '', 1, '2013-12-17 18:32:25', NULL, 1, NULL),
(479, 33, 'KOTA TEBING TINGGI', 1, '', 1, '2013-12-17 18:32:46', NULL, 1, NULL),
(480, 33, 'LABUHANBATU', 0, '', 1, '2013-12-17 18:32:59', NULL, 1, NULL),
(481, 33, 'LABUHANBATU SELATAN', 0, '', 1, '2013-12-17 18:33:12', NULL, 1, NULL),
(482, 33, 'LABUHANBATU UTARA', 0, '', 1, '2013-12-17 18:33:28', NULL, 1, NULL),
(483, 33, 'LANGKAT', 0, '', 1, '2013-12-17 18:33:37', NULL, 1, NULL),
(484, 33, 'MANDAILING NATAL', 0, '', 1, '2013-12-17 18:33:52', NULL, 1, NULL),
(485, 33, 'NIAS', 0, '', 1, '2013-12-17 18:34:02', NULL, 1, NULL),
(486, 33, 'NIAS BARAT', 0, '', 1, '2013-12-17 18:34:13', NULL, 1, NULL),
(487, 33, 'NIAS SELATAN', 0, '', 1, '2013-12-17 18:34:27', NULL, 1, NULL),
(488, 33, 'NIAS UTARA', 0, '', 1, '2013-12-17 18:34:38', NULL, 1, NULL),
(489, 33, 'PADANG LAWAS', 0, '', 1, '2013-12-17 18:34:54', NULL, 1, NULL),
(490, 33, 'PADANG LAWAS UTARA', 0, '', 1, '2013-12-17 18:35:08', NULL, 1, NULL),
(491, 33, 'PAKPAK BHARAT', 0, '', 1, '2013-12-17 18:35:23', NULL, 1, NULL),
(492, 33, 'SAMOSIR', 0, '', 1, '2013-12-17 18:35:35', NULL, 1, NULL),
(493, 33, 'SERDANG BEDAGAI', 0, '', 1, '2013-12-17 18:35:52', NULL, 1, NULL),
(494, 33, 'SIMALUNGUN', 0, '', 1, '2013-12-17 18:36:08', NULL, 1, NULL),
(495, 33, 'TAPANULI SELATAN', 0, '', 1, '2013-12-17 18:36:21', NULL, 1, NULL),
(496, 33, 'TAPANULI TENGAH', 0, '', 1, '2013-12-17 18:36:35', NULL, 1, NULL),
(497, 33, 'TAPANULI UTARA', 0, '', 1, '2013-12-17 18:36:47', NULL, 1, NULL),
(498, 33, 'TOBA SAMOSIR', 0, '', 1, '2013-12-17 18:37:04', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `provinsi`
--

CREATE TABLE `provinsi` (
  `id` int(11) NOT NULL,
  `pulau_id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` text,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `provinsi`
--

INSERT INTO `provinsi` (`id`, `pulau_id`, `nama`, `keterangan`, `is_active`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 6, 'ACEH', '', 1, NULL, '2015-11-14 16:36:31', NULL, 1),
(2, 7, 'BALI', '', 1, NULL, '2015-11-14 16:37:05', NULL, 1),
(3, 1, 'BANTEN', '', 1, NULL, '2015-11-14 16:37:18', NULL, 1),
(4, 6, 'BENGKULU', '', 1, NULL, '2015-11-14 16:37:33', NULL, 1),
(5, 3, 'GORONTALO', '', 1, NULL, '2015-11-14 16:38:33', NULL, 1),
(6, 1, 'DKI JAKARTA', '', 1, NULL, '2013-12-17 21:08:00', NULL, 1),
(7, 6, 'JAMBI', '', 1, NULL, '2015-11-14 16:38:49', NULL, 1),
(8, 1, 'JAWA BARAT', '', 1, NULL, '2013-12-17 21:08:00', NULL, 1),
(9, 1, 'JAWA TENGAH', '', 1, NULL, '2013-12-17 21:08:00', NULL, 1),
(10, 1, 'JAWA TIMUR', '', 1, NULL, '2013-12-17 21:08:00', NULL, 1),
(11, 2, 'KALIMANTAN BARAT', '', 1, NULL, '2015-11-14 16:38:21', NULL, 1),
(12, 2, 'KALIMANTAN SELATAN', '', 1, NULL, '2015-11-14 16:39:03', NULL, 1),
(13, 2, 'KALIMANTAN TENGAH', '', 1, NULL, '2015-11-14 16:39:14', NULL, 1),
(14, 2, 'KALIMANTAN TIMUR', '', 1, NULL, '2015-11-14 16:39:27', NULL, 1),
(16, 9, 'KEPULAUAN BANGKA BELITUNG', '', 1, NULL, '2015-11-14 16:40:14', NULL, 1),
(17, 10, 'KEPULAUAN RIAU', '', 1, NULL, '2015-11-18 14:37:20', NULL, 1),
(18, 6, 'LAMPUNG', '', 1, NULL, '2015-11-14 16:39:45', NULL, 1),
(19, 8, 'MALUKU', '', 1, NULL, '2015-11-14 16:37:58', NULL, 1),
(20, 8, 'MALUKU UTARA', '', 1, NULL, '2015-11-14 16:41:11', NULL, 1),
(21, 5, 'NUSA TENGGARA BARAT', '', 1, NULL, '2015-11-14 16:41:23', NULL, 1),
(22, 5, 'NUSA TENGGARA TIMUR', '', 1, NULL, '2015-11-14 16:41:33', NULL, 1),
(23, 4, 'PAPUA', '', 1, NULL, '2015-11-14 16:42:52', NULL, 1),
(24, 4, 'PAPUA BARAT', '', 1, NULL, '2015-11-14 16:43:04', NULL, 1),
(25, 10, 'RIAU', '', 1, NULL, '2015-11-14 16:43:18', NULL, 1),
(26, 3, 'SULAWESI BARAT', '', 1, NULL, '2015-11-14 16:38:09', NULL, 1),
(27, 3, 'SULAWESI SELATAN', '', 1, NULL, '2015-11-14 16:43:28', NULL, 1),
(28, 3, 'SULAWESI TENGAH', '', 1, NULL, '2015-11-14 16:43:38', NULL, 1),
(29, 3, 'SULAWESI TENGGARA', '', 1, NULL, '2015-11-14 16:43:49', NULL, 1),
(30, 3, 'SULAWESI UTARA', '', 1, NULL, '2015-11-14 16:43:58', NULL, 1),
(31, 6, 'SUMATERA BARAT', '', 1, NULL, '2015-11-14 16:44:12', NULL, 1),
(32, 6, 'SUMATERA SELATAN', '', 1, NULL, '2015-11-14 16:44:21', NULL, 1),
(33, 6, 'SUMATERA UTARA', '', 1, NULL, '2015-11-14 16:44:31', NULL, 1),
(34, 1, 'DI YOGYAKARTA', '', 1, NULL, '2013-12-17 21:08:01', NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pulau`
--

CREATE TABLE `pulau` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `keterangan` text,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `pulau`
--

INSERT INTO `pulau` (`id`, `nama`, `keterangan`, `is_active`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'JAWA', '', 1, '2015-11-14 16:25:06', '2016-05-12 13:13:43', 1, 1),
(2, 'KALIMANTAN', '', 1, '2015-11-14 16:25:20', '2015-11-14 16:33:18', 1, 1),
(3, 'SULAWESI', '', 1, '2015-11-14 16:25:29', '2015-11-14 16:33:27', 1, 1),
(4, 'PAPUA', '', 1, '2015-11-14 16:25:41', '2015-11-14 16:33:35', 1, 1),
(5, 'NUSATENGGARA', '', 1, '2015-11-14 16:26:03', '2015-11-14 16:33:45', 1, 1),
(6, 'SUMATERA', '', 1, '2015-11-14 16:26:16', '2015-11-14 16:33:53', 1, 1),
(7, 'BALI', '', 1, '2015-11-14 16:26:26', '2015-11-14 16:34:02', 1, 1),
(8, 'MALUKU', '', 1, '2015-11-14 16:26:33', '2015-11-14 16:34:10', 1, 1),
(9, 'KEPULAUAN BANGKA BELITUNG', '', 1, '2015-11-14 16:40:00', '2015-11-14 16:40:57', 1, 1),
(10, 'KEPULAUAN RIAU', '', 1, '2015-11-14 16:40:37', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `layer_1` text,
  `layer_2` text,
  `publish` int(11) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `slider`
--

INSERT INTO `slider` (`id`, `image`, `layer_1`, `layer_2`, `publish`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, '1463163441_7.jpg', '<div class="travelite_slider_text" style="font-size:36px; line-height:45px;">Mari Jelajahi</div>\r\n\r\n<div class="travelite_slider_text" style="font-size:40px; line-height:45px;">Jengkal Demi Jengkal</div>\r\n\r\n<div class="travelite_slider_text" style="font-size:30px; line-height:45px;">Tanah Air Kita</div>\r\n\r\n<div class="travelite_slider_text bottom_line_text" style="font-size:48px; line-height:45px;">Tercinta</div>\r\n', '<div class="tour_packages" style="font-size:26px;">Rencanakan Liburan Anda Bersama Kami</div>\r\n', 1, '2016-05-03 15:15:17', '2016-05-13 18:39:38', 1, 1),
(2, '1463166599_8.jpg', '<div class="travelite_slider_text" style="font-size:36px; line-height:45px;">Event</div>\r\n\r\n<div class="travelite_slider_text" style="font-size:40px; line-height:45px;">Dieng Culture</div>\r\n\r\n<div class="travelite_slider_text" style="font-size:30px; line-height:45px;">Festival 216</div>\r\n\r\n<div class="travelite_slider_text bottom_line_text" style="font-size:48px; line-height:45px;">5-7 Agustus 2016</div>\r\n', '<div class="tour_packages" style="font-size:26px;">Paket Mulai <span style="font-size:38px">Rp 950.000/Pax</span></div>\r\n', 1, '2016-05-03 17:32:30', '2016-05-21 10:03:07', 1, 1),
(3, '1463161025_6.jpg', '<div class="travelite_slider_text" style="font-size:36px; line-height:45px;">Paralayang Yang</div>\r\n\r\n<div class="travelite_slider_text" style="font-size:40px; line-height:45px;">Memacu Adrenalin</div>\r\n\r\n<div class="travelite_slider_text" style="font-size:30px; line-height:45px;">Anda</div>\r\n\r\n<div class="travelite_slider_text bottom_line_text" style="font-size:48px; line-height:45px;">Wisata Batu - Bromo</div>\r\n', '<div class="tour_packages" style="font-size:26px;">Paket Mulai <span style="font-size:38px">Rp 530.000/Pax</span></div>\r\n', 1, '2016-05-03 18:32:34', '2016-05-21 01:05:21', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `sosmed_account`
--

CREATE TABLE `sosmed_account` (
  `id` int(11) NOT NULL,
  `sosial_media` varchar(255) NOT NULL,
  `publish` int(11) NOT NULL DEFAULT '1',
  `account` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sosmed_account`
--

INSERT INTO `sosmed_account` (`id`, `sosial_media`, `publish`, `account`, `link`, `keterangan`) VALUES
(1, 'facebook', 1, 'bapontar', '-', ''),
(2, 'twitter', 1, 'bapontar', '-', NULL),
(3, 'google', 1, 'bapontar', '-', ''),
(4, 'instagram', 1, 'bapontar', '-', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `system_config`
--

CREATE TABLE `system_config` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `system_config`
--

INSERT INTO `system_config` (`id`, `key`, `value`, `keterangan`) VALUES
(1, 'about_us', '<p>Kami Agent biro perjalanan wisata yang bisa menjadi Partner perjalanan Anda ke tempat destinasi wisata terfavorit di Indonesia maupun Mancanegara. Layanan Kami saat ini:</p>\r\n\r\n<ul>\r\n	<li>- Tour Organizer (acara kantor, study tour, study banding dll)</li>\r\n	<li>- Guide Specialist</li>\r\n	<li>- Tour &amp; Travel</li>\r\n	<li>- Rent Car</li>\r\n	<li>- Reservasi tiket (pesawat, KAI, paket umroh, hotel, pembayaran speedy, pembayaran telpon, pulsa dll)</li>\r\n	<li>- Penjemputan kebandara/stasiun/terminal</li>\r\n	<li>- Trip wisata wonosobo (dieng), pacitan, solo, yogya dan lainnya</li>\r\n</ul>\r\n', NULL),
(2, 'address', 'Kutu Ngemplak\r\nRt.08 Rw.13 Sinduadi, Mlati', NULL),
(3, 'city', 'Sleman', NULL),
(4, 'provincies', 'Jawa Tengah', NULL),
(5, 'contry', 'Indonesia', NULL),
(6, 'zip_code', '40111', NULL),
(7, 'lat', '-7.558991', NULL),
(8, 'long', '110.824220', NULL),
(9, 'email', 'info@savanatourjogja.com', NULL),
(10, 'phone', '+62 813-9378-6030', NULL),
(11, 'whatsapp', '+62 813-9378-6030', NULL),
(12, 'homepage_video', 'https://www.youtube.com/watch?v=XxtAH_--QVU', NULL),
(13, 'facebook_panel', '<div class="fb-page" data-href="https://web.facebook.com/savanatourjogja/" data-tabs="timeline" data-height="480" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://web.facebook.com/savanatourjogja/"><a href="https://web.facebook.com/savanatourjogja/">Savanatourjogja</a></blockquote></div></div>', NULL),
(14, 'v_banner_wego_1', '<a href="http://www.wego.co.id/hotel?ts_code=d0c24&utm_source=d0c24&utm_medium=affiliate&utm_campaign=WAN_Affiliate&utm_content=banner" target="_blank" title="Cheap Hotels" rel="nofollow"><img src="http://www.wan.travel/en/resource/show/banner/161"/></a>', NULL),
(15, 'h_banner_wego_1', '<a href="http://www.wego.co.id/penerbangan?ts_code=d0c24&utm_source=d0c24&utm_medium=affiliate&utm_campaign=WAN_Affiliate&utm_content=banner" target="_blank" title="Cheap Flights" rel="nofollow"><img src="http://www.wan.travel/en/resource/show/banner/145"/></a>', NULL),
(16, 'g_adds_1', '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\r\n<!-- Iklan-4 -->\r\n<ins class="adsbygoogle"\r\n     style="display:block"\r\n     data-ad-client="ca-pub-8496671496317824"\r\n     data-ad-slot="4163340765"\r\n     data-ad-format="auto"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>', NULL),
(17, 'g_adds_2', '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\r\n<!-- Iklan-2 -->\r\n<ins class="adsbygoogle"\r\n     style="display:inline-block;width:300px;height:600px"\r\n     data-ad-client="ca-pub-8496671496317824"\r\n     data-ad-slot="1349475169"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>', NULL),
(18, 'g_adds_3', '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\r\n<!-- Iklan-3 -->\r\n<ins class="adsbygoogle"\r\n     style="display:inline-block;width:728px;height:90px"\r\n     data-ad-client="ca-pub-8496671496317824"\r\n     data-ad-slot="5779674766"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>', NULL),
(19, 'g_adds_4', '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>\r\n<!-- Iklan-4 -->\r\n<ins class="adsbygoogle"\r\n     style="display:block"\r\n     data-ad-client="ca-pub-8496671496317824"\r\n     data-ad-slot="4163340765"\r\n     data-ad-format="auto"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tour`
--

CREATE TABLE `tour` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `top` int(11) NOT NULL DEFAULT '0',
  `tour_kategori_id` int(11) NOT NULL,
  `harga_paket` varchar(255) DEFAULT NULL,
  `durasi` varchar(255) DEFAULT NULL,
  `minimal` varchar(255) DEFAULT NULL,
  `deskripsi` text NOT NULL,
  `itenary` text,
  `destinasi` text,
  `include` text,
  `exclude` text,
  `kabupaten_id` int(11) DEFAULT NULL,
  `publish` int(11) NOT NULL DEFAULT '1',
  `local_transport` int(11) NOT NULL DEFAULT '1',
  `pesawat` int(11) NOT NULL DEFAULT '1',
  `tiketing` int(11) NOT NULL DEFAULT '1',
  `makan` int(11) NOT NULL DEFAULT '1',
  `akomodasi` int(11) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tour`
--

INSERT INTO `tour` (`id`, `judul`, `top`, `tour_kategori_id`, `harga_paket`, `durasi`, `minimal`, `deskripsi`, `itenary`, `destinasi`, `include`, `exclude`, `kabupaten_id`, `publish`, `local_transport`, `pesawat`, `tiketing`, `makan`, `akomodasi`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'Wisata Kota Batu Malang ', 1, 2, 'Rp 530.000/pax', '2D1N', '6 orang', '<p>Ingin merasakan sensasi membuka hammock di ketingngian? ingin merasakan meluncur dari ketinggian tebing ? Ingin merasakan wahana kegiatan ketinggian lainnya? Inilah saatnya kawan ! Kami membuka kesempatan untuk teman-teman yang ingin menguji nyali terhadap ketinggian untuk bergabung di perjalanan kami? Tunggu apa lagi kawan ! Mumpung masih ada quota, jangan sampai kehabisan !! Semua wahana akan di instalasi oleh instruktur dan panitia, peserta kegiatan tinggal melihat instalasinya dan setelah selesai teman-teman akan mencobanya secara bergantian, naik wahananya, merasakan sensasinya dan berfoto. Ditunggu partisipasinya kawan !!</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Note: Kami juga melayani <em>Private Trip</em> dan <em>Company Outings</em>.</p>\r\n', '<p><strong>Day 1 </strong></p>\r\n\r\n<ul>\r\n	<li>08.00 &ndash; 09.00 : Penjemputan peserta.</li>\r\n	<li>09.30 &ndash; 10.30 : Perjalanan Menuju Batu.</li>\r\n	<li>10.30 &ndash; 12.00: TIba di Gunung Banyak &ndash; Bermain di Ruma Pohon.</li>\r\n	<li>12.00 &ndash; 13.00 : Makan siang.</li>\r\n	<li>13.00 &ndash; 17.00 : Menuju museum Angkut - Explore museum Angkut.</li>\r\n	<li>17.00 &ndash; 16.00 : Back to Malang. Istirahat. Day 2</li>\r\n	<li>00.00 &ndash; 0.15 : Persiapan sebelum penjemputan dengan Jeep.</li>\r\n	<li>00.00 &ndash; 03.00 : Perjalanan menuju Penanjakan Bromo - The best view Sunrise.</li>\r\n	<li>04.00 &ndash; 05.30 : Menikmati Sunrise di Penanjakan Bromo - Hunting foto dan foto bersama.</li>\r\n	<li>05.30 &ndash; 10.00 : Explore kawah Bromo - Pasir Berbisik - Bukit Teletubis.</li>\r\n	<li>10.30 &ndash; 13.00 : Back to Malang.</li>\r\n</ul>\r\n\r\n<p><strong>Day 2</strong></p>\r\n\r\n<ul>\r\n	<li>08.00 Menuju Bedugul</li>\r\n	<li>09.30 Sampai di Pura Ulun Danu Bedugul</li>\r\n	<li>11.30 Menuju Pusat Oleh oleh joger</li>\r\n	<li>12.30 Servis makan siang ( servis makan ke 3)</li>\r\n	<li>13.00 Menuju Tanah Lot</li>\r\n	<li>15.00 sampai di tanah lot, menikmati pura tanah lot dan sunset</li>\r\n	<li>18.00&nbsp; makan malam di krisna pusat oleh oleh (servis makan ke 4)</li>\r\n	<li>20.00 Menuju hotel beristirahat dan acara bebas</li>\r\n</ul>\r\n', '<ol>\r\n	<li>Point View Sunrise.</li>\r\n	<li>Kawah Bromo.</li>\r\n	<li>Pasir Berbisik.</li>\r\n	<li>Savanna Bukit Teletubis.</li>\r\n	<li>Pura Luhur Bromo.</li>\r\n	<li>Museum Angkut.</li>\r\n	<li>Paralayang.</li>\r\n	<li>Rumah Pohon.</li>\r\n</ol>\r\n', '<ul>\r\n	<li>Mobil + Tol + Parkir.</li>\r\n	<li>Guide.</li>\r\n	<li>Tiket masuk destinasi wisata..</li>\r\n	<li>Makan 4x (Makan Siang dan Makan Malam).</li>\r\n	<li>Air mineral.</li>\r\n	<li>Hotel Gosyen atau Alron (Jika Mengambil Paket Include Hotel)</li>\r\n	<li>Dokumentasi.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Sarapan.</li>\r\n	<li>Permainan di Tanjung Benoa Water Sport.</li>\r\n	<li>Tari kecak di Uluwatu.</li>\r\n</ul>\r\n', 162, 1, 1, 0, 1, 1, 0, '2016-05-11 17:03:21', '2016-05-13 18:57:07', 1, 1),
(2, 'Danau Ranu Kumbolo', 1, 2, 'Rp 560.000/Pax', '2D1N', '6 orang.', '<p>Ingin merasakan sensasi membuka hammock di ketingngian? ingin merasakan meluncur dari ketinggian&nbsp;tebing ? Ingin merasakan wahana kegiatan ketinggian lainnya? Inilah saatnya kawan ! Kami membuka kesempatan untuk teman-teman yang ingin menguji nyali terhadap ketinggian untuk bergabung di perjalanan kami? Tunggu apa lagi kawan ! Mumpung masih ada quota, jangan sampai kehabisan !!</p>\r\n\r\n<p>Semua wahana akan di instalasi oleh instruktur dan panitia, peserta kegiatan tinggal melihat instalasinya dan setelah selesai teman-teman akan mencobanya secara bergantian, naik wahananya, merasakan sensasinya dan berfoto. Ditunggu partisipasinya kawan !!!</p>\r\n', '<p><strong>Day 1</strong></p>\r\n\r\n<ul>\r\n	<li>08.00 &ndash; 10.00 : Berkumpul di meeting point stasiun Malang dan persiapan.</li>\r\n	<li>10.00 - 12.30 : Menuju desa Ranu Pani.</li>\r\n	<li>12.30 &ndash; 13.30 : Briefing di pos Ranu Pani dan registrasi.</li>\r\n	<li>13.30 &ndash; 18.30 : Sampai di Ranu Kumboloumbolo - Mendirikan tenda dan membersihkan diri.</li>\r\n	<li>20.00 &ndash; 20.30 : Menikmati makan malam - Cofee atau teh (fasilitas makan - 1)</li>\r\n	<li>21.00 &ndash; 05.00 : Goodnight Ranukumbolo J</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Day 2</strong></p>\r\n\r\n<ul>\r\n	<li>05.00 &ndash; 08.00 : Menikmati Sunrise Ranu Kumbolo &ndash; Coffee - Berfoto - &nbsp;Mendaki Tanjakan Cinta.</li>\r\n	<li>08.00 &ndash; 08.30 : Sarapan (Fasilitas makan - 2)</li>\r\n	<li>09.00 &ndash; 10.00 : Packing dan persiapan pulang.</li>\r\n	<li>10.00 &ndash; 15.00 : Sampai Ranu Pani.</li>\r\n	<li>15.00 &ndash; 16.00 : Bersih bersih dan makan (Fasilitas makan - 3)</li>\r\n	<li>16.00 &ndash; 18.00 : Back to Malang.</li>\r\n	<li>18.00 &ndash; 18.05 : Sayonara J</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Meeting </strong><strong>P</strong><strong>oint</strong>: Stasiun Malang.</p>\r\n\r\n<p><em>Note: Intinerary dapat berubah dan bersifat conditional.</em></p>\r\n', '<ul>\r\n	<li>Pasar Tumpang</li>\r\n	<li>Ranu Pane</li>\r\n	<li>Ranu Kumbolo</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Jeep + Driver.</li>\r\n	<li>Tiket masuk Semeru.</li>\r\n	<li>Makan 3x.</li>\r\n	<li>Logistik dan Air mineral.</li>\r\n	<li>Tenda.</li>\r\n	<li>Sleeping Bag</li>\r\n	<li>Matras.</li>\r\n	<li>Dokumentasi.</li>\r\n	<li>Merchandise.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Porter Barang.</li>\r\n	<li>Makan siang sebelum Tracking di Ranu Pani.</li>\r\n	<li>Transportasi dari rumah ke meeting point.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>SISTEM </strong></p>\r\n\r\n<ul>\r\n	<li>Jika tidak memakai poter maka barang akan di share bersama.</li>\r\n</ul>\r\n', 159, 1, 1, 1, 1, 1, 1, '2016-05-11 17:19:24', '2016-05-13 18:58:12', 1, 1),
(3, 'Pulau Bali', 1, 1, 'Rp 655.000/pax', '2D1N', '6 Orang', '<p>-<br />\r\n&nbsp;</p>\r\n', '', '', '', '', 35, 1, 1, 0, 1, 1, 1, '2016-05-11 18:09:53', '2016-05-13 18:57:57', 1, 1),
(4, 'Dataran Tinggi Dieng', 0, 1, 'Rp 800.000/pax', '2D1N', '4 Orang', '<p>Menikmati Keindahan Jawa Tengah.</p>\r\n', '<p><strong>Day 1</strong></p>\r\n\r\n<p>08.00 - 11.30 : Perjalanan Jogja &ndash; Dieng.<br />\r\n11.30 - 13.00 : Ishoma.<br />\r\n13.00 - 15.00 : Batu ratapan angin.<br />\r\n15.00 - 17.00 : Wisata ke Candi Arjuna.<br />\r\n17.00 - 17.30 : Perjalanan ke homestay.<br />\r\n17.30 - ....&nbsp; &nbsp; &nbsp; : Makan, Ccara bebas dan istirahat. Goodnight Dieng.<br />\r\n<br />\r\n<strong>Day 2</strong><br />\r\n<br />\r\n03.15 - 03.30 : Persiapan menuju golden sunrise view Sikunir.<br />\r\n03.30 - 04.00 : Perjalanan menuju Sikunir.<br />\r\n04.00 - 06.00 : Menikmati golden sunrise.<br />\r\n06.00 - 06.30 : Turun dari sikunir.<br />\r\n06.30 - 07.00 : Perjalanan menuju homestay.<br />\r\n07.00 - 09.30 : Sarapan, istirahat, mandi dan persiapan ke tempat wisata selanjutnya.<br />\r\n09.30 - 10.00 : Perjalanan menuju kawah Sikidang.<br />\r\n10.00 - 12.00 : Menikmati wisata kawah sikidang.<br />\r\n12.00 - 14.00 : Menikmati wisata Telaga Warna.<br />\r\n14.00 - 15.30 : Menuju pusat oleh-oleh Dieng Wonosobo.<br />\r\n15.30 - 17.00 : Menuju warung Mie Ongklok khas Wonosobo.<br />\r\n17.00 - 19.00 : Perjalanan kembali ke Jogja<br />\r\n&nbsp;</p>\r\n\r\n<p>Note: Kami juga melayani <em>Private Trip</em> dan <em>Company Outings</em>.</p>\r\n', '<ol>\r\n	<li>Batu Ratapan Angin</li>\r\n	<li>Wisata Candi Arjuna</li>\r\n	<li>Golden Sunrise Sikunir</li>\r\n	<li>Kawah Sikadang</li>\r\n	<li>Telaga Warna</li>\r\n</ol>\r\n', '<ul>\r\n	<li>Transport Jogja &ndash; Dieng PP&nbsp;</li>\r\n	<li>Guide.</li>\r\n	<li>Tiket masuk Destinasi Wiata.</li>\r\n	<li>Makan 4x</li>\r\n	<li>Air mineral.</li>\r\n	<li>Homestay Exclusive</li>\r\n	<li>Dokumentasi.</li>\r\n</ul>\r\n', '<p>-</p>\r\n', 105, 1, 1, 0, 1, 1, 1, '2016-05-13 16:43:32', '2016-05-13 18:57:25', 1, 1),
(5, 'Malang - Bromo (Pantai)', 0, 1, 'Rp 465.000/pax', '2D1N', '6 orang.', '<p>-</p>\r\n', '<p><strong>Day 1</strong></p>\r\n\r\n<ul>\r\n	<li>08.00 &ndash; 09.00 : Penjemputan peserta.</li>\r\n	<li>09.00 &ndash; 11.00 : Perjalanan Menuju pantai</li>\r\n	<li>11.00 &ndash; 11.30 : Tracking ke pantai 3 warna</li>\r\n	<li>11.20 &ndash; 15.00 : Explore Pantai 3 warna, Gatra dan Clungup serta makan siang &nbsp;</li>\r\n	<li>15.00 &ndash; 15.30 : Persiapan pulang</li>\r\n	<li>15.30 &ndash; 18.00 : Perjalanan pulang, makan malam, check in&nbsp; penginapan dan beristirahat</li>\r\n</ul>\r\n\r\n<p><strong>Day 2</strong></p>\r\n\r\n<ul>\r\n	<li>00.00 &ndash; 0.15 : Persiapan sebelum penjemputan dengan Jeep dan check out</li>\r\n	<li>00.00 &ndash; 03.00 : Perjalanan menuju Penanjakan Bromo - The best view Sunrise.</li>\r\n	<li>04.00 &ndash; 05.30 : Menikmati Sunrise di Penanjakan Bromo - Hunting foto dan foto bersama.</li>\r\n	<li>05.30 &ndash; 10.00 : Explore kawah Bromo - Pasir Berbisik - Bukit Teletubis.</li>\r\n	<li>10.30 &ndash; 13.00 : Back to Malang.</li>\r\n</ul>\r\n\r\n<p>Note: Kami juga melayani <em>Private Trip</em> dan <em>Company Outings</em>.</p>\r\n', '<ol>\r\n	<li>Pura Luhur Bromo.</li>\r\n	<li>Pantai gatra</li>\r\n	<li>Pantai clungup</li>\r\n	<li>Pantai 3 warna</li>\r\n	<li>Point View Sunrise.</li>\r\n	<li>Kawah Bromo.</li>\r\n	<li>Pasir Berbisik.</li>\r\n	<li>Savanna Bukit Teletubis.&nbsp;</li>\r\n</ol>\r\n', '<ul>\r\n	<li>Guide.</li>\r\n	<li>Jeep Menuju Bromo + Driver + BBM.</li>\r\n	<li>Tiket masuk Kawasan Bromo.</li>\r\n	<li>Tiket Masuk pantai</li>\r\n	<li>Mobil menuju pantai + Driver + BBM + Parkir.</li>\r\n	<li>Air mineral.</li>\r\n	<li>Dokumentasi.</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Penginapan.</li>\r\n	<li>Makan selama Trip.</li>\r\n</ul>\r\n', 153, 1, 1, 0, 1, 1, 1, '2016-05-13 17:46:59', '2016-05-13 18:57:41', 1, 1),
(6, 'Pullau Harapan', 0, 1, 'Rp 450.000/pax', '2D1N', '10 Orang', '<p>-</p>\r\n', '<p><strong>Meeting Point: Muara Angke.</strong></p>\r\n\r\n<p>Note: Kami juga melayani <em>Private Trip</em> dan <em>Company Outings</em>.</p>\r\n', '', '<ul>\r\n	<li>Transport Muara Angke- Pulau PP</li>\r\n	<li>Biaya masuk pelabuhan.</li>\r\n	<li>Makan 3x + 1x sarapan.</li>\r\n	<li>BBQ.</li>\r\n	<li>Welcome Drink.</li>\r\n	<li>Homestay AC dekat pantai.</li>\r\n	<li>Snorkeling set.</li>\r\n	<li>Snorkeling di 3 spot.</li>\r\n	<li>Shuttle ship.</li>\r\n	<li>Island Hopping ke 3 &ndash; 5 pulau.</li>\r\n	<li>Biaya sandar kapal saat jelajah.</li>\r\n	<li>Sunset Cruise.</li>\r\n	<li>Mineral Water.</li>\r\n	<li>Camera Up &amp; Underwater.</li>\r\n	<li>Wisata Rumah Adat Bugis.</li>\r\n	<li>Tiket masuk Penangkaran Penyu.</li>\r\n	<li>Guide.</li>\r\n	<li>Asuransi.</li>\r\n</ul>\r\n', '<p>-</p>\r\n', 65, 1, 1, 0, 1, 1, 1, '2016-05-13 17:53:01', '2016-05-13 18:58:27', 1, 1),
(7, 'Banyuwangi', 0, 1, 'Rp 450.000/Pax', '2D1N', '10 Orang', '<p>-</p>\r\n', '<p><strong>Meeting Point: Muara Angke.</strong></p>\r\n\r\n<p>Note: Kami juga melayani <em>Private Trip</em> dan <em>Company Outings</em>.</p>\r\n', '<ul>\r\n	<li>Kawah Ijen</li>\r\n	<li>Baluran</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Transport Muara Angke- Pulau PP</li>\r\n	<li>Biaya masuk pelabuhan.</li>\r\n	<li>Makan 3x + 1x sarapan.</li>\r\n	<li>BBQ.</li>\r\n	<li>Welcome Drink.</li>\r\n	<li>Homestay AC dekat pantai.</li>\r\n	<li>Snorkeling set.</li>\r\n	<li>Snorkeling di 3 spot.</li>\r\n	<li>Shuttle ship.</li>\r\n	<li>Island Hopping ke 3 &ndash; 5 pulau.</li>\r\n	<li>Biaya sandar kapal saat jelajah.</li>\r\n	<li>Sunset Cruise.</li>\r\n	<li>Mineral Water.</li>\r\n	<li>Camera Up &amp; Underwater.</li>\r\n	<li>Wisata Rumah Adat Bugis.</li>\r\n	<li>Tiket masuk Penangkaran Penyu.</li>\r\n	<li>Guide.</li>\r\n	<li>Asuransi.</li>\r\n</ul>\r\n', '<p>-</p>\r\n', 141, 1, 1, 0, 1, 1, 1, '2016-05-13 18:21:48', '2016-05-13 18:58:46', 1, 1),
(8, 'Dieng Culture Festival 2016', 1, 3, 'Rp 950.000/Pax', '3D2N', '10 Orang', '<p><strong>RAGAM ACARA DIENG CULTURE FESTIVAL 2016</strong></p>\r\n\r\n<p><em>Acara Dilaksanakan Pada Tanggal 5,6,7 Agustus 2016</em></p>\r\n\r\n<p>Panitia penyelenggara Dieng Culture Festival tahun 2016 berencana menyuguhkan hal yang baru dalam event budaya tahunan itu. Tujuanya agar para wisatawan dapat terlibat dan menyaksikan hal yang berbeda dari tahun penyelenggaraan Dieng Culture Festival tahun-tahun sebelumnya.</p>\r\n\r\n<p>Ragam acara Dieng Culture Festival 2016 Dieng Culture Festival 2016 akan dimulai pada tangga&nbsp;5 Agustus 2016, acara pembukaan festival ini dilaksanakan pada&nbsp;pukul 14.00&nbsp;di panggung utama, sebelah timur kompleks candi Arjuna. Rencananya setelah pembukaan berlangsung, dipanggung utama akan ada perfom musik Kyai Kanjeng beserta Cak Nun* dari Yogyakarta (*masih dalam konfirmasi). Pertunjukan pembuka festival tersebut akan berakhir pada&nbsp;pukul 17.00&nbsp;WIB.</p>\r\n\r\n<p>Bagi pengunjung yang berminat menikmati sunset, bisa naik ke bukit skuter/scooter yang dapat ditempuh hanya dalam kurun waktu 15 menit dari desa Dieng Kulon, dengan membayar biaya kontribusi bagi pengelola lokasi wisata sebesar lima ribu rupiah. Dari bukit ini juga bisa melihat landscape pemukiman di dataran tinggi Dieng.</p>\r\n\r\n<p>Setelah melihat sunset, pengunjung segera bersiap untuk menikmati pertunjukanJazzatasawan&nbsp;yang akan digelar di panggung utama, timur kompleks candi arjuna. Pertunjukan akan dimulai&nbsp;pukul 20.00&nbsp;WIB tepat, sangat disarankan pengunjung yang telah memiliki ID Card (tiket khusus DCF 2016) untuk hadir di venue pertunjukan jazz pada&nbsp;pukul 19.30&nbsp;WIB. Pertunjukan musik ditengah suhu 2-5 derajat celcius ini digelar pada tanggal 5 Agustus 2015 (start&nbsp;pukul 20.00&nbsp;WIB) dengan menampilkan sejumlah musisi jazz dari berbagai pelosok di tanah air. Para penampil pertunjukan Jazzatasawan akan dirilis bulan Juli 2016.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Keesokan harinya&nbsp;tanggal 6 agustus 2016, pengunjung bisa menikmati sunrise diberbagai puncak gunung di dataran tinggi Dieng. Bisa dari puncak Sikunir, puncak Prau, puncak Pakuwaja, dan atau puncak Pangonan. Disarankan, pengunjung start dari penginapan masing-masing&nbsp;pukul 02.00-04.00, agar tidak terjebak macet saat mendaki dan dapat menikmati sunrise dengan leluasa, tergantung puncak yang hendak dituju.</p>\r\n\r\n<p>Setelah puas melihat golden sunrise di pebagai puncak gunung, pada&nbsp;pukul 07.00&nbsp;s/d selesai, pengunjung dapat mengikuti acara &ldquo;Jalan Kaki Keliling Kampung&rdquo; di dataran tinggi Dieng, diakhiri dengan penerbangan ribuan balon gas dan minum purwaceng bersama-sama. Panitia akan menyediakan sejumlah doorprize buat peserta jalan kaki keliling kampung. Start dan finish dari kompleks gedung Soeharto-Withlam, Dieng.</p>\r\n\r\n<p>Pada tanggal 6 Agustus 2016, sepanjang hari ada ragam pertunjukan seni tradisi yang tersebar diberbagai lokasi, baik dipanggung utama maupun panggung khusus budaya. Ragam pertunjukan seni tradisi itu bukan hanya dari dataran tinggi Dieng, tetapi juga dari berbagai daerah lain yang ikut berpartisipasi dalam Dieng Culture Festival tahun 2016. Ditengah menyaksikan pertunjukan seni tradisi tersebut, pengunjung dapat menikmati wisata alam di Dieng, seperti telaga warna, kawah sikidang, maupun lokasi wisata alam lainnya.</p>\r\n\r\n<p>Malam harinya (malam minggu lho kaaak) pada&nbsp;pukul 20.00&nbsp;WIB, di panggung utama akan digelar pertunjukan musik akustik, dan stand up comedy, sebelum acara penerbangan lampion yang akan dilakukan secara bersama-sama. Lampion akan diterbangkan secara bersama-sama pada&nbsp;pukul 21.00&nbsp;WIB. Sangat disarankan, semua pengunjung untuk TIDAK menerbangkan lampion sebelum ada aba-aba dari MC acara. Diperkirakan, waktu penerbangan memakan waktu hingga 45 menit, karena panitia menyediakan 5000 lampion untuk para pengunjung ber-ID Card. Pada saat bersamaan, sejumlah musisi akan menyemarakan penerbangan lampion dengan berbagai lantunan lagu dan musik dari panggung utama.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Selain lampion, ada pula kembang api yang dibagikan kepada pengunjung yang membeli tiket khusus DCF saat penukaran tiket. Kembang api ini akan dinyalakan sesaat setelah lampion diterbangkan, diperkirakan pada&nbsp;pukul 21.45&nbsp;WIB, pesta kembang api dimulai. Ada 15 ribu letusan kembang api akan menghiasi langit Dieng, setelah penerbangan lampion usai. Penyelenggara Dieng Culture Festival berharap pengunjung bisa mematuhi ketentuan, bahwa kembang api dinyalakan setelah lampion diterbangkan, agar tidak mengganggu lampion yang sedang terbang. Acara penerbangan lampion, pesta kembang api, dan musik akustik ini akan berakhir pada&nbsp;pukul 23.00&nbsp;WIB.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Pada hari&nbsp;Minggu, 7 Agustus 2016&nbsp;adalah hari terakhir festival budaya di negeri atas awan ini, yakni rangkaian ritual cukur rambut anak gembel. Acara dimulai dengan kirab budaya dari rumah pemangku adat Mbah Naryono pada&nbsp;pukul 06.00&nbsp;WIB. Khusus acara kirab budaya, panitia penyelenggara akan membuka kesempatan bagi pengunjung yang berminat terlibat untuk ikut kirab budaya dengan pakaian adat masing-masing. Namun, kami membatasi hanya untuk 80 orang. Para wisatawan yang berminat untuk terlibat dalam kirab budaya akan diminta mendaftar dengan ketentuan yang ditentukan oleh panitia.</p>\r\n\r\n<p>Setelah kirab budaya keliling Dieng, rangkaian acara ritual potong rambut gembel adalah jamasan di Dharmasala, sebelum pemotongan rambut di Kompleks Candi Arjuna. Sejumlah anak berambut gembel, akan dicukur rambutnya oleh sejumlah tokoh yang ditunjuk oleh panitia penyelenggara untuk mencukur, dipimpin oleh pemimpin spiritual suku Dieng, mbah Naryono. Acara ini akan berakhir pada&nbsp;pukul 13.00&nbsp;WIB.</p>\r\n\r\n<p>Sementara di kompleks candi Arjuna ada upacara pemotongan rambut, dipanggung budaya ada pagelaran wayang kulit khusus ritual, dan berbagai pertunjukan seni tradisi dipanggung dan atau lokasi lain yang dapat dinikmati oleh para pengunjung.</p>\r\n\r\n<p>Bersiaplah untuk hadir, saksikan, dan terlibat dalam Dieng Culture Festival tahun 2016.</p>\r\n\r\n<p><em>Note: Di Dieng hanya ada ATM BRI, maka bawalah uang cash lebih bila tidak mau repot bila ada kebutuhan mendadak.</em></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Notes :</strong></p>\r\n\r\n<ul>\r\n	<li>DP : 50 %&nbsp;</li>\r\n	<li>(Minimal Kuota 10 orang, bila kurang dari 10 harga akan menyesuaikan)</li>\r\n</ul>\r\n', '<p>MORE INFO/Pendaftaraan &nbsp;</p>\r\n\r\n<p>Hub :&nbsp;081210445099 (WhatsApp/Line Available)</p>\r\n', '', '<ul>\r\n	<li>Makan 6 x</li>\r\n	<li>Homestay&nbsp;</li>\r\n	<li>Guide Lokal</li>\r\n	<li>Tiket VIP DCF7</li>\r\n	<li>T- Shirt DCF7</li>\r\n	<li>ID Card DCF7</li>\r\n	<li>Selendang Khusus untuk upacara&nbsp;</li>\r\n	<li>Lampion + Kembang Api</li>\r\n	<li>Wisata Dieng</li>\r\n</ul>\r\n', '<ul>\r\n	<li>Tiket Dari Kota Asal</li>\r\n	<li>Pengeluaran Pribadi</li>\r\n	<li>Perlengkapan Pribadi</li>\r\n	<li>Belanja Oleh-Oleh khas Dieng</li>\r\n</ul>\r\n', 105, 1, 1, 0, 0, 0, 1, '2016-05-13 18:45:39', '2016-05-20 21:07:20', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tour_kategori`
--

CREATE TABLE `tour_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(255) NOT NULL,
  `keterangan` text,
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tour_kategori`
--

INSERT INTO `tour_kategori` (`id`, `kategori`, `keterangan`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 'Adventure', '', NULL, NULL, NULL, NULL),
(2, 'Hiking', '', '2016-05-12 11:04:12', NULL, 1, NULL),
(3, 'Event', '', '2016-05-13 18:40:29', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tour_photo`
--

CREATE TABLE `tour_photo` (
  `id` int(11) NOT NULL,
  `tour_id` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `photo` text NOT NULL,
  `cover` int(11) NOT NULL DEFAULT '1',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tour_photo`
--

INSERT INTO `tour_photo` (`id`, `tour_id`, `keterangan`, `photo`, `cover`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(5, 3, '', '1462992547_1.jpg', 0, NULL, NULL, NULL, NULL),
(11, 4, '', '1463157845_IMG-20160215-WA0014.jpg', 0, NULL, NULL, NULL, NULL),
(13, 5, '', '1463161656_1455543884034.jpg', 0, NULL, NULL, NULL, NULL),
(15, 5, '', '1463161678_20140219_201813.jpg', 0, NULL, NULL, NULL, NULL),
(16, 5, '', '1463161688_DSCN2949.JPG', 1, NULL, NULL, NULL, NULL),
(17, 6, '', '1463161997_DSCN0244.JPG', 0, NULL, NULL, NULL, NULL),
(18, 6, '', '1463162005_DSCN1175.JPG', 1, NULL, NULL, NULL, NULL),
(19, 1, '', '1463162218_Museum-Angkut.jpg', 1, NULL, NULL, NULL, NULL),
(20, 1, '', '1463162229_Paralayang.JPG', 0, NULL, NULL, NULL, NULL),
(21, 2, '', '1463162268_20141031_163916---Copy.jpg', 0, NULL, NULL, NULL, NULL),
(23, 3, '', '1463162331_DREAMLAND-BALI.JPG', 0, NULL, NULL, NULL, NULL),
(24, 3, '', '1463162347_SUNSET-KUTA.JPG', 1, NULL, NULL, NULL, NULL),
(25, 3, '', '1463162364_TANUNG-BENOA-WATER-SPORT.JPG', 0, NULL, NULL, NULL, NULL),
(26, 4, '', '1463162392_IMG-20160215-WA0015.jpg', 0, NULL, NULL, NULL, NULL),
(27, 4, '', '1463162403_IMG-20160215-WA0017.jpg', 0, NULL, NULL, NULL, NULL),
(28, 6, '', '1463162438_DSCN0965.JPG', 0, NULL, NULL, NULL, NULL),
(29, 2, '', '1463163155_20141101_044425---Copy.jpg', 1, NULL, NULL, NULL, NULL),
(31, 7, '', '1463163736_TAMAN-NASIONAL-BALURAN.JPG', 0, NULL, NULL, NULL, NULL),
(32, 7, '', '1463163746_TAMAN-NASIONAL-BALURAN-2.JPG', 0, NULL, NULL, NULL, NULL),
(33, 7, '', '1463163758_KAWAH-IJEN-2.jpg', 0, NULL, NULL, NULL, NULL),
(34, 7, '', '1463163767_KAWAH-IJEN-1.jpg', 0, NULL, NULL, NULL, NULL),
(35, 7, '', '1463164013_TAMAN-NASIONAL-BALURAN-3.JPG', 1, NULL, NULL, NULL, NULL),
(36, 8, '', '1463165192_Dieng-Cultural-Festival-2014.jpg', 1, NULL, NULL, NULL, NULL),
(37, 8, '', '1463165209_Dieng-Culture-Festival.jpg', 0, NULL, NULL, NULL, NULL),
(38, 4, '', '1463165499_IMG-20160215-WA0013.jpg', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `last_login` timestamp NULL DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_time` timestamp NULL DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `user_role_id`, `username`, `password`, `nama`, `email`, `status`, `last_login`, `is_delete`, `create_time`, `update_time`, `create_user_id`, `update_user_id`) VALUES
(1, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator', 'admin@gmail.com', 1, '2016-10-16 08:16:46', 0, '2016-01-15 18:59:17', NULL, NULL, NULL),
(2, 2, 'member', '21232f297a57a5a743894a0e4a801fc3', 'Kasir', 'Kasir', 1, '2016-10-14 20:50:05', 0, '2016-01-15 19:08:12', '2016-01-15 19:11:08', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_kategori_id` (`blog_kategori_id`);

--
-- Indexes for table `blog_kategori`
--
ALTER TABLE `blog_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eksternal_link`
--
ALTER TABLE `eksternal_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_photo`
--
ALTER TABLE `event_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provinsi_id` (`provinsi_id`);

--
-- Indexes for table `provinsi`
--
ALTER TABLE `provinsi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pulau_id` (`pulau_id`);

--
-- Indexes for table `pulau`
--
ALTER TABLE `pulau`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sosmed_account`
--
ALTER TABLE `sosmed_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_config`
--
ALTER TABLE `system_config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tour_kategori_id` (`tour_kategori_id`);

--
-- Indexes for table `tour_kategori`
--
ALTER TABLE `tour_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tour_photo`
--
ALTER TABLE `tour_photo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_role` (`user_role_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `blog_kategori`
--
ALTER TABLE `blog_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `eksternal_link`
--
ALTER TABLE `eksternal_link`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `event_photo`
--
ALTER TABLE `event_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inbox`
--
ALTER TABLE `inbox`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kabupaten`
--
ALTER TABLE `kabupaten`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=499;
--
-- AUTO_INCREMENT for table `provinsi`
--
ALTER TABLE `provinsi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `pulau`
--
ALTER TABLE `pulau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sosmed_account`
--
ALTER TABLE `sosmed_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `system_config`
--
ALTER TABLE `system_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `tour`
--
ALTER TABLE `tour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tour_kategori`
--
ALTER TABLE `tour_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tour_photo`
--
ALTER TABLE `tour_photo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kabupaten`
--
ALTER TABLE `kabupaten`
  ADD CONSTRAINT `kabupaten_ibfk_1` FOREIGN KEY (`provinsi_id`) REFERENCES `provinsi` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `provinsi`
--
ALTER TABLE `provinsi`
  ADD CONSTRAINT `provinsi_ibfk_1` FOREIGN KEY (`pulau_id`) REFERENCES `pulau` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
