<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		
		$this->render('index');
	}
	
	public function actionAbout()
	{
		$this->render('about');
	}
	
	public function actionGallery()
	{
		$this->render('gallery');
	}
	
	public function actionEvent()
	{
		$this->render('event');
	}
	
	public function actionBlog()
	{
		$this->render('blog');
	}
	
	public function actionBlogDetail($id)
	{
		$model = Blog::model()->findByPk($id);
		
		$tags = explode(',', $model->tags);
		
		$this->render('blogDetail', array('model'=>$model, 'tags'=>$tags));	
	}
	
	public function actionBlogTag($key)
	{
		$key = str_replace(' ', '', $key);
		/*
		if($key != null){
			$sqlBlog = "SELECT * FROM blog WHERE konten LIKE '%$key%' OR  judul LIKE '%$key%'";
			$resultBlog = Blog::model()->findAllBySql($sqlBlog);			
		}
		
		*/
		
		$dataProvider=new CActiveDataProvider('Blog', array(
								'criteria'=>array('order'=>'id DESC',
								'condition'=>"judul LIKE '%$key%' OR konten LIKE '%$key%' OR tags LIKE '%$key%'",
								//'params'=>array(':key'=>$key),
								),
								'pagination'=>array('pageSize'=>4,), 
							));
		
		$this->render('blogTag', array('key'=>$key, 'dataProvider'=>$dataProvider));
	}
	
	public function actionTours()
	{
		$this->render('tours');
	}
	
	public function actionDisclaimer()
	{
		$this->render('disclaimer');
	}
	
	public function actionHotels()
	{
		$this->render('ticket');
	}
	
	public function actionTourDetail($id)
	{
		$model = Tour::model()->findByPk($id);
		
		$this->render('tourDetail', array('model'=>$model));	
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = '//layouts/column2';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	
	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new Inbox;
		$a = rand(1,9);
		$b = rand(1,9);
		
		if(isset($_POST['Inbox']))
		{		
			$model->attributes=$_POST['Inbox'];
			if($model->validate())
			{
				if($model->save())
				{
					$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
					$subject='=?UTF-8?B?'.base64_encode("[BAPONTAR.ID] Visitor Message").'?=';
					$headers="From: $name <{$model->email}>\r\n".
						"Reply-To: {$model->email}\r\n".
						"MIME-Version: 1.0\r\n".
						"Content-Type: text/plain; charset=UTF-8";
	
					mail(Yii::app()->params['adminEmail'],$subject,$model->message,$headers);
					Yii::app()->user->setFlash('contact','Terima kasih sudah menghubungi kami. Kami akan merespon anda secepatnya.');
					$this->refresh();
				}
			}
		}
		$this->render('contact',array('model'=>$model, 'a'=>$a, 'b'=>$b));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout ='//layouts/administrator/login-form';
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				//$this->redirect(Yii::app()->user->returnUrl);
				$this->redirect(array('administrator'));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	public function actionAdministrator()
	{
		if(!Yii::app()->user->isGuest)
		{
			$this->layout ='//layouts/administrator/column1';
			$this->render('backendDashboard');
		}else{
			$this->redirect(array('login'));
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}