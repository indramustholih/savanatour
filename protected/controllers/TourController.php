<?php

class TourController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/administrator/column2';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','view'),
'users'=>array('@'),
),
array('allow', // allow authenticated user to perform 'create' and 'update' actions
'actions'=>array('create','update', 'UploadPhoto', 'hapusPhoto'),
'users'=>array('@'),
),
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('admin','delete'),
'users'=>array('admin'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

    /**
    * Displays a particular model.
    * @param integer $id the ID of the model to be displayed
    */
    public function actionView($id)
    {
        $TourPhoto = new TourPhoto;
        $this->render('view',array(
            'model'=>$this->loadModel($id),
            'TourPhoto'=>$TourPhoto,
        ));
    }
    
    public function actionUploadPhoto()
    {
        $model=new TourPhoto;
    
        // Uncomment the following line if AJAX validation is needed
        $this->performAjaxValidation2($model);
        
        if(isset($_POST['TourPhoto']))
        {
            $model->attributes=$_POST['TourPhoto'];        
            //FOTO
            $foto = CUploadedFile::getInstance($model,'photo');
            
            if($foto != null){
                
                        $model->photo = str_replace(' ','-',time().'_'.$foto->name);
                
                   
                if($model->cover == 1){
                    $model->cover = TourPhoto::model()->getCover($model->tour_id);
                }
 
                if($model->save())
                {
                    if($foto !== null)
                    {
                            $path = Yii::app()->basePath.'/../images/tours/';
                            $foto->saveAs($path.$model->photo);
                    }
                    
                    Yii::app()->user->setFlash('success','Photo berhasil ditambahkan.');
                    $this->redirect(array('tour/view','id'=>$model->tour_id));
                }
            }else{
                Yii::app()->user->setFlash('danger','Photo tidak berhasil ditambahkan. Silahkan lengkapi judul dan upload file photo.');
                $this->redirect(array('tour/view','id'=>$model->tour_id));
            }
        }
    }
    
    public function actionHapusPhoto($id)
    {
        $model = TourPhoto::model()->findByPk($id);
        $tourId = $model->tour_id;
        
        //Hapus File Gambar
        $path = Yii::app()->basePath.'/../images/tours/';
        if(file_exists($path.$model->photo))
                unlink($path.$model->photo);
        
        if($model->delete())
        {           
            Yii::app()->user->setFlash('success','Photo berhasil dihapus');
            $this->redirect(array('/tour/view','id'=>$tourId));
        }
    }

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Tour;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Tour']))
{
$model->attributes=$_POST['Tour'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Tour']))
{
$model->attributes=$_POST['Tour'];
if($model->save())
$this->redirect(array('view','id'=>$model->id));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Tour');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
$model=new Tour('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Tour']))
$model->attributes=$_GET['Tour'];

$this->render('admin',array(
'model'=>$model,
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Tour::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

    /**
    * Performs the AJAX validation.
    * @param CModel the model to be validated
    */
    protected function performAjaxValidation($model)
    {
    if(isset($_POST['ajax']) && $_POST['ajax']==='tour-form')
    {
    echo CActiveForm::validate($model);
    Yii::app()->end();
    }
    
    }
    
     protected function performAjaxValidation2($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='tour-photo-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
