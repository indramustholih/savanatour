<?php

/**
 * This is the model class for table "tour".
 *
 * The followings are the available columns in table 'tour':
 * @property integer $id
 * @property string $judul
 * @property integer $tour_kategori_id
 * @property string $harga_paket
 * @property string $durasi
 * @property string $minimal
 * @property string $deskripsi
 * @property string $itenary
 * @property string $destinasi
 * @property string $include
 * @property string $exclude
 * @property integer $kabupaten_id
 * @property integer $publish
 * @property integer $local_transport
 * @property integer $pesawat
 * @property integer $tiketing
 * @property integer $makan
 * @property integer $akomodasi
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_user_id
 * @property integer $update_user_id
 */
class Tour extends CActiveRecord
{
	public $provinsi_id;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tour';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('judul, tour_kategori_id, harga_paket, durasi, minimal, provinsi_id, kabupaten_id, deskripsi', 'required'),
			array('tour_kategori_id, top, kabupaten_id, provinsi_id, publish, local_transport, pesawat, tiketing, makan, akomodasi, create_user_id, update_user_id', 'numerical', 'integerOnly'=>true),
			array('judul, harga_paket, durasi, minimal', 'length', 'max'=>255),
			array('itenary, destinasi, include, exclude, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, judul, tour_kategori_id, harga_paket, top, durasi, minimal, deskripsi, itenary, destinasi, include, exclude, kabupaten_id, provinsi_id, publish, local_transport, pesawat, tiketing, makan, akomodasi, create_time, update_time, create_user_id, update_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tourKategori' =>array(self::BELONGS_TO, 'TourKategori', 'tour_kategori_id'),
			'kabupaten' =>array(self::BELONGS_TO, 'Kabupaten', 'kabupaten_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'judul' => 'Judul',
			'top' => 'Top Destinasi',
			'tour_kategori_id' => 'Tour Kategori',
			'harga_paket' => 'Harga Paket',
			'durasi' => 'Durasi',
			'minimal' => 'Minimal',
			'deskripsi' => 'Deskripsi',
			'itenary' => 'Itenary',
			'destinasi' => 'Destinasi',
			'include' => 'Include',
			'exclude' => 'Exclude',
			'kabupaten_id' => 'Kabupaten',
			'provinsi_id' => 'Provinsi',
			'publish' => 'Publish',
			'local_transport' => 'Transport Lokal',
			'pesawat' => 'Pesawat',
			'tiketing' => 'Tiketing',
			'makan' => 'Makan',
			'akomodasi' => 'Akomodasi',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_user_id' => 'Create User',
			'update_user_id' => 'Update User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('judul',$this->judul,true);
		$criteria->compare('top',$this->top,true);
		$criteria->compare('tour_kategori_id',$this->tour_kategori_id);
		$criteria->compare('harga_paket',$this->harga_paket,true);
		$criteria->compare('durasi',$this->durasi,true);
		$criteria->compare('minimal',$this->minimal,true);
		$criteria->compare('deskripsi',$this->deskripsi,true);
		$criteria->compare('itenary',$this->itenary,true);
		$criteria->compare('destinasi',$this->destinasi,true);
		$criteria->compare('include',$this->include,true);
		$criteria->compare('exclude',$this->exclude,true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('publish',$this->publish);
		$criteria->compare('local_transport',$this->local_transport);
		$criteria->compare('pesawat',$this->pesawat);
		$criteria->compare('tiketing',$this->tiketing);
		$criteria->compare('makan',$this->makan);
		$criteria->compare('akomodasi',$this->akomodasi);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('update_time',$this->update_time,true);
		$criteria->compare('create_user_id',$this->create_user_id);
		$criteria->compare('update_user_id',$this->update_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Tour the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function beforeSave() {
	
		if ($this->isNewRecord){
			$this->create_time = new CDbExpression('NOW()');
			$this->create_user_id = Yii::app()->user->id;
		}else{
			$this->update_time = new CDbExpression('NOW()');
			$this->update_user_id = Yii::app()->user->id;
		}	
		return parent::beforeSave();
	}
	
	public function getCover($id)
	{
		$photo = TourPhoto::model()->findByAttributes(array('tour_id'=>$id, 'cover'=>'1'))->photo;
		return $photo;
	}
}
