<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Tools {
    public static function number($number,$decimals=0){
		$result = number_format($number,$decimals,',','.');
		
		return $result;
	}
	
    public static function getDate($timeStamp, $model=1)
    {
	    list($date, $time) = explode(' ', $timeStamp);	
	    list($year, $month, $day) = explode('-', $date);
	    if($model==1)
	    {
		return $day;
	    }elseif ($model==2){
		return $month;
	    }
	    
    }
    
    public static function getMonth($date, $model=1)
    {
	    list($year, $month, $day) = explode('-', $date);
	    
	    if($model==1)
	    {
		$month = Tools::getMonthName($month);
		
		$date = $day."-".$month."-".$year;
	    }
	    elseif($model==2)
	    {
		$date = $day."-".$month."-".$year;
	    }		
	    return $date;
    }
    
    public static function getDateTime($timeStamp){
	list($date, $time) = explode(' ', $timeStamp);
	
	list($year, $month, $day) = explode('-', $date);
		
	$month = Tools::getMonthName($month);
	    
	    $date = $day."-".$month."-".$year." | ".$time;
		
	return $date;
    }
    
    public static function getMonthName($month)
    {
	switch ($month){
	    case '01' : $month = 'Jan'; break;
	    case '02' : $month = 'Feb'; break;
	    case '03' : $month = 'March'; break;
	    case '04' : $month = 'Apr'; break;
	    case '05' : $month = 'Mei'; break;
	    case '06' : $month = 'Jun'; break;
	    case '07' : $month = 'Jul'; break;
	    case '08' : $month = 'Augt'; break;
	    case '09' : $month = 'Sept'; break;
	    case '10' : $month = 'Okt'; break;
	    case '11' : $month = 'Nov'; break;
	    case '12' : $month = 'Dec'; break;
	}
	
	return $month;
    }
    
    public static function getNamaHari($hari){
	
	switch($hari){     
	    case 0 : {
			$hari='Sun';
		    }break;
	    case 1 : {
			$hari='Mon';
		    }break;
	    case 2 : {
			$hari='Tue';
		    }break;
	    case 3 : {
			$hari='Wen';
		    }break;
	    case 4 : {
			$hari='Thu';
		    }break;
	    case 5 : {
			$hari="Fri";
		    }break;
	    case 6 : {
			$hari='Sat';
		    }break;
	    default: {
			$hari='UnKnown';
		    }break;
	}
	
	return $hari;
    }
    
    public function getTags($tags)
    {
	$tags2 = explode(',', $tags);
    }
    
}
?>